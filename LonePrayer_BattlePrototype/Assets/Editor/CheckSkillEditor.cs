﻿using UnityEditor.UI;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
[CustomEditor(typeof(CheckSkillButton))]
[CanEditMultipleObjects]
public class CheckSkillEditor : ButtonEditor
{
    bool showButtonsProp;
    bool showSprites;
    bool showImages;
    public override void OnInspectorGUI()
    {
        CheckSkillButton csb = (CheckSkillButton)target;

        csb.text = (Text)EditorGUILayout.ObjectField("text", csb.text, typeof(Text), false);

        showImages = EditorGUILayout.Foldout(showImages, "Target Images");

        if (showImages) {
            csb.typeTeller = (Image)EditorGUILayout.ObjectField("typeTeller", csb.typeTeller, typeof(Image), false);
            csb.slate = (Image)EditorGUILayout.ObjectField("slate", csb.slate, typeof(Image), false);
        }

        showSprites = EditorGUILayout.Foldout(showSprites, "Target Sprites");

        if (showSprites)
        {
            csb.noneTell = (Sprite)EditorGUILayout.ObjectField("noneTell", csb.noneTell, typeof(Sprite), false);
            csb.cutTell = (Sprite)EditorGUILayout.ObjectField("cutTell", csb.cutTell, typeof(Sprite), false);
            csb.bashTell = (Sprite)EditorGUILayout.ObjectField("bashTell", csb.bashTell, typeof(Sprite), false);
            csb.pierceTell = (Sprite)EditorGUILayout.ObjectField("pierceTell", csb.pierceTell, typeof(Sprite), false);
            csb.fireTell = (Sprite)EditorGUILayout.ObjectField("fireTell", csb.fireTell, typeof(Sprite), false);
            csb.iceTell = (Sprite)EditorGUILayout.ObjectField("iceTell", csb.iceTell, typeof(Sprite), false);
            csb.elecTell = (Sprite)EditorGUILayout.ObjectField("elecTell", csb.elecTell, typeof(Sprite), false);
            csb.windTell = (Sprite)EditorGUILayout.ObjectField("windTell", csb.windTell, typeof(Sprite), false);
            csb.earthTell = (Sprite)EditorGUILayout.ObjectField("earthTell", csb.earthTell, typeof(Sprite), false);
            csb.darkTell = (Sprite)EditorGUILayout.ObjectField("darkTell", csb.darkTell, typeof(Sprite), false);
            csb.healTell = (Sprite)EditorGUILayout.ObjectField("healTell", csb.healTell, typeof(Sprite), false);
            csb.buffTell = (Sprite)EditorGUILayout.ObjectField("buffTell", csb.buffTell, typeof(Sprite), false);
            csb.statusTell = (Sprite)EditorGUILayout.ObjectField("statusTell", csb.statusTell, typeof(Sprite), false);

            csb.actionSprite = (Sprite)EditorGUILayout.ObjectField("actionSprite", csb.actionSprite, typeof(Sprite), false);
            csb.abilitySprite = (Sprite)EditorGUILayout.ObjectField("abilitySprite", csb.abilitySprite, typeof(Sprite), false);
        }

        showButtonsProp = EditorGUILayout.Foldout(showButtonsProp, "Button Properties");

        if (showButtonsProp)
        {
            base.OnInspectorGUI();
        }
    }
}
