﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FleeScreen : MonoBehaviour
{
    public static FleeScreen S;
    // Start is called before the first frame update
    void Awake()
    {
        if (S == null)
        {
            S = this;
            S.gameObject.SetActive(false);
        }
        else {
            Destroy(gameObject);
        }
    }

    public void ExitGame() {
        Application.Quit();
    }

    public void Back() {
        gameObject.SetActive(false);
    }

    public void Open() {
        
        gameObject.SetActive(true);
    }
}
