﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action_Drain : Action
{
    [SerializeField]
    private float hpAbsorb = 0.0f;

    [SerializeField]
    private float spDrain = 0.0f;

    public override int CalcDamage(Fighter target, Fighter user)
    {
        float damage = 0;

        float damageMulti = 1;
        //
        if (!_sureHit)
        {
            float hitChance = CalcHit(target, user);
            if (hitChance > _accuracy)
            {
                return -1;
            }
        }

        ATTRIBUTE attackAttribute = _actionAttribute;
        RESIST_STYLE targetResistance = CalculateRes(target, user);
        int attributeAffinity = CalcAffinity(user);

        switch (targetResistance)
        {
            case RESIST_STYLE.WEAK:
                damageMulti = weaknessMultiplier;
                BattleController.S.tagSet = true;
                break;
            case RESIST_STYLE.NULL:
                return -2;
                break;
            case RESIST_STYLE.RESIST:
                damageMulti = resistMultiplier;
                break;
        }

        float critChance = CalcCrit(target, user);

        string critOn = "";
        if (critChance <= _critRate && critChance != 9999)
        {
            critOn = "It's a Critical Hit!!";
            damageMulti *= CriticalMod;
            target.guarding = false;
            BattleController.S.tagSet = true;
        }
        Debug.Log(user.FighterName + " rolled " + critChance + " for critting." + critOn);

        switch (_actionType)
        {
            case ACTION_TYPE.PHYSICAL:
                damage = (_actionPower + (user.Level * damageRatio) + Random.Range(0, 6)) * ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) /
                    (target.Stats.Defense * (1 + target.stat_Mods.PhysicalDefense)))
                    * ((attributeAffinity + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
            case ACTION_TYPE.MAGIC:
                damage = (_actionPower + (user.Level * DamageRatio) + Random.Range(0, 6)) * ((user.Stats.Magic * (1 + user.stat_Mods.MagicAttack)) /
                    (target.Stats.Magic * (1 + target.stat_Mods.MagicDefense)))
                    * ((attributeAffinity + 100 + Random.Range(5, 16)) / 100.0f) * damageMulti;
                break;
            default:
                damage = (_actionPower + (user.Level * damageRatio) + Random.Range(0, 6)) * ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) /
                    (target.Stats.Defense * (1 + target.stat_Mods.PhysicalDefense)))
                    * ((attributeAffinity + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
        }

        switch (targetResistance)
        {
            case RESIST_STYLE.REFLECT:
                user.TakeDamage((int)damage);
                BattleController.S.DisplayAnimation(this, user);
                BattleController.S.DisplayNote("REFLECT", target, FloatDisplay.DAMAGE);
                BattleController.S.DisplayNote((int)damage, user, FloatDisplay.DAMAGE);
                return -3;
            case RESIST_STYLE.ABSORB:
                target.Heal((int)(damage / 2.0f));
                BattleController.S.DisplayNote("ABSORB\n" + (int)(damage / 2.0f), target, FloatDisplay.HEAL);
                return -4;
            case RESIST_STYLE.DRAIN:
                target.SPRecover((int)(damage / 4.0f));
                BattleController.S.DisplayNote("DRAIN\n" + (int)(damage / 4.0f), target, FloatDisplay.SP_GET);
                return -5;
        }

        if (target.guarding)
        {
            damage /= 2.0f;
        }

        if (damage < 0)
        {
            damage = 0;
        }

        if (damage > 0)
        {
            if (BreakGuard)
            {
                target.guarding = false;
            }

            int spToDrain = (int)(damage * spDrain);

            if (spToDrain > target.UsableSP) {
                spToDrain = target.UsableSP;
            }

            if (hpAbsorb > 0.0f && spToDrain > 0)
            {
                BattleController.S.DisplayNote((int)(damage * hpAbsorb) + "\n", user, FloatDisplay.HEAL);
                BattleController.S.DisplayNote( "\n" + spToDrain, user, FloatDisplay.SP_GET);
            }
            else if (hpAbsorb > 0.0f)
            {
                BattleController.S.DisplayNote((int)(damage * hpAbsorb), user, FloatDisplay.HEAL);
            }
            else if (spToDrain > 0) {
                BattleController.S.DisplayNote(spToDrain, user, FloatDisplay.SP_GET);
            }

            target.SPDown(spToDrain);
            user.Heal((int)(damage * hpAbsorb));
            user.SPRecover(spToDrain);
        }

        return (int)damage;
    }

    public override void Action_Effect(Fighter user, Fighter target)
    {
        if (!_sureHit)
        {
            float hitChance = Random.Range(0, 100) - (user.Stats.Dexterity * (1 + user.stat_Mods.Accuracy));

            switch (CalculateRes(target, user))
            {
                case RESIST_STYLE.NORMAL:
                    //nothing happens
                    break;
                case RESIST_STYLE.WEAK:
                    hitChance *= 2.0f;
                    break;
                case RESIST_STYLE.RESIST:
                    hitChance /= 2.0f;
                    break;
                default:
                    hitChance = _accuracy + 1; // sure miss
                    break;
            }

            if (target.AbilitiesInclude("Status Resist"))
            {
                hitChance /= 2.0f;
            }

            if (hitChance > _accuracy)
            {
                if (BattleController.S != null)
                {
                    //miss animation
                    BattleController.S.DisplayNote(-1, target, FloatDisplay.DAMAGE);
                    return;
                }
                return;
            }
        }

        int hpToAbsorb = (int)((target.Stats.MaxHitPoints - target.Damage) * hpAbsorb * ((100 - target.StatusResistance) / 100.0f));
        int spToDrain = (int)((target.UsableSP) * spDrain * ((100 - target.StatusResistance) / 100.0f));

        if (spToDrain > target.UsableSP)
        {
            spToDrain = target.UsableSP;
        }

        if (hpToAbsorb > 0.0f && spToDrain > 0)
        {
            user.Heal(hpToAbsorb);
            target.TakeDamage(hpToAbsorb);
            BattleController.S.DisplayNote(hpToAbsorb + "\n", target, FloatDisplay.DAMAGE);
            BattleController.S.DisplayNote(hpToAbsorb + "\n", user, FloatDisplay.HEAL);

            target.SPDown(spToDrain);
            user.SPRecover(spToDrain);
            BattleController.S.DisplayNote("\n" + spToDrain, user, FloatDisplay.SP_GET);
        }
        else if (hpToAbsorb > 0.0f)
        {
            user.Heal(hpToAbsorb);
            target.TakeDamage(hpToAbsorb);
            BattleController.S.DisplayNote(hpToAbsorb, user, FloatDisplay.HEAL);
            BattleController.S.DisplayNote(hpToAbsorb, target, FloatDisplay.DAMAGE);
        }
        else if (spToDrain > 0)
        {
            target.SPDown(spToDrain);
            user.SPRecover(spToDrain);
            BattleController.S.DisplayNote(spToDrain, user, FloatDisplay.SP_GET);
        }
    }
}
