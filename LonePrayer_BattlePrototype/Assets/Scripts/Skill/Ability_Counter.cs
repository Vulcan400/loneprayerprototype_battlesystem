﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability_Counter : Ability
{
    public override void DamageTrigger(Fighter user, Fighter dealer)
    {
        if (_abilityType == ABILITY_TYPE.ON_PHYS_DAMAGE)
        {
            if (_attachedAction != null)
            {
               _attachedAction.Dmg_Spec(dealer, user);
            }
        }
        else if (_abilityType == ABILITY_TYPE.ON_MAG_DAMAGE)
        {
            if (_attachedAction != null)
            {
                _attachedAction.Dmg_Spec(dealer, user);
            }
        }
        else if (_abilityType == ABILITY_TYPE.ON_DAMAGE) {
            if (_attachedAction != null)
            {
                _attachedAction.Dmg_Spec(dealer, user);
            }
        }

        Debug.Log("Ability Triggered by Damage : " + _name + " by " + user.FighterName);
        return;
    }
}
