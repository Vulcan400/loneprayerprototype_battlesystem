﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ATTRIBUTE{
    CUT, BASH, PIERCE, FIRE, ICE, ELEC, VARIABLE, NONE, WIND, DARK, EARTH
}

public enum ACTION_TYPE
{
    PHYSICAL,
    MAGIC,
    HEAL,
    STATUS,
    REVIVE,
    CURE,
    SPECIAL, //Used for counterattack and other such things
    SP_RESTORE
}

public enum TARGET_TYPE {
    ENEMY, ALLY, ALL_ENEMY, ALL_ALLY, SELF, RANDOM
}

public class Action : Skill
{
    static protected float CriticalMod = 1.5f; //critcal damage multiplier
    static protected int HeartseekerCritRate = 20; //chances of heartseeker granting a critical hit
    static protected float weaknessMultiplier = 1.5f; // number by which weakness is calculated
    static protected float resistMultiplier = 0.5f;
    static protected float curseReduction = 0.75f,  curseBoost = 1.25f; //how curse effects damage reduction or boosting 

    //The type of action
    [SerializeField]
    protected ACTION_TYPE _actionType = ACTION_TYPE.PHYSICAL;
    public virtual ACTION_TYPE ActionType {
        get { return _actionType; }
    }

    [SerializeField]
    protected ATTRIBUTE _actionAttribute;
    public virtual ATTRIBUTE ActionAttribute {
        get { return _actionAttribute; }
    }

    [SerializeField]
    protected int _cost = 0;
    public int Cost {
        get { return _cost; }
    }

    //The power behind an action
    [SerializeField]
    protected int _actionPower = 5;
    public int ActionPower {
        get { return _actionPower; }
    }

    [SerializeField]
    protected bool _sureHit = false;
    public bool SureHit {
        get { return _sureHit; }
    }

    [SerializeField]
    protected int _accuracy = 50;
    public virtual int Accuracy {
        get { return _accuracy; }
    }

    [SerializeField]
    protected int _critRate = 1;
    public int CriticalRate {
        get {return _critRate; }
    }

    [SerializeField]
    protected int _hits = 1;
    public int Hits {
        get { return _hits; }
    }

    [SerializeField]
    protected int bonusRate = 100; // chances of extra beneficial effect happening.

    [SerializeField]
    protected float damageRatio = 0.25f;
    protected float DamageRatio {
        get {
            if (damageRatio > 1.0f)
            {
                return 1.0f;
            }
            else if (damageRatio > 0)
            {
                return damageRatio;
            }
            else
            {
                return 0.1f;
            }
        }
    }

    //Does this skill cause the user to guard
    [SerializeField]
    protected bool _activateGuard = false;
    public bool ActivateGuard {
        get { return _activateGuard; }
    }

    //Does this skill cause the user to taunt
    [SerializeField]
    protected bool _activateTaunt = false;
    public bool ActivateTaunt {
        get { return _activateTaunt; }
    }

    //Does this skill nullify guard
    [SerializeField]
    protected bool _breakGuard = false;
    public bool BreakGuard {
        get { return _breakGuard; }
    }

    //How the attack targets
    [SerializeField]
    protected TARGET_TYPE _targetMode = TARGET_TYPE.ENEMY;
    public TARGET_TYPE TargetMode {
        get { return _targetMode; }
    }

    public override SKILL_TYPE SkillType
    {
        get { return SKILL_TYPE.ACTION; }
    }

    public virtual void Action_Effect(Fighter user, Fighter target) {
        
        return;
    }

    public void Dmg_Spec(Fighter target, Fighter user ) {
        float damageMulti = 1;
        float damageToDeal = 0;
        if (!_sureHit)
        {
            float hitChance = CalcHit(target, user);

            Debug.Log(user.FighterName + " rolled " + hitChance + " for hitting.");
            if (hitChance > _accuracy)
            {
                if (BattleController.S != null)
                {
                    //miss animation
                    BattleController.S.DisplayNote(-1, target, FloatDisplay.DAMAGE);
                    return;
                }
            }
        }

        int attackStat = 0;

        switch (_actionType) {
            case ACTION_TYPE.PHYSICAL:
                attackStat = user.Stats.Strength;
                damageToDeal = (_actionPower + (user.Level * DamageRatio) + Random.Range(0, 6)) * ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) /
                (target.Stats.Defense * (1 + target.stat_Mods.PhysicalDefense)))
                * ((user.Affinities.Physical + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
            case ACTION_TYPE.MAGIC:
                attackStat = user.Stats.Magic;
                damageToDeal = (_actionPower + (user.Level * DamageRatio) + Random.Range(0, 6)) * ((user.Stats.Magic * (1 + user.stat_Mods.MagicAttack)) /
                (target.Stats.Magic * (1 + target.stat_Mods.MagicDefense)))
                * ((user.Affinities.Magic + 100 + Random.Range(5, 16)) / 100.0f) * damageMulti;
                break;
            default:
                attackStat = user.Stats.Strength;
                damageToDeal = (_actionPower + (user.Level * DamageRatio) + Random.Range(0, 6)) * ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) /
                (target.Stats.Defense * (1 + target.stat_Mods.PhysicalDefense)))
                * ((user.Affinities.Physical + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
        }

        if (damageToDeal < 0)
        {
            damageToDeal = 0;
        }

        if (BattleController.S != null)
        {
            BattleController.S.DisplayAnimation(this, target);
            BattleController.S.DisplayNote((int)damageToDeal, target, FloatDisplay.DAMAGE);
            target.TakeDamage((int)damageToDeal);
        }
    }

    public int Act_Heal(Fighter target, Fighter user)
    {
        float amountToHeal = 0;

        amountToHeal = (_actionPower + (user.Stats.Magic * DamageRatio)) * 
            ( (user.Affinities.Heal + 100 + Random.Range(-10,11)) / 100.0f);

        return (int)amountToHeal;
    }

    public int Act_RestoreSP(Fighter target) {
        return _actionPower;
    }

    public void Act_Revive(Fighter target) {
        if (target.Status == STATUS.KO) {
            target.Revive(_actionPower / 100.0f);
        }
    }

    public RESIST_STYLE CalculateRes(Fighter target, Fighter user) {
        ATTRIBUTE attackAttribute = _actionAttribute;
        RESIST_STYLE targetResistance = 0;
        if (attackAttribute == ATTRIBUTE.VARIABLE)
        {
            attackAttribute = user.AttackType;
        }

        switch (attackAttribute)
        {
            case ATTRIBUTE.NONE:
                targetResistance = RESIST_STYLE.NORMAL;
                break;
            case ATTRIBUTE.CUT:
                targetResistance = target.Resistances.Cut;
                break;
            case ATTRIBUTE.BASH:
                targetResistance = target.Resistances.Bash;
                break;
            case ATTRIBUTE.PIERCE:
                targetResistance = target.Resistances.Pierce;
                break;
            case ATTRIBUTE.FIRE:
                targetResistance = target.Resistances.Fire;
                break;
            case ATTRIBUTE.ICE:
                targetResistance = target.Resistances.Ice;
                break;
            case ATTRIBUTE.ELEC:
                targetResistance = target.Resistances.Elec;
                break;
            case ATTRIBUTE.WIND:
                targetResistance = target.Resistances.Wind;
                break;
            case ATTRIBUTE.EARTH:
                targetResistance = target.Resistances.Earth;
                break;
            case ATTRIBUTE.DARK:
                targetResistance = target.Resistances.Dark;
                break;
            default:
                targetResistance = RESIST_STYLE.NORMAL;
                break;
        }

        return targetResistance;

    }

    public int CalcAffinity(Fighter user)
    {
        ATTRIBUTE attackAttribute = _actionAttribute;
        int attributeAffinity = 0;

        if (attackAttribute == ATTRIBUTE.VARIABLE)
        {
            attackAttribute = user.AttackType;
        }

        switch (attackAttribute)
        {
            case ATTRIBUTE.CUT:
                attributeAffinity = user.Affinities.Cut;
                break;
            case ATTRIBUTE.BASH:
                attributeAffinity = user.Affinities.Bash;
                break;
            case ATTRIBUTE.PIERCE:
                attributeAffinity = user.Affinities.Pierce;
                break;
            case ATTRIBUTE.FIRE:
                attributeAffinity = user.Affinities.Fire;
                break;
            case ATTRIBUTE.ICE:
                attributeAffinity = user.Affinities.Ice;
                break;
            case ATTRIBUTE.ELEC:
                attributeAffinity = user.Affinities.Elec;
                break;
            case ATTRIBUTE.WIND:
                attributeAffinity = user.Affinities.Wind;
                break;
            case ATTRIBUTE.EARTH:
                attributeAffinity = user.Affinities.Earth;
                break;
            case ATTRIBUTE.DARK:
                attributeAffinity = user.Affinities.Dark;
                break;
            default:
                attributeAffinity = 0;
                break;
        }

        switch (_actionType) {
            case ACTION_TYPE.PHYSICAL:
                attributeAffinity += user.Affinities.Physical;
                break;
            case ACTION_TYPE.MAGIC:
                attributeAffinity += user.Affinities.Magic;
                break;
        }

        return attributeAffinity;
    }

    public virtual float CalcHit(Fighter target, Fighter user) {
        if (_sureHit) {
            return 0;
        }

        float hitChance;

        switch (_actionType)
        {
            case ACTION_TYPE.PHYSICAL:
                hitChance = Random.Range(1, 100) - (user.Stats.Dexterity * (1 + user.stat_Mods.Accuracy)) / 2.0f + (target.Stats.Agility * (1 + target.stat_Mods.Evasion)) / 2.0f;
                break;
            case ACTION_TYPE.MAGIC:
                if (target.AbilitiesInclude("Premonition"))
                {
                    //Premonition causes the target to be able to dodge magic attacks
                    hitChance = Random.Range(1, 100) - (user.Stats.Magic) / 2.0f + (target.Stats.Agility * (1 + target.stat_Mods.Evasion)) / 2.0f;
                }
                else
                {
                    //normal magic hit calculation
                    hitChance = Random.Range(1, 100);
                }
                break;
            default:
                hitChance = Random.Range(1, 100) - (user.Stats.Dexterity * (1 + user.stat_Mods.Accuracy)) / 2.0f + (target.Stats.Agility * (1 + target.stat_Mods.Evasion)) / 2.0f;
                break;
        }

        return hitChance;
    }

    public virtual float CalcCrit(Fighter target, Fighter user) {
        float critChance = 9999;

        switch (_actionType)
        {
            case ACTION_TYPE.PHYSICAL:
                critChance = Random.Range(1, 100) - ((user.Stats.Dexterity * (1 + user.stat_Mods.CriticalRate)) / 5.0f) + (target.Stats.Defense / 5.0f);

                if (user.stat_Mods.CriticalRate > 0.0f)
                {
                    critChance -= 10;
                }
                else if (user.stat_Mods.CriticalRate < 0.0f)
                {
                    critChance += 10;
                }
                break;
            case ACTION_TYPE.MAGIC:
                if (user.AbilitiesInclude("Heartseeker"))
                {
                    //Heartseeker allows magic attacks to deal critical damage
                    critChance = Random.Range(1, 100) - ((user.Stats.Dexterity * (1 + user.stat_Mods.CriticalRate)) / 5.0f) + (target.Stats.Defense / 5.0f);
                    if (user.stat_Mods.CriticalRate > 0.0f)
                    {
                        critChance -= 10;
                    }
                    else if (user.stat_Mods.CriticalRate < 0.0f)
                    {
                        critChance += 10;
                    }

                    string critOn = "";
                    if (critChance <= HeartseekerCritRate)
                    {
                        critOn = "It's a Critical Hit!!";
                        target.guarding = false;
                    }

                    Debug.Log("Heartseeker Active" /*+ user.FighterName + " rolled " + critChance + " for critting." + critOn*/);
                }
                else
                {
                    //normal magic hit calculation
                    return 9999;
                }
                break;
            default:
                return 9999;
                break;
        }

        return critChance;
    }

    public virtual int CalcDamage(Fighter target, Fighter user) {
        float damage = 0;

        float damageMulti = 1;
        //
        if (!_sureHit)
        {
            float hitChance = CalcHit(target, user);
            if (hitChance > _accuracy)
            {
                return -1;
            }
        }

        ATTRIBUTE attackAttribute = _actionAttribute;
        RESIST_STYLE targetResistance = CalculateRes(target, user);
        int attributeAffinity = CalcAffinity(user);

        switch (targetResistance) {
            case RESIST_STYLE.WEAK:
                damageMulti = weaknessMultiplier;
                BattleController.S.tagSet = true;
                BattleController.S.DisplayCheck(target, false);
                break;
            case RESIST_STYLE.NULL:
                return -2;
                break;
            case RESIST_STYLE.RESIST:
                damageMulti = resistMultiplier;
                break;
        }
       
        float critChance = CalcCrit(target, user);

        string critOn = "";
        if (critChance <= _critRate && critChance != 9999)
        {
            critOn = "It's a Critical Hit!!";
            damageMulti *= CriticalMod;
            target.guarding = false;
            BattleController.S.tagSet = true;
            BattleController.S.DisplayCheck(target, true);
        }
        Debug.Log(user.FighterName + " rolled " + critChance + " for critting." + critOn);

        switch (_actionType) {
            case ACTION_TYPE.PHYSICAL:
                damage =(_actionPower + (user.Level * damageRatio) + Random.Range(0, 6)) * ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) /
                    (target.Stats.Defense * (1 + target.stat_Mods.PhysicalDefense)))
                    * ((attributeAffinity + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
            case ACTION_TYPE.MAGIC:
                damage = (_actionPower + (user.Level * DamageRatio) + Random.Range(0, 6)) * ((user.Stats.Magic * (1 + user.stat_Mods.MagicAttack)) /
                    (target.Stats.Magic * (1 + target.stat_Mods.MagicDefense)))
                    * ((attributeAffinity + 100 + Random.Range(5, 16)) / 100.0f) * damageMulti;
                break;
            default:
                damage = (_actionPower + (user.Level * damageRatio) + Random.Range(0, 6)) * ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) /
                    (target.Stats.Defense * (1 + target.stat_Mods.PhysicalDefense)))
                    * ((attributeAffinity + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
        }

        switch (targetResistance) {
            case RESIST_STYLE.REFLECT:
                user.TakeDamage((int)damage);
                BattleController.S.DisplayAnimation(this, user);
                BattleController.S.DisplayNote("REFLECT", target, FloatDisplay.DAMAGE);
                BattleController.S.DisplayNote((int)damage, user, FloatDisplay.DAMAGE);
                return -3;
            case RESIST_STYLE.ABSORB:
                target.Heal((int)(damage / 2.0f));
                BattleController.S.DisplayNote("ABSORB\n" + (int)(damage / 2.0f), target, FloatDisplay.HEAL);
                return -4;
            case RESIST_STYLE.DRAIN:
                target.SPRecover((int)(damage / 4.0f));
                BattleController.S.DisplayNote("DRAIN\n" + (int)(damage / 4.0f), target, FloatDisplay.SP_GET);
                return -5;
        }

        if (target.guarding)
        {
            damage /= 2.0f;
        }

        if (damage < 0)
        {
            damage = 0;
        }

        if (damage > 0)
        {
            if (BreakGuard)
            {
                target.guarding = false;
            }

            float bonusTrigger = Random.Range(1, 100) - (user.Stats.Dexterity / 10.0f);

            if ( bonusTrigger <= bonusRate)
            {
                //Bonus Activation
                Debug.Log(user.FighterName + " activated bonus effect of " + SkillName);
                Action_Effect(user, target);
            }
        }

        return (int)damage;
    }
}