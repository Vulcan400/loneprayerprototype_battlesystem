﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ABILITY_TYPE {//When does this ability trigger??
    BOOST,//Stat boost, constant
    ON_ACT_END,//After acting
    ON_ATK_END,//After using a phys attack
    ON_MAG_END,//After using a magic attack
    ON_GUARD,//When initiating Guard
    ON_PHYS_DAMAGE,//After taking physical damage
    ON_MAG_DAMAGE,//After taking magic damage
    ON_DAMAGE,//After taking any damage
    ON_ENTER, //When battle starts
    ABSORB_DAMAGE, //affects damage taken
    ABSORB_PHYS,
    ABSORB_MAG,
    ON_DODGE //activates when user has dodged an attack
}

public enum ABSORB_TYPE {
    NONE,
    RES, // resist damage
    DRAIN, // drain damage into hp
    P_DRAIN, // Partial drain, take damage, recover hp afterwards
    SP, // convert damage taken into sp
    NULL, //nullify damage
    ENDURE // endure an attack
}

public class Ability : Skill
{
    [SerializeField]
    protected ABILITY_TYPE _abilityType = ABILITY_TYPE.BOOST;
    public ABILITY_TYPE AbilityType {
        get { return _abilityType; }
    }

    [SerializeField]
    protected AFFINITIES _affinities;
    public AFFINITIES Affinities
    {
        get { return _affinities; }
    }

    public override SKILL_TYPE SkillType
    {
        get { return SKILL_TYPE.ABILITY; }
    }

    [SerializeField]
    protected bool _triggerBased = false;
    public virtual bool TriggerBased{
        get{ return _triggerBased; }
    }

    [SerializeField]
    protected int _triggerRate = 50;
    public virtual int TriggerRate {
        get { return _triggerRate; }
    }

    [SerializeField]
    protected Action _attachedAction;

    [SerializeField]
    protected float absorbRate = 0.0f;
    [SerializeField]
    protected ABSORB_TYPE absorbType = ABSORB_TYPE.NONE;

    [SerializeField]
    protected ATTRIBUTE absorbAttribute = ATTRIBUTE.NONE;

    public virtual void AbilityTrigger(Fighter user) {
        //if (triggerType != _abilityType) { return; }
        Debug.Log("Ability Triggered : " + _name + " by " + user.FighterName);

        if (_attachedAction == null) {
            Debug.Log("No action attached: " + user.FighterName + "/" + _name);
            return;
        }

        if (_attachedAction.TargetMode == TARGET_TYPE.SELF || _attachedAction.TargetMode == TARGET_TYPE.ALLY)
        {
            switch (_attachedAction.ActionType )
            {
                case ACTION_TYPE.HEAL:
                    int HealAmount = _attachedAction.Act_Heal(user, user);
                    if (BattleController.S.battleControllerMode != BC_Mode.START)
                    {
                        BattleController.S.DisplayAnimation(_attachedAction, user);
                        BattleController.S.DisplayNote(HealAmount, user, FloatDisplay.HEAL);
                    }
                    user.Heal(HealAmount);
                    break;
                case ACTION_TYPE.STATUS:
                    if (BattleController.S.battleControllerMode != BC_Mode.START)
                    {
                        BattleController.S.DisplayAnimation(_attachedAction, user);
                    }
                    _attachedAction.Action_Effect(user, user);
                    break;
            }
        }
        else if (_attachedAction.TargetMode == TARGET_TYPE.ALL_ALLY) {
            switch (user.team) {
                case TEAM.PLAYER:
                    foreach (Fighter f in BattleController.S.player_party) {
                        switch (_attachedAction.ActionType)
                        {
                            case ACTION_TYPE.HEAL:
                                int HealAmount = _attachedAction.Act_Heal(f, user);
                                if (BattleController.S.battleControllerMode != BC_Mode.START)
                                {
                                    BattleController.S.DisplayAnimation(_attachedAction, f);
                                    BattleController.S.DisplayNote(HealAmount, f, FloatDisplay.HEAL);
                                }
                                user.Heal(HealAmount);
                                break;
                            case ACTION_TYPE.STATUS:
                                if (BattleController.S.battleControllerMode != BC_Mode.START)
                                {
                                    BattleController.S.DisplayAnimation(_attachedAction, f);
                                }
                                _attachedAction.Action_Effect(user, f);
                                break;
                        }
                    }
                    break;
            }
        }
        return;
    }

    public virtual void DamageTrigger( Fighter user, Fighter dealer) {
        //if (triggerType != _abilityType) { return; }
        

        Debug.Log("Ability Triggered by Damage : " + _name + " by " + user.FighterName);
        return;
    }

    public int DamageAbsorb( Fighter user, int damage, ATTRIBUTE attribute = ATTRIBUTE.NONE) {
        
        if (attribute != absorbAttribute) {
            if (absorbAttribute != ATTRIBUTE.VARIABLE) {
                return damage;
            }
        }

        if (damage < 1) {
            return damage;
        }

        Debug.Log("Absorbing Damage : " + _name + " by " + user.FighterName);

        switch (absorbType) {
            case ABSORB_TYPE.RES:
                damage = (int)(damage * 0.5f);
                break;
            case ABSORB_TYPE.P_DRAIN:
                if ((damage + user.Damage) < user.Stats.MaxHitPoints)
                {
                    user.Heal((int)(damage * absorbRate));
                    BattleController.S.DisplayNote((int)(damage * absorbRate), user, FloatDisplay.HEAL);
                }
                break;
            case ABSORB_TYPE.DRAIN:
                user.Heal((int)(damage * absorbRate));
                BattleController.S.DisplayNote((int)(damage * absorbRate), user, FloatDisplay.HEAL);
                damage = 0;
                break;
            case ABSORB_TYPE.SP:
                user.SPRecover((int)(damage * absorbRate));
                BattleController.S.DisplayNote((int)(damage * absorbRate), user, FloatDisplay.SP_GET);
                break;
            case ABSORB_TYPE.ENDURE:
                if ((damage + user.Damage) >= user.Stats.MaxHitPoints && user.Damage <= user.Stats.MaxHitPoints * absorbRate) {
                    damage = (user.Stats.MaxHitPoints - user.Damage - 1);
                }
                    break;
        }
        return damage;
    }
}
