﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action_Status : Action
{
    [SerializeField]
    protected STATUS inflict = STATUS.CURSE;

    public override void Action_Effect(Fighter user, Fighter target)
    {
        if (ActionType == ACTION_TYPE.STATUS && !SureHit) {
            //If action is status type do accuracy calc
            float hitChance = Random.Range(0, 100) - (user.Stats.Dexterity * (1 + user.stat_Mods.Accuracy)) + (target.Stats.Defense * (1 + target.stat_Mods.Evasion)) ;

            switch (CalculateRes(target, user)) {
                case RESIST_STYLE.NORMAL:
                    //nothing happens
                    break;
                case RESIST_STYLE.WEAK:
                    hitChance *= 2.0f;
                    break;
                case RESIST_STYLE.RESIST:
                    hitChance /= 2.0f;
                    break;
                default:
                    hitChance = _accuracy + 1; // sure miss
                    break;
            }

            if (target.AbilitiesInclude("Status Resist")) {
                hitChance /= 2.0f;
            }

            Debug.Log(user.FighterName + " rolled " + hitChance + " / " + (_accuracy - (_accuracy * (target.StatusResistance / 100.0f))) + " for hitting.");

            if (hitChance > _accuracy - (_accuracy * (target.StatusResistance / 100.0f)))
            {
                if (BattleController.S != null)
                {
                    //miss animation
                    BattleController.S.DisplayNote(-1, target, FloatDisplay.DAMAGE);
                    return;
                }
                return;
            }
        }

        if (target.Status == STATUS.OK && !target.AbilitiesInclude("Status Guard"))
        {
            switch (inflict)
            {
                case STATUS.CURSE:
                    target.Status = STATUS.CURSE;
                    return;
                case STATUS.EXPEL:
                    if (target.Type == FIGHTER_TYPE.SPIRIT) {
                        target.Status = STATUS.EXPEL;
                        if (BattleController.S != null)
                        {
                            //miss animation
                            BattleController.S.DisplayNote(-1, target, FloatDisplay.DAMAGE);
                            return;
                        }
                        return;
                    }
                    break;
                case STATUS.STUN:
                    target.Status = STATUS.STUN;
                    return;
                case STATUS.KO:
                    target.TakeDamage(target.Stats.MaxHitPoints);
                    //target.Status = STATUS.KO;
                    //target.Damage = target.Stats.MaxHitPoints;
                    return;
            }
        }
        else {
            if (BattleController.S != null)
            {
                //miss animation
                BattleController.S.DisplayNote(-1, target, FloatDisplay.DAMAGE);
                return;
            }
        }
    }
}
