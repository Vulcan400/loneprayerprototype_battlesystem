﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action_NullDefenses : Action
{
    public override int CalcDamage(Fighter target, Fighter user)
    {
        float damage = 0;

        float damageMulti = 1;
        //
        if (!_sureHit)
        {
            float hitChance = CalcHit(target, user);
            if (hitChance > _accuracy)
            {
                return -1;
            }
        }

        ATTRIBUTE attackAttribute = _actionAttribute;
        RESIST_STYLE targetResistance = CalculateRes(target, user);
        int attributeAffinity = CalcAffinity(user);

        switch (targetResistance)
        {
            case RESIST_STYLE.WEAK:
                damageMulti = weaknessMultiplier;
                BattleController.S.tagSet = true;
                break;
            case RESIST_STYLE.NULL:
                return -2;
                break;
            case RESIST_STYLE.RESIST:
                damageMulti = resistMultiplier;
                break;
        }

        float critChance = CalcCrit(target, user);

        string critOn = "";
        if (critChance <= _critRate && critChance != 9999)
        {
            critOn = "It's a Critical Hit!!";
            damageMulti *= CriticalMod;
            target.guarding = false;
            BattleController.S.tagSet = true;
        }
        Debug.Log(user.FighterName + " rolled " + critChance + " for critting." + critOn);

        switch (_actionType)
        {
            case ACTION_TYPE.PHYSICAL:
                damage = (_actionPower + ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) * DamageRatio) + Random.Range(0, 6))
                * ((attributeAffinity + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
            case ACTION_TYPE.MAGIC:
                damage = (_actionPower + ((user.Stats.Magic * (1 + user.stat_Mods.MagicAttack)) * DamageRatio) + Random.Range(0, 6))
                * ((attributeAffinity + 100 + Random.Range(5, 16)) / 100.0f) * damageMulti;
                break;
            default:
                damage = (_actionPower + ((user.Stats.Strength * (1 + user.stat_Mods.PhysicalAttack)) * DamageRatio) + Random.Range(0, 6))
                * ((attributeAffinity + 100 + Random.Range(0, 21)) / 100.0f) * damageMulti;
                break;
        }

        if (damage < 0)
        {
            damage = 0;
        }

        switch (targetResistance)
        {
            case RESIST_STYLE.REFLECT:
                user.TakeDamage((int)damage);
                BattleController.S.DisplayAnimation(this, user);
                BattleController.S.DisplayNote("REFLECT", target, FloatDisplay.DAMAGE);
                BattleController.S.DisplayNote((int)damage, user, FloatDisplay.DAMAGE);
                return -3;
            case RESIST_STYLE.ABSORB:
                target.Heal((int)(damage / 2.0f));
                BattleController.S.DisplayNote("ABSORB\n" + (int)(damage / 2.0f), target, FloatDisplay.HEAL);
                return -4;
            case RESIST_STYLE.DRAIN:
                target.SPRecover((int)(damage / 4.0f));
                BattleController.S.DisplayNote("DRAIN\n" + (int)(damage / 4.0f), target, FloatDisplay.SP_GET);
                return -5;
        }

        if (user.Status == STATUS.CURSE) {
            damage *= curseReduction; //If user is cursed deal less damage
        }

        if (target.Status == STATUS.CURSE) {
            damage *= curseBoost; // If target is cursed deal more damage
        }

        if (damage > 0)
        {
            if (BreakGuard)
            {
                target.guarding = false;
            }

            float bonusTrigger = Random.Range(1, 100) - (user.Stats.Dexterity / 10.0f);

            if (bonusTrigger <= bonusRate)
            {
                //Bonus Activation
                Debug.Log(user.FighterName + " activated bonus effect of " + SkillName);
                Action_Effect(user, target);
            }
        }

        return (int)damage;
    }

}
