﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mod_Action : Action
{
    //Designed for the simple buff and debuff actions
    [SerializeField]
    protected bool physAtk = false, physDef = false, magAtk = false, magDef = false, speed = false, evasion = false, accuracy = false, critical = false;
    [SerializeField]
    protected bool physAtkDown = false, physDefDown = false, magAtkDown = false, magDefDown = false, speedDown = false, evasionDown = false, accuracyDown = false, criticalDown = false;
    [SerializeField]
    protected int extraTurns = 0;

    public override void Action_Effect(Fighter user, Fighter target)
    {
        //Boost

        if (physAtk)
        {
            target.stat_Mods.BuffPhysAtk(3 + extraTurns);
        }
        if (physDef)
        {
            target.stat_Mods.BuffPhysDef(3 + extraTurns);
        }
        if (magAtk)
        {
            target.stat_Mods.BuffMagicAtk(3 + extraTurns);
        }
        if (magDef)
        {
            target.stat_Mods.BuffMagicDefense(3 + extraTurns);
        }
        if (speed)
        {
            target.stat_Mods.BuffSpeed(3 + extraTurns);
        }
        if (evasion)
        {
            target.stat_Mods.BuffEvasion(3 + extraTurns);
        }
        if (accuracy)
        {
            target.stat_Mods.BuffAccuracy(3 + extraTurns);
        }
        if (critical)
        {
            target.stat_Mods.BuffCriticalRate(3 + extraTurns);
        }

        //Down

        if (physAtkDown)
        {
            target.stat_Mods.LowerPhysAtk(3 + extraTurns);
        }
        if (physDefDown)
        {
            target.stat_Mods.LowerPhysDef(3 + extraTurns);
        }
        if (magAtkDown)
        {
            target.stat_Mods.LowerMagicAtk(3 + extraTurns);
        }
        if (magDefDown)
        {
            target.stat_Mods.LowerMagicDefense(3 + extraTurns);
        }
        if (speedDown)
        {
            target.stat_Mods.LowerSpeed(3 + extraTurns);
        }
        if (evasionDown)
        {
            target.stat_Mods.LowerEvasion(3 + extraTurns);
        }
        if (accuracyDown)
        {
            target.stat_Mods.LowerAccuracy(3 + extraTurns);
        }
        if (criticalDown)
        {
            target.stat_Mods.LowerCriticalRate(3 + extraTurns);
        }
        return;
    }
}
