﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SKILL_TYPE {
    NONE,
    ACTION,
    ABILITY
}

public class Skill : MonoBehaviour
{
    [SerializeField]
    protected string _name = "Sample Skill";
    public string SkillName {
        get { return _name; }
    }

    [SerializeField]
    protected string _description = "This skill seems incomplete";
    public string Description
    {
        get { return _description; }
    }

    /*[SerializeField]
    protected AFFINITIES _affinities;
    public AFFINITIES Affinities
    {
        get { return _affinities; }
    }*/

    public virtual SKILL_TYPE SkillType{
        get{ return SKILL_TYPE.NONE; }
    }

    /*public virtual void AbilityTrigger(ABILITY_TYPE triggerType, Fighter user)
    {
        //DO NOT TOUCH: This is here so that abilities can be triggered from the skill list. Go make an Ability if you need to do so
        return;
    }

    public virtual void DamageTrigger(ABILITY_TYPE triggerType, Fighter user, Fighter dealer)
    {
        //DO NOT TOUCH: This is here so that abilities can be triggered from the skill list. Go make an Ability if you need to do so
        return;
    }*/
}

