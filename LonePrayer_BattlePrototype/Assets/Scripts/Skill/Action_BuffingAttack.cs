﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Action_BuffingAttack : Action
{
    [SerializeField]
    private int extraTurns = 0;
    [SerializeField]
    protected bool physAtk = false, physDef = false, magAtk = false, magDef = false, speed = false, evasion = false, accuracy = false, critical = false;
    [SerializeField]
    protected bool physAtkDown = false, physDefDown = false, magAtkDown = false, magDefDown = false, speedDown = false, evasionDown = false, accuracyDown = false, criticalDown = false;


    public override void Action_Effect(Fighter user, Fighter target)
    {
        //Boost

        if (physAtk)
        {
            user.stat_Mods.BuffPhysAtk(3 + extraTurns);
        }
        if (physDef)
        {
            user.stat_Mods.BuffPhysDef(3 + extraTurns);
        }
        if (magAtk)
        {
            user.stat_Mods.BuffMagicAtk(3 + extraTurns);
        }
        if (magDef)
        {
            user.stat_Mods.BuffMagicDefense(3 + extraTurns);
        }
        if (speed)
        {
            user.stat_Mods.BuffSpeed(3 + extraTurns);
        }
        if (evasion)
        {
            user.stat_Mods.BuffEvasion(3 + extraTurns);
        }
        if (accuracy)
        {
            user.stat_Mods.BuffAccuracy(3 + extraTurns);
        }
        if (critical)
        {
            user.stat_Mods.BuffCriticalRate(3 + extraTurns);
        }

        //Down

        if (physAtkDown)
        {
            user.stat_Mods.LowerPhysAtk(3 + extraTurns);
        }
        if (physDefDown)
        {
            user.stat_Mods.LowerPhysDef(3 + extraTurns);
        }
        if (magAtkDown)
        {
            user.stat_Mods.LowerMagicAtk(3 + extraTurns);
        }
        if (magDefDown)
        {
            user.stat_Mods.LowerMagicDefense(3 + extraTurns);
        }
        if (speedDown)
        {
            user.stat_Mods.LowerSpeed(3 + extraTurns);
        }
        if (evasionDown)
        {
            user.stat_Mods.LowerEvasion(3 + extraTurns);
        }
        if (accuracyDown)
        {
            user.stat_Mods.LowerAccuracy(3 + extraTurns);
        }
        if (criticalDown)
        {
            user.stat_Mods.LowerCriticalRate(3 + extraTurns);
        }
        return;
    }
}
