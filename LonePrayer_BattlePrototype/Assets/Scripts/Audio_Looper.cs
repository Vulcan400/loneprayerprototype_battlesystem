﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio_Looper : MonoBehaviour
{
    public AudioClip MusicClip;
    public AudioSource MusicSource;

    //Loop Audio
    public float loopStart = 10;
    public float loopEnd = 60;
    

    private void Start()
    {
        MusicSource.clip = MusicClip;
    }

    private void Update()
    {
        if (MusicSource.time >= loopEnd) {
            MusicSource.time = loopStart;
        }
    }
}
