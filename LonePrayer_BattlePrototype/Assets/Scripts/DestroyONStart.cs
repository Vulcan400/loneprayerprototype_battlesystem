﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyONStart : MonoBehaviour
{
    public Coroutine destroyInTime;
    public float destructTime = 1;

    // Start is called before the first frame update
    void Start()
    {
        destroyInTime = StartCoroutine(TimedDestruction());
    }

    public IEnumerator TimedDestruction() {

        yield return new WaitForSeconds(destructTime/BattleController.S.battleSpeed);

        Destroy(this.gameObject);
    }
}
