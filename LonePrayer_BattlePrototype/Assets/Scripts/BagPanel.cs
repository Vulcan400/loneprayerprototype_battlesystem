﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BagPanel : MonoBehaviour
{
    [SerializeField]
    private int page = 1, screenLimit = 8, pageOffset = 0;

    [SerializeField]
    protected List<ItemButton> ItemButtons;

    public void DisplayBag() {
        page = 1;
        pageOffset = 0;

        if (ItemButtons.Count > 0) {
            foreach (ItemButton  i in ItemButtons) {
                i.gameObject.SetActive(false);
            }

            int displayedItems = 0;

            foreach (ItemButton i in ItemButtons) {
                if (displayedItems >= screenLimit) {
                    break;
                }
                if (Party.S.bag != null) {

                }
                i.gameObject.SetActive(true);
                i.text.text = i.ItemSlot.GetItem.Item_Name;
            }
        }

    }

}
