﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TargetButton : Button
{
    [SerializeField]
    private Action assignedAction;
    public Action GetAction {
        get { return assignedAction; }
        set { assignedAction = value; }
    }

    [SerializeField]
    private Fighter fighter;
    public Fighter AssignedFighter {
        get { return fighter; }
        set { fighter = value; }
    }

    public void TargetClick() {
        if (BattleController.S.battleControllerMode != BC_Mode.TARGET && BattleController.S.battleControllerMode != BC_Mode.TAGGING)
        {
            Debug.Log("Button Inactive : Wrong Mode");
            return;
        }

        if (BattleController.S.battleControllerMode == BC_Mode.TARGET) {
            if (assignedAction == null)
            {
                Debug.Log("Button Inactive : No Action");
                return;
            }

            if (fighter == null)
            {
                Debug.Log("Button Inactive : No Target");
                return;
            }

            if (assignedAction.TargetMode == TARGET_TYPE.ENEMY || assignedAction.TargetMode == TARGET_TYPE.ALLY)
            {
                BattleController.S.Player_Act(assignedAction, BattleController.S.turnFighter, fighter);
            }
            else
            {
                BattleController.S.Player_Act(assignedAction, BattleController.S.turnFighter);
            }
        } else if (BattleController.S.battleControllerMode == BC_Mode.TAGGING) {
            BattleController.S.tagNext = fighter;
            BattleController.S.tagSet = false;
        }

        
        BattleController.S.HideTargets();
        BattleController.S.HideActions();
    }
}
