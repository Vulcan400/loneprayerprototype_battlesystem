﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckButton : MonoBehaviour
{
    public Button button;
    public Text text;
    public Fighter fighter;

    void Awake()
    {
        if (button == null) {
            button = gameObject.GetComponent<Button>();
        }
        if (text == null) {
            text = gameObject.GetComponentInChildren<Text>();
        }
    }

    public void ShowInfo() {
        if(CheckScreenPrototype.S != null && fighter != null){
            CheckScreenPrototype.S.ShowInfo(fighter);
            return;
        }

        Debug.Log("Cannot Check");
    }
}
