﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ActionButton : Button
{
    [SerializeField]
    private Action action;

    [SerializeField]
    public Sprite noneTell, cutTell, bashTell, pierceTell, fireTell, iceTell, elecTell, windTell, earthTell, darkTell, healTell, buffTell, statusTell;

    [SerializeField]
    public Image typeTeller;

    protected bool pointerDown = false;
    protected float pointerFloat = 0.0f;

    public UnityEvent onHold;

    protected override void Awake()
    {
        base.Awake();
        if (GetComponentsInChildren<Image>().Length > 1)
        {
            typeTeller = GetComponentsInChildren<Image>()[1];
        }
        else {
            typeTeller = null;
        }
    }

    public Action AssignedAction {
        get { return action; }
        set { action = value; }
    }

    public override void OnPointerDown(PointerEventData eventData) {
        base.OnPointerDown(eventData);
        pointerDown = true;
        Debug.Log("Pointer Down: " + name);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        pointerFloat = 0.0f;
        pointerDown = false;
        if (ActionChecker.S != null) {
            ActionChecker.S.gameObject.SetActive(false);
        }
        //Debug.Log("Pointer Up: " + name);
    }

    protected void Update()
    {
        if (pointerDown)
        {
            pointerFloat += Time.deltaTime;
            if (pointerFloat >= 1.0f)
            {
                //Debug.Log("Button Hold : " + name);
                if (ActionChecker.S != null)
                {
                    ActionChecker.S.gameObject.SetActive(true);
                    ActionChecker.S.UpdateChecker(action);
                }
            }
        }
    }

    public void SpriteAdapt() {
        if (typeTeller == null) { return; }
        //Debug.Log(gameObject.name + " change sprite");

        switch (action.ActionType) {
            case ACTION_TYPE.PHYSICAL:
                switch (action.ActionAttribute) {
                    case ATTRIBUTE.CUT:
                        typeTeller.sprite = cutTell;
                        break;
                    case ATTRIBUTE.BASH:
                        typeTeller.sprite = bashTell;
                        break;
                    case ATTRIBUTE.PIERCE:
                        typeTeller.sprite = pierceTell;
                        break;
                    case ATTRIBUTE.FIRE:
                        typeTeller.sprite = fireTell;
                        break;
                    case ATTRIBUTE.ICE:
                        typeTeller.sprite = iceTell;
                        break;
                    case ATTRIBUTE.ELEC:
                        typeTeller.sprite = elecTell;
                        break;
                    case ATTRIBUTE.WIND:
                        typeTeller.sprite = windTell;
                        break;
                    case ATTRIBUTE.EARTH:
                        typeTeller.sprite = earthTell;
                        break;
                    case ATTRIBUTE.DARK:
                        typeTeller.sprite = darkTell;
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (BattleController.S.turnFighter.AttackType)
                        {
                            case ATTRIBUTE.CUT:
                                typeTeller.sprite = cutTell;
                                break;
                            case ATTRIBUTE.BASH:
                                typeTeller.sprite = bashTell;
                                break;
                            case ATTRIBUTE.PIERCE:
                                typeTeller.sprite = pierceTell;
                                break;
                            case ATTRIBUTE.FIRE:
                                typeTeller.sprite = fireTell;
                                break;
                            case ATTRIBUTE.ICE:
                                typeTeller.sprite = iceTell;
                                break;
                            case ATTRIBUTE.ELEC:
                                typeTeller.sprite = elecTell;
                                break;
                            case ATTRIBUTE.WIND:
                                typeTeller.sprite = windTell;
                                break;
                            case ATTRIBUTE.EARTH:
                                typeTeller.sprite = earthTell;
                                break;
                            case ATTRIBUTE.DARK:
                                typeTeller.sprite = darkTell;
                                break;
                            default:
                                typeTeller.sprite = noneTell;
                                break;
                        }
                        break;
                    default:
                        typeTeller.sprite = noneTell;
                        break;
                }
                break;
            case ACTION_TYPE.MAGIC:
                switch (action.ActionAttribute)
                {
                    case ATTRIBUTE.CUT:
                        typeTeller.sprite = cutTell;
                        break;
                    case ATTRIBUTE.BASH:
                        typeTeller.sprite = bashTell;
                        break;
                    case ATTRIBUTE.PIERCE:
                        typeTeller.sprite = pierceTell;
                        break;
                    case ATTRIBUTE.FIRE:
                        typeTeller.sprite = fireTell;
                        break;
                    case ATTRIBUTE.ICE:
                        typeTeller.sprite = iceTell;
                        break;
                    case ATTRIBUTE.ELEC:
                        typeTeller.sprite = elecTell;
                        break;
                    case ATTRIBUTE.WIND:
                        typeTeller.sprite = windTell;
                        break;
                    case ATTRIBUTE.EARTH:
                        typeTeller.sprite = earthTell;
                        break;
                    case ATTRIBUTE.DARK:
                        typeTeller.sprite = darkTell;
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (BattleController.S.turnFighter.AttackType)
                        {
                            case ATTRIBUTE.CUT:
                                typeTeller.sprite = cutTell;
                                break;
                            case ATTRIBUTE.BASH:
                                typeTeller.sprite = bashTell;
                                break;
                            case ATTRIBUTE.PIERCE:
                                typeTeller.sprite = pierceTell;
                                break;
                            case ATTRIBUTE.FIRE:
                                typeTeller.sprite = fireTell;
                                break;
                            case ATTRIBUTE.ICE:
                                typeTeller.sprite = iceTell;
                                break;
                            case ATTRIBUTE.ELEC:
                                typeTeller.sprite = elecTell;
                                break;
                            case ATTRIBUTE.WIND:
                                typeTeller.sprite = windTell;
                                break;
                            case ATTRIBUTE.EARTH:
                                typeTeller.sprite = earthTell;
                                break;
                            case ATTRIBUTE.DARK:
                                typeTeller.sprite = darkTell;
                                break;
                            default:
                                typeTeller.sprite = noneTell;
                                break;
                        }
                        break;
                    default:
                        typeTeller.sprite = noneTell;
                        break;
                }
                break;
            case ACTION_TYPE.HEAL:
                typeTeller.sprite = healTell;
                break;
            case ACTION_TYPE.REVIVE:
                typeTeller.sprite = healTell;
                break;
            case ACTION_TYPE.STATUS:
                switch (action.ActionAttribute)
                {
                    case ATTRIBUTE.CUT:
                        typeTeller.sprite = cutTell;
                        break;
                    case ATTRIBUTE.BASH:
                        typeTeller.sprite = bashTell;
                        break;
                    case ATTRIBUTE.PIERCE:
                        typeTeller.sprite = pierceTell;
                        break;
                    case ATTRIBUTE.FIRE:
                        typeTeller.sprite = fireTell;
                        break;
                    case ATTRIBUTE.ICE:
                        typeTeller.sprite = iceTell;
                        break;
                    case ATTRIBUTE.ELEC:
                        typeTeller.sprite = elecTell;
                        break;
                    case ATTRIBUTE.WIND:
                        typeTeller.sprite = windTell;
                        break;
                    case ATTRIBUTE.EARTH:
                        typeTeller.sprite = earthTell;
                        break;
                    case ATTRIBUTE.DARK:
                        typeTeller.sprite = darkTell;
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (BattleController.S.turnFighter.AttackType)
                        {
                            case ATTRIBUTE.CUT:
                                typeTeller.sprite = cutTell;
                                break;
                            case ATTRIBUTE.BASH:
                                typeTeller.sprite = bashTell;
                                break;
                            case ATTRIBUTE.PIERCE:
                                typeTeller.sprite = pierceTell;
                                break;
                            case ATTRIBUTE.FIRE:
                                typeTeller.sprite = fireTell;
                                break;
                            case ATTRIBUTE.ICE:
                                typeTeller.sprite = iceTell;
                                break;
                            case ATTRIBUTE.ELEC:
                                typeTeller.sprite = elecTell;
                                break;
                            case ATTRIBUTE.WIND:
                                typeTeller.sprite = windTell;
                                break;
                            case ATTRIBUTE.EARTH:
                                typeTeller.sprite = earthTell;
                                break;
                            case ATTRIBUTE.DARK:
                                typeTeller.sprite = darkTell;
                                break;
                            default:
                                typeTeller.sprite = noneTell;
                                break;
                        }
                        break;
                    default:
                        switch (action.TargetMode)
                        {
                            case TARGET_TYPE.ALLY:
                                typeTeller.sprite = buffTell;
                                break;
                            case TARGET_TYPE.ALL_ALLY:
                                typeTeller.sprite = buffTell;
                                break;
                            case TARGET_TYPE.SELF:
                                typeTeller.sprite = buffTell;
                                break;
                            default:
                                typeTeller.sprite = statusTell;
                                break;
                        }
                        break;
                }      
                break;
            default:
                typeTeller.sprite = noneTell;
                break;
        }
    }

    public void ActionExecute() {
        if (BattleController.S.battleControllerMode != BC_Mode.CHOOSE) {
            Debug.Log("Button Inactive : Wrong Mode");
            return;
        }

        if (action == null) {
            Debug.Log("Button Inactive : No Action");
            return;
        }

        BattleController.S.DisplayTargets(action);
    }
}
