﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedButton : MonoBehaviour
{
    [SerializeField]
    private Image speedIndicator;

    [SerializeField]
    private Text speedLevel;

    [SerializeField]
    private List<Sprite> speedSprites;

    private void Awake()
    {
        if (speedLevel == null) {
            Destroy(this.gameObject);
        }
        if (speedSprites.Count < 1) {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (BattleController.S != null)
        {
            switch (BattleController.S.battleSpeed)
            {
                case 1:
                    speedLevel.text = "1";
                    speedIndicator.sprite = speedSprites[0 % speedSprites.Count];
                    break;
                case 2:
                    speedLevel.text = "2";
                    speedIndicator.sprite = speedSprites[1 % speedSprites.Count];
                    break;
                case 3:
                    speedLevel.text = "3";
                    speedIndicator.sprite = speedSprites[2 % speedSprites.Count];
                    break;
            }
        }
    }
}
