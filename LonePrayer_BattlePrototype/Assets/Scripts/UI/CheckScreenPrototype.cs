﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CheckMode {
    SELECT,
    DISPLAY,
    D_SKILL
}

public class CheckScreenPrototype : MonoBehaviour
{
    static public CheckScreenPrototype S;

    protected CheckMode mode = CheckMode.SELECT;

    public List<CheckButton> enemyButtons, partyButtons;
    public Text nameTag, cutTag, bashTag, pierceTag, fireTag, iceTag, elecTag, windTag, earthTag, darkTag, stats, buffs, skills, statusTag;

    public GameObject infoPanel, buttonPanel;

    public Fighter checkFighter;

    public void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        else {
            Destroy(this.gameObject);
        }
    }

    public void Back() {
        switch (mode) {
            case CheckMode.SELECT:
                mode = CheckMode.SELECT;
                infoPanel.gameObject.SetActive(false);
                this.gameObject.SetActive(false);
                break;
            case CheckMode.DISPLAY:
                mode = CheckMode.SELECT;
                DisplayChoices();
                break;
            default:
                mode = CheckMode.SELECT;
                this.gameObject.SetActive(false);
                break;
        }
    }

    public void DisplayChoices() {
        buttonPanel.gameObject.SetActive(true);

        foreach (CheckButton c in enemyButtons) {
            c.gameObject.SetActive(false);
        }

        foreach (CheckButton c in partyButtons) {
            c.gameObject.SetActive(false);
        }

        int enemyCount = BattleController.S.enemy_party.Count;
        for (int i = 0; i < enemyCount; i++) {
            if (BattleController.S.enemy_party[i] != null)
            {
                if (BattleController.S.enemy_party[i].Status != STATUS.KO)
                {
                    enemyButtons[i].gameObject.SetActive(true);
                    enemyButtons[i].fighter = BattleController.S.enemy_party[i];
                    enemyButtons[i].text.text = enemyButtons[i].fighter.FighterName;
                }
            }
        }

        int partyCount = BattleController.S.player_party.Count;
        for (int i = 0; i < partyCount; i++)
        {
            if (BattleController.S.player_party[i] != null)
            {
                if (BattleController.S.player_party[i].Status != STATUS.KO)
                {
                    partyButtons[i].gameObject.SetActive(true);
                    partyButtons[i].fighter = BattleController.S.player_party[i];
                    partyButtons[i].text.text = partyButtons[i].fighter.FighterName;
                }
            }
        }
    }

    public virtual void ShowInfo(Fighter fighter) {
        infoPanel.gameObject.SetActive(true);

        checkFighter = fighter;

        nameTag.text = fighter.FighterName + " Lv. " + fighter.Level;

        statusTag.text = "Status: ";

        //Show fighter status
        switch (fighter.Status) {
            case STATUS.CURSE:
                statusTag.text = statusTag.text + "Cursed";
                break;
            case STATUS.EXPEL:
                statusTag.text = statusTag.text + "Expelled";
                break;
            case STATUS.STUN:
                statusTag.text = statusTag.text + "Stunned";
                break;
            case STATUS.KO:
                statusTag.text = statusTag.text + "KO";
                break;
            default:
                statusTag.text = statusTag.text + "OK";
                break;
        }

        //set resistances
        SetResistance(cutTag, fighter.Resistances.Cut);
        SetResistance(bashTag, fighter.Resistances.Bash);
        SetResistance(pierceTag, fighter.Resistances.Pierce);
        SetResistance(fireTag, fighter.Resistances.Fire);
        SetResistance(iceTag, fighter.Resistances.Ice);
        SetResistance(elecTag, fighter.Resistances.Elec);
        SetResistance(windTag, fighter.Resistances.Wind);
        SetResistance(earthTag, fighter.Resistances.Earth);
        SetResistance(darkTag, fighter.Resistances.Dark);

        //print stats

        if (fighter.team == TEAM.ENEMY)
        {
            stats.text = "HP: " + (fighter.Stats.MaxHitPoints - fighter.Damage) + "/ " + (fighter.Stats.MaxHitPoints) +
            "\nSP: ???/ ???" +
            "\nSTR: " + fighter.Stats.Strength +
            "\nDEF: " + fighter.Stats.Defense +
            "\nMAG: " + fighter.Stats.Magic +
            "\nAGI: " + fighter.Stats.Agility +
            "\nDEX: " + fighter.Stats.Dexterity;
        }
        else {
            stats.text = "HP: " + (fighter.Stats.MaxHitPoints - fighter.Damage) + "/ " + (fighter.Stats.MaxHitPoints) +
            "\nSP: " + (fighter.UsableSP) + "/ " + (fighter.Stats.MaxSkillPoints) +
            "\nSTR: " + fighter.Stats.Strength +
            "\nDEF: " + fighter.Stats.Defense +
            "\nMAG: " + fighter.Stats.Magic +
            "\nAGI: " + fighter.Stats.Agility +
            "\nDEX: " + fighter.Stats.Dexterity;
        }

        SetMods(fighter);

        //Skills
        SetSkills(fighter);

    }

    protected virtual void SetSkills(Fighter fighter) {
        skills.text = "<Skills>";

        foreach (Skill s in fighter.Actions)
        {
            skills.text = skills.text + "\n >" + s.SkillName + ": " + s.Description;
        }

        foreach (Skill s in fighter.Abilities)
        {
            skills.text = skills.text + "\n >>" + s.SkillName + ": " + s.Description;
        }
    }

    protected virtual void SetMods(Fighter fighter) {
        //printBuffs

        //get which stats are buffed

        string pAtk = "NONE", pDef = "NONE", mAtk = "NONE", mDef = "NONE", speed = "NONE", evade = "NONE", accuracy = "NONE", critRt = "NONE";

        if (fighter.stat_Mods.PhysicalAttack < 0)
        {
            pAtk = "Down";
        }
        else if (fighter.stat_Mods.PhysicalAttack > 0)
        {
            pAtk = "Up";
        }

        if (fighter.stat_Mods.PhysicalDefense < 0)
        {
            pDef = "Down";
        }
        else if (fighter.stat_Mods.PhysicalDefense > 0)
        {
            pDef = "Up";
        }

        if (fighter.stat_Mods.MagicAttack < 0)
        {
            mAtk = "Down";
        }
        else if (fighter.stat_Mods.MagicAttack > 0)
        {
            mAtk = "Up";
        }

        if (fighter.stat_Mods.MagicDefense < 0)
        {
            mDef = "Down";
        }
        else if (fighter.stat_Mods.MagicDefense > 0)
        {
            mDef = "Up";
        }

        if (fighter.stat_Mods.Speed < 0)
        {
            speed = "Down";
        }
        else if (fighter.stat_Mods.Speed > 0)
        {
            speed = "Up";
        }

        if (fighter.stat_Mods.Evasion < 0)
        {
            evade = "Down";
        }
        else if (fighter.stat_Mods.Evasion > 0)
        {
            evade = "Up";
        }

        if (fighter.stat_Mods.Accuracy < 0)
        {
            accuracy = "Down";
        }
        else if (fighter.stat_Mods.Accuracy > 0)
        {
            accuracy = "Up";
        }

        if (fighter.stat_Mods.CriticalRate < 0)
        {
            critRt = "Down";
        }
        else if (fighter.stat_Mods.CriticalRate > 0)
        {
            critRt = "Up";
        }

        //print
        buffs.text = "P.Atk: " + pAtk + " [" + fighter.stat_Mods.counter.PhysicalAttack + "]" +
            "\nP.Def: " + pDef + " [" + fighter.stat_Mods.counter.Defense + "]" +
            "\nM.Atk: " + mAtk + " [" + fighter.stat_Mods.counter.MagicAttack + "]" +
            "\nM.Def: " + mDef + " [" + fighter.stat_Mods.counter.MagicDefense + "]" +
            "\nSpeed: " + speed + " [" + fighter.stat_Mods.counter.Speed + "]" +
            "\nEvade: " + evade + " [" + fighter.stat_Mods.counter.Evasion + "]" +
            "\nAccuracy: " + accuracy + " [" + fighter.stat_Mods.counter.Accuracy + "]" +
            "\nCritRt: " + critRt + " [" + fighter.stat_Mods.counter.Critical + "]";
    }

    protected void SetResistance(Text refText, RESIST_STYLE style = RESIST_STYLE.NORMAL) {
        switch (style)
        {
            case RESIST_STYLE.WEAK:
                refText.text = "Wk";
                break;
            case RESIST_STYLE.RESIST:
                refText.text = "Res";
                break;
            case RESIST_STYLE.ABSORB:
                refText.text = "Abs";
                break;
            case RESIST_STYLE.REFLECT:
                refText.text = "Ref";
                break;
            case RESIST_STYLE.DRAIN:
                refText.text = "Drn";
                break;
            case RESIST_STYLE.NULL:
                refText.text = "Nul";
                break;
            case RESIST_STYLE.NORMAL:
                refText.text = "-";
                break;
            default:
                refText.text = "?";
                break;
        }
    }
}
