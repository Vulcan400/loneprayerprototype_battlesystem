﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum FloatDisplay {
    DAMAGE,
    HEAL,
    SP_GET
}

public class DamageText : MonoBehaviour
{
    public Coroutine displayDamage;

    [SerializeField]
    private float floatInterval = 0.01f, floatDuration = 1.0f, floatSpeed = 15.0f;

    [SerializeField]
    private Color damageColor = Color.white, healColor = Color.green, getSPColor = Color.magenta;

    public FloatDisplay floatDisplay = FloatDisplay.DAMAGE;

    private Text text;
    public Text Text {
        get { return gameObject.GetComponent<Text>(); }
    }

    public string Damage {
        get { return Text.text; }
        set { Text.text = value; }
    }

    // Start is called before the first frame update

    void Start()
    {
        text = gameObject.GetComponent<Text>();
        displayDamage = StartCoroutine(floatDamage());
    }

    private void Update()
    {
        switch (floatDisplay)
        {
            case FloatDisplay.DAMAGE:
                text.color = damageColor;
                break;
            case FloatDisplay.HEAL:
                text.color = healColor;
                break;
            case FloatDisplay.SP_GET:
                text.color = getSPColor;
                break;
            default:
                break;
        }
    }

    public IEnumerator floatDamage() {
        gameObject.transform.localPosition = Vector2.zero;
        float floatCounter = 0.0f;

        yield return new WaitForSeconds(0.3f);

        while (floatCounter < floatDuration) {
            if (BattleController.S != null)
            {
                gameObject.transform.localPosition = new Vector2(0.0f, gameObject.transform.localPosition.y + (floatSpeed * floatInterval * BattleController.S.battleSpeed));
                floatCounter += (floatInterval * BattleController.S.battleSpeed);
            }
            else {
                gameObject.transform.localPosition = new Vector2(0.0f, gameObject.transform.localPosition.y + (floatSpeed * floatInterval ));
                floatCounter += floatInterval;
            }
            
            yield return new WaitForSeconds(floatInterval);
        }

        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);

    }
}
