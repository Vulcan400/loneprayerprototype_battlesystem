﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckNote : MonoBehaviour
{

    public Coroutine display;

    private Image image;
    [SerializeField]
    private Sprite weakSpr, crtSpr;
    public bool isCrit;

    void Start()
    {
        image = GetComponent<Image>();
        image.rectTransform.localScale = Vector3.zero;
        display = StartCoroutine(floating());
    }

    // Update is called once per frame
    void Update()
    {
        if (isCrit)
        {
            image.sprite = crtSpr;
        }
        else {
            image.sprite = weakSpr;
        }

    }

    public IEnumerator floating()
    {
        //gameObject.transform.localPosition = Vector2.zero;

        float batspeed = 1;
        float scaling = 0;
        if (BattleController.S != null) {
            batspeed = BattleController.S.battleSpeed;
        }

        while (image.rectTransform.localScale.x < 1) {
            scaling += 1.0f / 5.0f;

            image.rectTransform.localScale = new Vector3(scaling, scaling, scaling);
            
            yield return new WaitForSeconds(1.0f/24.0f);
        }
        image.rectTransform.localScale = Vector3.one;
        yield return new WaitForSeconds(0.75f / batspeed);
        Destroy(this.gameObject);

    }
}
