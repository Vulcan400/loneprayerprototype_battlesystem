﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillDetailer : MonoBehaviour
{
    public static SkillDetailer S;

    [SerializeField]
    protected Text nameTag, description;

    protected void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    protected void Start()
    {
        gameObject.SetActive(false);
    }

    public void UpdateChecker( Fighter fighter)
    {
        nameTag.text = "Stat Mods";

        string pAtk = "NONE", pDef = "NONE", mAtk = "NONE", mDef = "NONE", speed = "NONE", evade = "NONE", accuracy = "NONE", critRt = "NONE";

        if (fighter.stat_Mods.PhysicalAttack < 0)
        {
            pAtk = "Down";
        }
        else if (fighter.stat_Mods.PhysicalAttack > 0)
        {
            pAtk = "Up";
        }

        if (fighter.stat_Mods.PhysicalDefense < 0)
        {
            pDef = "Down";
        }
        else if (fighter.stat_Mods.PhysicalDefense > 0)
        {
            pDef = "Up";
        }

        if (fighter.stat_Mods.MagicAttack < 0)
        {
            mAtk = "Down";
        }
        else if (fighter.stat_Mods.MagicAttack > 0)
        {
            mAtk = "Up";
        }

        if (fighter.stat_Mods.MagicDefense < 0)
        {
            mDef = "Down";
        }
        else if (fighter.stat_Mods.MagicDefense > 0)
        {
            mDef = "Up";
        }

        if (fighter.stat_Mods.Speed < 0)
        {
            speed = "Down";
        }
        else if (fighter.stat_Mods.Speed > 0)
        {
            speed = "Up";
        }

        if (fighter.stat_Mods.Evasion < 0)
        {
            evade = "Down";
        }
        else if (fighter.stat_Mods.Evasion > 0)
        {
            evade = "Up";
        }

        if (fighter.stat_Mods.Accuracy < 0)
        {
            accuracy = "Down";
        }
        else if (fighter.stat_Mods.Accuracy > 0)
        {
            accuracy = "Up";
        }

        if (fighter.stat_Mods.CriticalRate < 0)
        {
            critRt = "Down";
        }
        else if (fighter.stat_Mods.CriticalRate > 0)
        {
            critRt = "Up";
        }

        description.text = "P.Atk: \t" + pAtk + " [" + fighter.stat_Mods.counter.PhysicalAttack + "]" +
            "\nP.Def: " + pDef + " [" + fighter.stat_Mods.counter.Defense + "]" +
            "\nM.Atk: " + mAtk + " [" + fighter.stat_Mods.counter.MagicAttack + "]" +
            "\nM.Def: " + mDef + " [" + fighter.stat_Mods.counter.MagicDefense + "]" +
            "\nSpeed: " + speed + " [" + fighter.stat_Mods.counter.Speed + "]" +
            "\nEvade: " + evade + " [" + fighter.stat_Mods.counter.Evasion + "]" +
            "\nHitRt: " + accuracy + " [" + fighter.stat_Mods.counter.Accuracy + "]" +
            "\nCritRt: " + critRt + " [" + fighter.stat_Mods.counter.Critical + "]"; 
    }

    public void UpdateChecker(Skill skill)
    {
        if (nameTag != null)
        {
            nameTag.text = skill.SkillName;
        }

        if (skill.SkillType == SKILL_TYPE.ACTION)
        {
            Action action = (Action)skill;

            if (description != null)
            {
                string additions = "";

                switch (action.ActionType)
                {
                    case ACTION_TYPE.HEAL:
                        additions += "[Heal]";
                        break;
                    case ACTION_TYPE.MAGIC:
                        additions += "[Magic]";
                        break;
                    case ACTION_TYPE.PHYSICAL:
                        additions += "[Phys]";
                        break;
                    case ACTION_TYPE.REVIVE:
                        additions += "[Revive]";
                        break;
                    case ACTION_TYPE.SPECIAL:
                        break;
                    case ACTION_TYPE.STATUS:
                        additions += "[Status]";
                        break;
                    default:
                        break;
                }

                if (action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.MAGIC)
                {
                    switch (action.ActionAttribute)
                    {
                        case ATTRIBUTE.BASH:
                            additions = additions + "[Bash]";
                            break;
                        case ATTRIBUTE.CUT:
                            additions += "[Cut]";
                            break;
                        case ATTRIBUTE.PIERCE:
                            additions += "[Pierce]";
                            break;
                        case ATTRIBUTE.FIRE:
                            additions += "[Fire]";
                            break;
                        case ATTRIBUTE.ICE:
                            additions += "[Ice]";
                            break;
                        case ATTRIBUTE.ELEC:
                            additions += "[Elec]";
                            break;
                        case ATTRIBUTE.VARIABLE:
                            switch (BattleController.S.turnFighter.AttackType)
                            {
                                case ATTRIBUTE.BASH:
                                    additions += "[Bash]";
                                    break;
                                case ATTRIBUTE.CUT:
                                    additions += "[Cut]";
                                    break;
                                case ATTRIBUTE.PIERCE:
                                    additions += "[Pierce]";
                                    break;
                                case ATTRIBUTE.FIRE:
                                    additions += "[Fire]";
                                    break;
                                case ATTRIBUTE.ICE:
                                    additions += "[Ice]";
                                    break;
                                case ATTRIBUTE.ELEC:
                                    additions += "[Elec]";
                                    break;
                                case ATTRIBUTE.NONE:
                                    additions += "[None]";
                                    break;
                            }
                            break;
                        case ATTRIBUTE.NONE:
                            additions += "[None]";
                            break;
                    }
                }

                description.text = additions + "\nCost : " + action.Cost + "\tAcc : ";

                if (action.SureHit || action.TargetMode == TARGET_TYPE.ALL_ALLY || action.TargetMode == TARGET_TYPE.ALLY || action.TargetMode == TARGET_TYPE.SELF)
                {
                    description.text = description.text + "N/A";
                }
                else {
                    description.text = description.text + action.Accuracy;
                }

                if (action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.MAGIC || action.ActionType == ACTION_TYPE.HEAL) {
                    description.text = description.text + "\nPow : " + action.ActionPower;
                }

                if (action.ActionType == ACTION_TYPE.PHYSICAL) {
                    description.text = description.text + "\tCritRt : " + action.CriticalRate + "%";
                }

                description.text = description.text + "\n" + action.Description;
            }
        }

        else {
            description.text = "[Ability]\n" + skill.Description;
        }

        
    }

}
