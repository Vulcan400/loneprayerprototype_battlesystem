﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FighterPanel : MonoBehaviour
{
    public Player_Fighter fighter;

    [SerializeField]
    private Image profile, hpBar, spBar, background;

    [SerializeField]
    private Text hpText, spText;

    void Update()
    {
        UpdatePanel();
    }

    public void UpdatePanel()
    {
        hpBar.transform.localScale = new Vector2(GetHP(fighter.Damage, fighter.Stats.MaxHitPoints),1);
        spBar.transform.localScale = new Vector2(GetSP(fighter.UsableSP, fighter.Stats.MaxSkillPoints),1);

        int hp = fighter.Stats.MaxHitPoints - fighter.Damage;
        if (fighter.Status == STATUS.KO) {
            hp = 0;
        }
        int sp = fighter.UsableSP;

        hpText.text = hp.ToString();
        spText.text = sp.ToString();

        if (fighter.Faceplate != null)
        {
            profile.sprite = fighter.Faceplate;
        }

        switch (fighter.Status) {
            case STATUS.KO:
                background.color = Color.red;
                profile.color = Color.red;
                break;
            default:
                background.color = Color.white;
                profile.color = Color.white;
                break;
        }
    }

    protected float GetHP(float damage = 0, float maxHP = 1) {
        float hpScale = 1;

        hpScale = (maxHP - damage)/maxHP;

        if (fighter.Status == STATUS.KO)
        {
            hpScale = 0.0f;
        }

        if (hpScale > 1) {

            return 1.0f;
        }

        if (hpScale < 0){
            return 0.0f;
        }

        return hpScale;
    }

    protected float GetSP(float usableSP = 1, float maxSP = 1) {
        float spScale = 1;

        spScale = usableSP / maxSP;

        if (spScale > 1.0f)
        {

            return 1.0f;
        }

        if (spScale < 0.0f)
        {
            return 0.0f;
        }

        return spScale;
    }
}