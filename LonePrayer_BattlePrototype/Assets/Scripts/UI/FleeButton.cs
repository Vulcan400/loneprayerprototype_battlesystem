﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FleeButton : MonoBehaviour
{
    public void OpenFlee() {
        if (FleeScreen.S == null) {
            return;
        }

        FleeScreen.S.Open();
    }
}
