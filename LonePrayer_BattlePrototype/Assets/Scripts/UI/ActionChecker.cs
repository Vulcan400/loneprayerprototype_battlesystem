﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionChecker : MonoBehaviour
{
    public static ActionChecker S;

    [SerializeField]
    protected Text nameTag, description;

    protected void Awake()
    {
        if (S == null) {
            S = this;
        }
        else {
            Destroy(this.gameObject);
        }
    }

    protected void Start()
    {
        gameObject.SetActive(false);
    }

    public void UpdateChecker(Action action) {
        if (nameTag != null) {
            nameTag.text = action.SkillName;
        }

        if (description != null) {
            string additions = "";

            switch (action.ActionType){
                case ACTION_TYPE.HEAL:
                    additions += "[Heal]" ;
                    break;
                case ACTION_TYPE.MAGIC:
                    additions += "[Magic]";
                    break;
                case ACTION_TYPE.PHYSICAL:
                    additions += "[Phys]";
                    break;
                case ACTION_TYPE.REVIVE:
                    additions += "[Revive]";
                    break;
                case ACTION_TYPE.SPECIAL:
                    break;
                case ACTION_TYPE.STATUS:
                    additions += "[Status]";
                    break;
                default:
                    break;
            }

            if (action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.MAGIC) {
                switch (action.ActionAttribute)
                {
                    case ATTRIBUTE.BASH:
                        additions = additions + "[Bash]";
                        break;
                    case ATTRIBUTE.CUT:
                        additions += "[Cut]";
                        break;
                    case ATTRIBUTE.PIERCE:
                        additions += "[Pierce]";
                        break;
                    case ATTRIBUTE.FIRE:
                        additions += "[Fire]";
                        break;
                    case ATTRIBUTE.ICE:
                        additions += "[Ice]";
                        break;
                    case ATTRIBUTE.ELEC:
                        additions += "[Elec]";
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (BattleController.S.turnFighter.AttackType)
                        {
                            case ATTRIBUTE.BASH:
                                additions += "[Bash]";
                                break;
                            case ATTRIBUTE.CUT:
                                additions += "[Cut]";
                                break;
                            case ATTRIBUTE.PIERCE:
                                additions += "[Pierce]";
                                break;
                            case ATTRIBUTE.FIRE:
                                additions += "[Fire]";
                                break;
                            case ATTRIBUTE.ICE:
                                additions += "[Ice]";
                                break;
                            case ATTRIBUTE.ELEC:
                                additions += "[Elec]";
                                break;
                            case ATTRIBUTE.NONE:
                                additions += "[None]";
                                break;
                        }
                        break;
                    case ATTRIBUTE.NONE:
                        additions += "[None]";
                        break;
                }
            }

            description.text = additions + " " + action.Description;
        }
    }
}
