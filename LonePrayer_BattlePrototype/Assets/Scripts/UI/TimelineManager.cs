﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimelineManager : MonoBehaviour
{
    [SerializeField]
    protected List<Image> Icons;
    [SerializeField]
    protected Image pointer;
    [SerializeField]
    protected Sprite blankIcon, deadIcon;

    protected void Start()
    {
        Setup();
    }

    public void Setup() {
        if (Icons[0] == null) {
            return;
        }
        pointer.transform.localPosition = new Vector2(Icons[0].transform.localPosition.x, pointer.transform.localPosition.y);
    }

    public void MoveUp(int turnCharacter = 0) {
        if (Icons[0] == null) {
            return;
        }
        pointer.transform.localPosition = new Vector2(Icons[turnCharacter % Icons.Count].transform.localPosition.x, pointer.transform.transform.localPosition.y);
    }

    public void UpdateTimelineTurns() {
        if (Icons[0] == null) {
            return;
        }

        int changeIncrement = 0;

        foreach (Image i in Icons) {
            Icons[changeIncrement].sprite = blankIcon;
            changeIncrement++;
        }

        int turnCheck = 0;

        foreach (Fighter f in BattleController.S.turnOrder) {
            if (f != null) {
                if (f.Status != STATUS.KO)
                {
                    Icons[turnCheck].sprite = f.Icon;
                    turnCheck++;
                }
            }
        }
    }

    public void KOCleanup() {
        int increment = 0;
        foreach (Fighter f in BattleController.S.turnOrder) {
            if (f != null) {
                if (f.Status == STATUS.KO && Icons[increment].sprite != blankIcon)
                {
                    Icons[increment].sprite = deadIcon;
                }
            }
            increment++;
        }
    }
}
