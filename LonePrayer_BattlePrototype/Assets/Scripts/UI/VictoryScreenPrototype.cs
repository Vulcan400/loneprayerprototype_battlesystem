﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VictoryScreenPrototype : MonoBehaviour
{

    [SerializeField]
    protected Text xpText, moneyText, notifierText;

    public Color winColor, loseColor;

    public Image panel;

    public int xpGiven = 0, moneyGiven = 0, xpGet = 0, moneyGet = 0, advanceMode = 0, currentScene = 1;

    public Coroutine victoryRoutine;

    public void PrintResults() {

        notifierText.text = "Victory!";
        panel.color = winColor;
        victoryRoutine = StartCoroutine(Victory());
    }

    public void PrintLose() {
        notifierText.text = "GAME OVER";
        panel.color = loseColor;
        xpText.gameObject.SetActive(false);
        moneyText.gameObject.SetActive(false);
        victoryRoutine = StartCoroutine(GameOver());
    }

    public void LoadScene(){

    }

    public void AdvanceVictory() {
        switch (advanceMode) {
            case 1:
                xpGiven = xpGet;
                break;
            case 2:
                moneyGiven = moneyGet;
                break;
        }
    }

    public IEnumerator Victory() {

        foreach (Enemy e in BattleController.S.enemy_party)
        {
            xpGet += e.XP_Given * e.Level;
            moneyGet += e.Money * e.Level;
        }

        advanceMode = 1;

        xpText.text = "XP GET: 0";
        moneyText.text = "MONEY GET: $0";
        while (xpGiven < xpGet) {
            xpGiven++;
            xpText.text = "XP GET: " + xpGiven;
            yield return new WaitForSeconds(1.0f/xpGet);
        }

        xpText.text = "XP GET: " + xpGiven;

        advanceMode = 2;
        while (moneyGiven < moneyGet) {
            moneyGiven++;
            moneyText.text = "MONEY GET: $" + moneyGiven;
            yield return new WaitForSeconds(1.0f/moneyGet);
        }

        moneyText.text = "MONEY GET: $" + moneyGiven;

        advanceMode = 3;
        yield return new WaitForSeconds(3);

        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1) % SceneManager.sceneCountInBuildSettings);

        yield break;
    }

    public IEnumerator GameOver() {

        advanceMode = 4;
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(0);
        yield break;
    }
}
