﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckScreen : CheckScreenPrototype
{
    public Text hp_Text, sp_Text, str_text, def_text, mag_text, agi_text, dex_text;

    public List<CheckSkillButton> checkSkillButtons = new List<CheckSkillButton>();

    public Image profile;

    public override void ShowInfo(Fighter fighter)
    {
        infoPanel.gameObject.SetActive(true);

        checkFighter = fighter;

        nameTag.text = fighter.FighterName + " Lv. " + fighter.Level;

        statusTag.text = "Status: ";

        profile.sprite = fighter.Profile;

        if (fighter.Type == FIGHTER_TYPE.ENEMY)
        {
            Enemy enemy = (Enemy)fighter;
            profile.color = enemy.GetColor;
        }
        else {
            profile.color = Color.white;
        }

        //Show fighter status
        switch (fighter.Status)
        {
            case STATUS.CURSE:
                statusTag.text = statusTag.text + "Cursed";
                break;
            case STATUS.EXPEL:
                statusTag.text = statusTag.text + "Expelled";
                break;
            case STATUS.STUN:
                statusTag.text = statusTag.text + "Stunned";
                break;
            case STATUS.KO:
                statusTag.text = statusTag.text + "KO";
                break;
            default:
                statusTag.text = statusTag.text + "OK";
                break;
        }

        //set resistances
        SetResistance(cutTag, fighter.Resistances.Cut);
        SetResistance(bashTag, fighter.Resistances.Bash);
        SetResistance(pierceTag, fighter.Resistances.Pierce);
        SetResistance(fireTag, fighter.Resistances.Fire);
        SetResistance(iceTag, fighter.Resistances.Ice);
        SetResistance(elecTag, fighter.Resistances.Elec);
        SetResistance(windTag, fighter.Resistances.Wind);
        SetResistance(earthTag, fighter.Resistances.Earth);
        SetResistance(darkTag, fighter.Resistances.Dark);

        //print stats

        if (fighter.team == TEAM.ENEMY)
        {
            sp_Text.text = "???/ ???";
        }
        else {
            sp_Text.text = (fighter.UsableSP) + "/ " + (fighter.Stats.MaxSkillPoints);
        }

        hp_Text.text = (fighter.Stats.MaxHitPoints - fighter.Damage) + "/ " + (fighter.Stats.MaxHitPoints);
        str_text.text = fighter.Stats.Strength.ToString();
        def_text.text = fighter.Stats.Defense.ToString();
        mag_text.text = fighter.Stats.Magic.ToString();
        agi_text.text = fighter.Stats.Agility.ToString();
        dex_text.text = fighter.Stats.Dexterity.ToString();

        //SetMods(fighter);
        SetSkills(fighter);
    }

    protected override void SetSkills(Fighter fighter)
    {
        foreach (CheckSkillButton c in checkSkillButtons) {
            c.SetSkill(null);
        }
        int skillOn = 0;
        foreach (Skill s in fighter.Skills) {
            checkSkillButtons[skillOn].SetSkill(s);
            skillOn++;
            if (skillOn >= Fighter.skillLimit) {
                break;
            }
        }
    }
}
