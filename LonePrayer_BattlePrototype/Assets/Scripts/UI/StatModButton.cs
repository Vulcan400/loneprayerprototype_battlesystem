﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class StatModButton : Button
{
    protected bool pointerDown = false;
    protected float pointerFloat = 0.0f;

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        pointerDown = true;
        Debug.Log("Pointer Down: " + name);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        pointerFloat = 0.0f;
        pointerDown = false;
        if (SkillDetailer.S != null)
        {
            SkillDetailer.S.gameObject.SetActive(false);
        }
        //Debug.Log("Pointer Up: " + name);
    }

    protected void Update()
    {
        if (pointerDown)
        {
            pointerFloat += Time.deltaTime;
            if (pointerFloat >= 0.1f)
            {
                //Debug.Log("Button Hold : " + name);
                if (SkillDetailer.S != null)
                {
                    SkillDetailer.S.gameObject.SetActive(true);
                    SkillDetailer.S.UpdateChecker(CheckScreen.S.checkFighter);
                }
            }
        }
    }
}
