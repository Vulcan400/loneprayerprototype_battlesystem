﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    [SerializeField]
    protected ItemSlot slot;

    public ItemSlot ItemSlot{
        get { return slot; }
        set { slot = value; }
    }

    public Text text;

    public void UseItem() {
        return;
    }
}
