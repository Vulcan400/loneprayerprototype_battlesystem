﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Mob : MonoBehaviour
{

    public static Mob ActiveMob;

    [SerializeField]
    private List<Enemy> enemyMob;

    [SerializeField]
    private int minLevel, maxLevel;

    public List<Enemy> Enemies {
        get {
            return enemyMob;
        }
    }

    public int MinimumLevel {
        get {
            if (minLevel <= 0)
            {
                return 1;
            }
            else if (minLevel > 99)
            {
                return 99;
            }
            else {
                return minLevel;
            }
        }

    }

    public int MaximimumLevel {
        get
        {
            if (maxLevel <= 0)
            {
                return 1;
            }
            else if (maxLevel > 100)
            {
                return 100;
            }
            else
            {
                return maxLevel;
            }
        }
    }

    void Awake()
    {
        if (ActiveMob == null)
        {
            ActiveMob = this;
        }
        else {
            Destroy(this.gameObject);
        }
    }

    public void SpawnEnemies() {

        if (BattleController.S == null) {
            Debug.Log("No BattleCon found : Reload");
            return;
        }

        if (enemyMob.Count == 0) {
            Debug.Log("Mob Empty : Reload");
            return;
        }

        int enemiesSpawned = 0;
        Enemy enemy1, enemy2, enemy3;

        foreach (Enemy e in enemyMob) {
            if (enemiesSpawned >= 3) {
                break;
            }

            if (e == null) {
                break;
            }

            switch (enemiesSpawned) {
                case 0:
                    enemy1 = Instantiate(e, BattleController.S.enemyAnchor1);
                    enemy1.SetLevel(Random.Range(minLevel, maxLevel));
                    enemy1.SPMaximize();
                    enemy1.name = e.name + " A";
                    BattleController.S.enemy_party.Add(enemy1);
                    break;
                case 1:
                    enemy2 = Instantiate(e, BattleController.S.enemyAnchor2);
                    enemy2.SetLevel(Random.Range(minLevel, maxLevel));
                    enemy2.name = e.name + " B";
                    enemy2.SPMaximize();
                    BattleController.S.enemy_party.Add(enemy2);
                    break;
                case 2:
                    enemy3 = Instantiate(e, BattleController.S.enemyAnchor3);
                    enemy3.SetLevel(Random.Range(minLevel, maxLevel));
                    enemy3.name = e.name + " C";
                    enemy3.SPMaximize();
                    BattleController.S.enemy_party.Add(enemy3);
                    break;
            }

            enemiesSpawned++;
        }

        return;
    }

}
