﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Keeps track at what level a fighter can learn a skill at
[System.Serializable]
public struct LEARNABLE_SKILL {
    [SerializeField]
    private Skill SkillToLearn;
    [SerializeField]
    private int LearnAtLevel;

    public Skill skill {
        get {return SkillToLearn;}
    }

    public int level {
        get { return LearnAtLevel; }
    }

    public LEARNABLE_SKILL(Skill LearnableSkill, int Level = 1) {
        SkillToLearn = LearnableSkill;
        LearnAtLevel = Level;
    }
}

public class Player_Fighter : Fighter
{
    public override TEAM team
    {
        get {
            return TEAM.PLAYER;
        }
    }

    [SerializeField]
    protected Sprite _faceplate;
    public Sprite Faceplate {
        get { return _faceplate; }
    }

    //Skills a fighter learn as they level up
    [SerializeField]
    protected List<LEARNABLE_SKILL> _learnableSkills;
    public List<LEARNABLE_SKILL> LearnableSkills {
        get { return _learnableSkills; }
    }

    [SerializeField]
    protected List<Skill> _reserveSkills;//Skills not in use
    public List<Skill> ReserveSkills {
        get { return _reserveSkills; }
    }

    public Charm_Equip charm; //both spirits and conjurers can equip charms.

    public override STATS Stats {
        get {
            int hp = 0, sp = 0, str = 0, def = 0, mag = 0, agi = 0, dex = 0;
            int a_hp = 0, a_sp = 0, a_str = 0, a_def = 0, a_mag = 0, a_agi = 0, a_dex = 0;
            int p_hp = 100, p_sp = 100, p_str = 100, p_def = 100, p_mag = 100, p_agi = 100, p_dex = 100;

            if (charm != null)
            {
                a_hp += charm.DirectBoost.MaxHitPoints;
                a_sp += charm.DirectBoost.MaxSkillPoints;
                a_str += charm.DirectBoost.Strength;
                a_def += charm.DirectBoost.Defense;
                a_mag += charm.DirectBoost.Magic;
                a_agi += charm.DirectBoost.Agility;
                a_dex += charm.DirectBoost.Dexterity;

                p_hp += charm.ProportionalBoost.MaxHitPoints;
                p_sp += charm.ProportionalBoost.MaxSkillPoints;
                p_str += charm.ProportionalBoost.Strength;
                p_def += charm.ProportionalBoost.Defense;
                p_mag += charm.ProportionalBoost.Magic;
                p_agi += charm.ProportionalBoost.Agility;
                p_dex += charm.ProportionalBoost.Dexterity;
            }

            hp = (int)((10 + (baseHitPoints / 10) + ((_level * baseHitPoints) / 5)) * (p_hp / 100.0f) + a_hp);
            sp = (int)((5 + (baseSkillPoints / 10) + ((_level * baseSkillPoints) / 20) + (_level)) * (p_sp / 100.0f) + a_sp);
            str = (int)(((baseStrength / 10) + ((_level * baseStrength) / 50)) * (p_str / 100.0f) + a_str);
            def = (int)(((baseDefense / 10) + ((_level * baseDefense) / 50)) * (p_def / 100.0f) + a_def);
            mag = (int)(((baseMagic / 10) + ((_level * baseMagic) / 50)) * (p_mag / 100.0f) + a_mag);
            agi = (int)(((baseAgility / 10) + ((_level * baseAgility) / 50)) * (p_agi / 100.0f) + a_agi);
            dex = (int)(((baseDexterity / 10) + ((_level * baseDexterity) / 50)) * (p_dex / 100.0f) + a_dex);

            return new STATS(hp, sp, str, def, mag, agi, dex);

        }
    }

    public override AFFINITIES Affinities {
        get {
            int phys = fighterAffinities.Physical,
                mag = fighterAffinities.Magic,
                heal = fighterAffinities.Heal,
                cut = fighterAffinities.Cut,
                bash = fighterAffinities.Bash,
                pierce = fighterAffinities.Pierce,
                fire = fighterAffinities.Fire,
                ice = fighterAffinities.Ice,
                elec = fighterAffinities.Elec,
                wind = fighterAffinities.Wind,
                earth = fighterAffinities.Earth,
                dark = fighterAffinities.Dark;
            foreach (Ability a in Abilities) {
                phys += a.Affinities.Physical;
                mag += a.Affinities.Magic;
                heal += a.Affinities.Heal;
                cut += a.Affinities.Cut;
                bash += a.Affinities.Bash;
                pierce += a.Affinities.Pierce;
                fire += a.Affinities.Fire;
                ice += a.Affinities.Ice;
                elec += a.Affinities.Elec;
                wind += a.Affinities.Wind;
                earth += a.Affinities.Earth;
                dark += a.Affinities.Dark;
            }
            if (charm != null) {
                if (charm.Ability != null)
                {
                    phys += charm.Ability.Affinities.Physical;
                    mag += charm.Ability.Affinities.Magic;
                    heal += charm.Ability.Affinities.Heal;
                    cut += charm.Ability.Affinities.Cut;
                    bash += charm.Ability.Affinities.Bash;
                    pierce += charm.Ability.Affinities.Pierce;
                    fire += charm.Ability.Affinities.Fire;
                    ice += charm.Ability.Affinities.Ice;
                    elec += charm.Ability.Affinities.Elec;
                    wind += charm.Ability.Affinities.Wind;
                    earth += charm.Ability.Affinities.Earth;
                    dark += charm.Ability.Affinities.Dark;
                }
            }
            return new AFFINITIES(phys, mag, heal, cut, bash, pierce, fire, ice, elec, wind, earth, dark);
        }
    }

    public override List<Ability> Abilities
    {
        get
        {
            List<Ability> abilities = new List<Ability>();
            int skillsChecked = 0;
            foreach (Skill s in Skills)
            {
                if (skillsChecked >= skillLimit)
                {
                    break;
                }
                if (s.SkillType == SKILL_TYPE.ABILITY)
                {
                    abilities.Add((Ability)s);
                }
            }

            if (charm != null) {
                if (charm.Ability != null && !abilities.Contains(charm.Ability)) {
                    abilities.Add(charm.Ability);
                }
            }

            return abilities;
        }
    }
}
