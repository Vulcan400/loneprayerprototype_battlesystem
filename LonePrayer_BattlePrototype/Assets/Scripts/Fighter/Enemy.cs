﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ITEM_DROP {
    [SerializeField]
    private Item[] itemDrops;
    [SerializeField]
    private int[] dropRate;

    public Item item1 {
        get { return itemDrops[0]; }
    }

    public Item item2
    {
        get { return itemDrops[1]; }
    }

    public Item item3
    {
        get { return itemDrops[2]; }
    }

    public int rate1 {
        get { return dropRate[0]; }
    }

    public int rate2
    {
        get { return dropRate[1]; }
    }

    public int rate3
    {
        get { return dropRate[2]; }
    }

    public ITEM_DROP(int dropRate1 = 10, int dropRate2 = 5, int dropRate3 = 1) {
        itemDrops = new Item [3];
        dropRate = new int[3];
        dropRate[0] = dropRate1;
        dropRate[1] = dropRate2;
        dropRate[2] = dropRate3;
    }
}

public class Enemy : Fighter
{
    public override TEAM team
    {
        get {
            return TEAM.ENEMY;
        }
    }

    //XP reward
    protected int _baseXp = 10;
    public int XP_Given {
        get { return _baseXp; }
    }

    //money reward
    protected int _baseMoney = 2;
    public int Money {
        get { return _baseMoney; }
    }

    //item drop system for enemy
    [SerializeField]
    protected ITEM_DROP _itemDrop;
    public ITEM_DROP ItemDrop {
        get { return _itemDrop; }
    }

    public override FIGHTER_TYPE Type {
        get { return FIGHTER_TYPE.ENEMY; }
    }

    [SerializeField]
    protected Sprite sprite;
    public Sprite GetSprite {
        get { return sprite; }
    }

    public void SetLevel(int LevelSet = 1) {
        _level = LevelSet;
    }

    public Color GetColor {
        get { if (gameObject.GetComponent<SpriteRenderer>() != null)
            {
                return gameObject.GetComponent<SpriteRenderer>().color;
            }
            else {
                return Color.white;
            } }
    }

    public override Fighter TargetSelect()
    {
        if (BattleController.S != null)
        {
            int randomizer = Random.Range(0, BattleController.S.player_party.Count);

            while (BattleController.S.Fighters[randomizer].Status == STATUS.KO) {
                randomizer = Random.Range(0, BattleController.S.player_party.Count);
            }

            return BattleController.S.player_party[randomizer];
        }
        else
        {
            return null;
        }
    }

    public override void FighterAI() {
        BattleController.S.Player_Act(BaseAttack, this, TargetSelect());
        return;
    }

    public override void TakeDamage(int damageTaken = 1, ACTION_TYPE damageType = ACTION_TYPE.SPECIAL)
    {
        if (damageTaken < 0)
        {
            return;
        }

        damage += damageTaken;
        Debug.Log(_name + " took " + damageTaken + " damage!");

        if (damage >= Stats.MaxHitPoints)
        {
            _status = STATUS.KO;
            damage = Stats.MaxHitPoints;
            stat_Mods.Reset();
            if (GetComponent<SpriteRenderer>() != null) {
                GetComponent<SpriteRenderer>().enabled = false;
            }
            if (BattleController.S.timelineManager != null)
            {
                BattleController.S.timelineManager.KOCleanup();
            }
        }
    }
}
