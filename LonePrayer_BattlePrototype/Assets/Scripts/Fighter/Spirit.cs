﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spirit : Player_Fighter
{
    [SerializeField]
    private int _bond = 0;
    public int bond {
        get {
            if (_bond > 100)
            {
                return 100;
            }
            else {
                return _bond;
            }
        }
    }

    public void AlterBond(int change = 0) {
        int resultingBond = _bond + change;
        if (resultingBond > 0) {
            _bond = resultingBond;
        }
        Debug.Log(_name + "bond change by : " + change + " to : " + _bond);
        return;
    }
}
