﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_PaperWhale : Enemy
{
    public override void FighterAI()
    {
        Debug.Log(FighterName + ": Initiating AI");
        int randomizer = Random.Range(0, 10);

        if (Actions.Count < 1)
        {
            Debug.Log(FighterName + ": Skills not found");
            BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
            return;
        }

        if (randomizer <= 5)
        {
            if (Actions[0] == null)
            {
                Debug.Log(FighterName + ": Choice 1 unusable not found");
                BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
                return;
            }

            if (Actions[0].Cost > usable_SP)
            {
                Debug.Log(FighterName + ": Choice 1  unusable, not enough sp");
                BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
                return;
            }

            Debug.Log(FighterName + ": Choice 1");
            BattleController.S.Player_Act(Actions[0], this, TargetSelect());
            return;
        }
        else if (randomizer <= 7) {
            if (Actions[1] == null)
            {
                Debug.Log(FighterName + ": Choice 2 not found");
                BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
                return;
            }

            if (Actions[1].Cost > usable_SP)
            {
                Debug.Log(FighterName + ": Choice 2  unusable, not enough sp");
                BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
                return;
            }

            Debug.Log(FighterName + ": Choice 2");
            BattleController.S.Player_Act(Actions[1], this, TargetSelect());
            return;
        }
        else if (randomizer <= 10)
        {
            if (Actions[2] == null)
            {
                Debug.Log(FighterName + ": Choice 3 not found");
                BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
                return;
            }

            if (Actions[2].Cost > usable_SP)
            {
                Debug.Log(FighterName + ": Choice 3  unusable, not enough sp");
                BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
                return;
            }

            Debug.Log(FighterName + ": Choice 3");
            BattleController.S.Player_Act(Actions[2], this);
            return;
        }

        Debug.Log(FighterName + ": Initiating basic attack");
        BattleController.S.Player_Act(BattleController.S.defaultActions[0], this, TargetSelect());
        return;
    }
}
