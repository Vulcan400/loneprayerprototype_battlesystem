﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conjurer : Player_Fighter
{
    public Armor_Equip armor;
    public Weapon_Equip weapon;

    public override Action BaseAttack
    {
        get
        {
            if (weapon !=  null)
            {
                return weapon.Attack;
            }
            else
            {
                if (baseAttack != null) {
                    return baseAttack;
                }
                else
                {
                    if (BattleController.S == null)
                    {
                        return null;
                    }

                    return BattleController.S.defaultActions[0];
                }
            }
        }
    }

    public override ATTRIBUTE AttackType
    {
        get
        {
            if (weapon != null)
            {
                return weapon.AttackAttribute;
            }
            else
            {
                return _baseAttackType;
            }
        }
    }

    public override STATS Stats {
        get {
            int hp = 0, sp = 0, str = 0, def = 0, mag = 0, agi = 0, dex = 0;
            int a_hp = 0, a_sp = 0, a_str = 0, a_def = 0, a_mag = 0, a_agi = 0, a_dex = 0; //direct addition
            float p_hp = 100, p_sp = 100, p_str = 100, p_def = 100, p_mag = 100, p_agi = 100, p_dex = 100; // proportional addition

            if (armor != null)
            {
                a_hp += armor.DirectBoost.MaxHitPoints;
                a_sp += armor.DirectBoost.MaxSkillPoints;
                a_str += armor.DirectBoost.Strength;
                a_def += armor.DirectBoost.Defense;
                a_mag += armor.DirectBoost.Magic;
                a_agi += armor.DirectBoost.Agility;
                a_dex += armor.DirectBoost.Dexterity;
            }

            if (weapon != null)
            {
                a_hp += weapon.DirectBoost.MaxHitPoints;
                a_sp += weapon.DirectBoost.MaxSkillPoints;
                a_str += weapon.DirectBoost.Strength;
                a_def += weapon.DirectBoost.Defense;
                a_mag += weapon.DirectBoost.Magic;
                a_agi += weapon.DirectBoost.Agility;
                a_dex += weapon.DirectBoost.Dexterity;
            }


            if (charm != null)
            {
                a_hp += charm.DirectBoost.MaxHitPoints;
                a_sp += charm.DirectBoost.MaxSkillPoints;
                a_str += charm.DirectBoost.Strength;
                a_def += charm.DirectBoost.Defense;
                a_mag += charm.DirectBoost.Magic;
                a_agi += charm.DirectBoost.Agility;
                a_dex += charm.DirectBoost.Dexterity;

                p_hp += charm.ProportionalBoost.MaxHitPoints;
                p_sp += charm.ProportionalBoost.MaxSkillPoints;
                p_str += charm.ProportionalBoost.Strength;
                p_def += charm.ProportionalBoost.Defense;
                p_mag += charm.ProportionalBoost.Magic;
                p_agi += charm.ProportionalBoost.Agility;
                p_dex += charm.ProportionalBoost.Dexterity;
            }
            if (Party.S != null)
            {
                if (Party.S.spirit1 != null)
                {
                    if (Party.S.spirit1.Status != STATUS.KO && Party.S.spirit1.Status != STATUS.EXPEL)
                    {
                        a_str += (int)(Party.S.spirit1.AffinityStrength * Party.S.spirit1.bond / 100.0f);
                        a_def += (int)(Party.S.spirit1.AffinityDefense * Party.S.spirit1.bond / 100.0f);
                        a_mag += (int)(Party.S.spirit1.AffinityMagic * Party.S.spirit1.bond / 100.0f);
                        a_agi += (int)(Party.S.spirit1.AffinityAgility * Party.S.spirit1.bond / 100.0f);
                        a_dex += (int)(Party.S.spirit1.AffinityDexterity * Party.S.spirit1.bond / 100.0f);
                    }
                }

                if (Party.S.spirit2 != null)
                {
                    if (Party.S.spirit2.Status != STATUS.KO && Party.S.spirit2.Status != STATUS.EXPEL)
                    {
                        a_str += (int)(Party.S.spirit2.AffinityStrength * Party.S.spirit2.bond / 100.0f);
                        a_def += (int)(Party.S.spirit2.AffinityDefense * Party.S.spirit2.bond / 100.0f);
                        a_mag += (int)(Party.S.spirit2.AffinityMagic * Party.S.spirit2.bond / 100.0f);
                        a_agi += (int)(Party.S.spirit2.AffinityAgility * Party.S.spirit2.bond / 100.0f);
                        a_dex += (int)(Party.S.spirit2.AffinityDexterity * Party.S.spirit2.bond / 100.0f);
                    }
                }
            }

            hp = (int)((10 + (baseHitPoints / 10) + ((_level * baseHitPoints) / 5)) * (p_hp / 100.0f) + a_hp);
            sp = (int)((5 + (baseSkillPoints / 10) + ((_level * baseSkillPoints) / 20) + (_level)) * (p_sp / 100.0f) + a_sp);
            str = (int)(((baseStrength / 10) + ((_level * baseStrength) / 50)) * (p_str / 100.0f) + a_str);
            def = (int)(((baseDefense / 10) + ((_level * baseDefense) / 50)) * (p_def / 100.0f) + a_def);
            mag = (int)(((baseMagic / 10) + ((_level * baseMagic) / 50)) * (p_mag / 100.0f) + a_mag);
            agi = (int)(((baseAgility / 10) + ((_level * baseAgility) / 50)) * (p_agi / 100.0f) + a_agi);
            dex = (int)(((baseDexterity / 10) + ((_level * baseDexterity) / 50)) * (p_dex / 100.0f) + a_dex);

            return new STATS(hp, sp, str, def, mag, agi, dex);
        }
    }

    /*public override RESISTANCES Resistances
    {
        get {
            if (armor != null)
            {
                return armor.ResistBonus;
            }
            else
            {
                return fighterResistances;
            }
        }
    }*/

    public override AFFINITIES Affinities
    {
        get
        {
            int phys = fighterAffinities.Physical,
                mag = fighterAffinities.Magic,
                heal = fighterAffinities.Heal,
                cut = fighterAffinities.Cut,
                bash = fighterAffinities.Bash,
                pierce = fighterAffinities.Pierce,
                fire = fighterAffinities.Fire,
                ice = fighterAffinities.Ice,
                elec = fighterAffinities.Elec,
                wind = fighterAffinities.Wind,
                earth = fighterAffinities.Earth,
                dark = fighterAffinities.Dark;
            foreach (Ability a in Abilities)
            {
                phys += a.Affinities.Physical;
                mag += a.Affinities.Magic;
                heal += a.Affinities.Heal;
                cut += a.Affinities.Cut;
                bash += a.Affinities.Bash;
                pierce += a.Affinities.Pierce;
                fire += a.Affinities.Fire;
                ice += a.Affinities.Ice;
                elec += a.Affinities.Elec;
                wind += a.Affinities.Wind;
                earth += a.Affinities.Earth;
                dark += a.Affinities.Dark;
            }
            if (charm != null)
            {
                if (charm.Ability != null)
                {
                    phys += charm.Ability.Affinities.Physical;
                    mag += charm.Ability.Affinities.Magic;
                    heal += charm.Ability.Affinities.Heal;
                    cut += charm.Ability.Affinities.Cut;
                    bash += charm.Ability.Affinities.Bash;
                    pierce += charm.Ability.Affinities.Pierce;
                    fire += charm.Ability.Affinities.Fire;
                    ice += charm.Ability.Affinities.Ice;
                    elec += charm.Ability.Affinities.Elec;
                    wind += charm.Ability.Affinities.Wind;
                    earth += charm.Ability.Affinities.Earth;
                    dark += charm.Ability.Affinities.Dark;
                }
            }
            if (weapon != null)
            {
                if (weapon.Ability != null)
                {
                    phys += weapon.Ability.Affinities.Physical;
                    mag += weapon.Ability.Affinities.Magic;
                    heal += weapon.Ability.Affinities.Heal;
                    cut += weapon.Ability.Affinities.Cut;
                    bash += weapon.Ability.Affinities.Bash;
                    pierce += weapon.Ability.Affinities.Pierce;
                    fire += weapon.Ability.Affinities.Fire;
                    ice += weapon.Ability.Affinities.Ice;
                    elec += weapon.Ability.Affinities.Elec;
                    wind += weapon.Ability.Affinities.Wind;
                    earth += weapon.Ability.Affinities.Earth;
                    dark += weapon.Ability.Affinities.Dark;
                }
            }
            if (armor != null)
            {
                if (armor.Ability != null)
                {
                    phys += armor.Ability.Affinities.Physical;
                    mag += armor.Ability.Affinities.Magic;
                    heal += armor.Ability.Affinities.Heal;
                    cut += armor.Ability.Affinities.Cut;
                    bash += armor.Ability.Affinities.Bash;
                    pierce += armor.Ability.Affinities.Pierce;
                    fire += armor.Ability.Affinities.Fire;
                    ice += armor.Ability.Affinities.Ice;
                    elec += armor.Ability.Affinities.Elec;
                    wind += armor.Ability.Affinities.Wind;
                    earth += armor.Ability.Affinities.Earth;
                    dark += armor.Ability.Affinities.Dark;
                }
            }
            return new AFFINITIES(phys, mag, heal, cut, bash, pierce, fire, ice, elec, wind, earth, dark);
        }
    }

    public override List<Ability> Abilities
    {
        get
        {
            List<Ability> abilities = new List<Ability>();
            int skillsChecked = 0;
            foreach (Skill s in Skills)
            {
                if (skillsChecked >= skillLimit)
                {
                    break;
                }
                if (s.SkillType == SKILL_TYPE.ABILITY)
                {
                    abilities.Add((Ability)s);
                }
            }

            if (charm != null)
            {
                if (charm.Ability != null && !abilities.Contains(charm.Ability))
                {
                    abilities.Add(charm.Ability);
                }
            }

            if (weapon != null)
            {
                if (weapon.Ability != null && !abilities.Contains(weapon.Ability))
                {
                    abilities.Add(weapon.Ability);
                }
            }

            if (armor != null && !abilities.Contains(armor.Ability))
            {
                if (armor.Ability != null)
                {
                    abilities.Add(armor.Ability);
                }
            }

            return abilities;
        }
    }

    public override RESIST_STYLE ResistanceCheck(ATTRIBUTE attribute)
    {
        RESISTANCES resistGet = fighterResistances;

        if (armor != null) {
            resistGet = armor.ResistBonus;
        }

        RESIST_STYLE resist = RESIST_STYLE.NORMAL;
        string styleName = "";

        switch (attribute)
        {
            case ATTRIBUTE.CUT:
                resist = resistGet.Cut;
                styleName = "Cut";
                break;
            case ATTRIBUTE.BASH:
                resist = resistGet.Bash;
                styleName = "Bash";
                break;
            case ATTRIBUTE.PIERCE:
                resist = resistGet.Pierce;
                styleName = "Pierce";
                break;
            case ATTRIBUTE.FIRE:
                resist = resistGet.Fire;
                styleName = "Fire";
                break;
            case ATTRIBUTE.ICE:
                resist = resistGet.Ice;
                styleName = "Ice";
                break;
            case ATTRIBUTE.ELEC:
                resist = resistGet.Elec;
                styleName = "Elec";
                break;
            case ATTRIBUTE.WIND:
                resist = resistGet.Wind;
                styleName = "Wind";
                break;
            case ATTRIBUTE.EARTH:
                resist = resistGet.Earth;
                styleName = "Earth";
                break;
            case ATTRIBUTE.DARK:
                resist = resistGet.Dark;
                styleName = "Dark";
                break;
        }

        //Checks if fighter has a skill of Resist Style X, depending on their resistance style it will be replaced
        if (AbilitiesInclude("Reflect " + styleName))
        {
            if ((int)resist <= 3)
            {
                resist = RESIST_STYLE.REFLECT;
            }
        }
        else if (AbilitiesInclude("Drain " + styleName))
        {
            if ((int)resist <= 3)
            {
                resist = RESIST_STYLE.DRAIN;
            }
        }
        else if (AbilitiesInclude("Absorb " + styleName))
        {
            if ((int)resist <= 3)
            {
                resist = RESIST_STYLE.ABSORB;
            }
        }
        else if (AbilitiesInclude("Null " + styleName))
        {
            if ((int)resist <= 2)
            {
                resist = RESIST_STYLE.NULL;
            }
        }
        else if (AbilitiesInclude("Resist " + styleName))
        {
            if ((int)resist <= 2)
            {
                resist = RESIST_STYLE.RESIST;
            }
        }

        return resist;
    }

}
