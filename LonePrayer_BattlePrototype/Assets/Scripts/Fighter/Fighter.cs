﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TEAM // What side am I on?
{
    PLAYER,
    ENEMY,
    ALLY //AI Helper
}

public enum FIGHTER_TYPE // what type of fighter am I?
{
    BASIC,//Gets/Gives no bonuses from/to team
    CONJURER,//Gets bonuses from spirits on team
    SPIRIT,//Gives bonuses to conjuerers
    ENEMY
}

public enum RESIST_STYLE {
    NORMAL,
    RESIST, // halve the damage
    WEAK, //fighter's damage is multiplied
    NULL, //turn to 0
    ABSORB, //turn to HP
    DRAIN, //turn to SP
    REFLECT //Send back at attacker
}

public enum STATUS //Condition of the fighter
{
    OK,
    CURSE, // take 5 to 10 percent of Max_HP in damage at turn end
    EXPEL, // Break affinities between conjuerer and spirit
    KO, //You ded
    STUN //Cannot act next turn
}

[System.Serializable]
public struct STATS // the stats of the fighter
{
    [SerializeField]
    private int MAX_HP, MAX_SP, STR, DEF, MAG, AGI, DEX;

    public int MaxHitPoints
    {
        get
        {
            return MAX_HP;
        }
    }

    public int MaxSkillPoints
    {
        get
        {
            return MAX_SP;
        }
    }

    public int Strength
    {
        get
        {
            return STR;
        }
    }

    public int Defense
    {
        get
        {
            return DEF;
        }
    }

    public int Magic
    {
        get
        {
            return MAG;
        }
    }

    public int Agility
    {
        get
        {
            return AGI;
        }
    }

    public int Dexterity
    {
        get
        {
            return DEX;
        }
    }

    public STATS( int HIT_POINTS, int SKILL_POINTS, int STRENGTH, int DEFENSE, int MAGIC, int AGILITY, int DEXTERITY)
    {
        MAX_HP = HIT_POINTS;
        MAX_SP = SKILL_POINTS;
        if (STRENGTH < 1) {
            STRENGTH = 1;
        }
        STR = STRENGTH;
        if (DEFENSE < 1)
        {
            DEFENSE = 1;
        }
        DEF = DEFENSE;
        if (MAGIC < 1)
        {
            MAGIC = 1;
        }
        MAG = MAGIC;
        if (AGILITY < 1)
        {
            AGILITY = 1;
        }
        AGI = AGILITY;
        if (DEXTERITY < 1)
        {
            DEXTERITY = 1;
        }
        DEX = DEXTERITY;
    }

    public void SetHitPoints(int HIT_POINTS) {
        MAX_HP = HIT_POINTS;
    }

    public void SetSkillPoints(int SKILL_POINTS) {
        MAX_SP = SKILL_POINTS;
    }

    public void SetStrength(int STRENGTH) {
        STR = STRENGTH;
    }

    public void SetDefense(int DEFENSE) {
        DEF = DEFENSE;
    }

    public void SetMagic(int MAGIC) {
        MAG = MAGIC;
    }

    public void SetAgility(int AGILITY) {
        AGI = AGILITY;
    }

    public void SetDexterity(int DEXTERITY) {
        DEX = DEXTERITY;
    }
}

//Damage resistances
[System.Serializable]
public struct RESISTANCES
{
    [SerializeField]
    private RESIST_STYLE CUT, BASH, PIERCE, FIRE, ICE, ELEC, WIND, EARTH, DARK;

    public RESIST_STYLE Cut
    {
        get { return CUT; }
    }

    public RESIST_STYLE Bash
    {
        get { return BASH; }
    }

    public RESIST_STYLE Pierce
    {
        get { return PIERCE; }
    }

    public RESIST_STYLE Fire
    {
        get { return FIRE; }
    }

    public RESIST_STYLE Ice
    {
        get { return ICE; }
    }

    public RESIST_STYLE Elec
    {
        get { return ELEC; }
    }

    public RESIST_STYLE Wind {
        get { return WIND; }
    }

    public RESIST_STYLE Earth {
        get { return EARTH; }
    }

    public RESIST_STYLE Dark {
        get { return DARK; }
    }

    public RESISTANCES(RESIST_STYLE CutResistance, RESIST_STYLE BashResistance, RESIST_STYLE PierceResistance, RESIST_STYLE FireResistance, RESIST_STYLE IceResistance, RESIST_STYLE ElecResistance, RESIST_STYLE WindResistance, RESIST_STYLE EarthResistance, RESIST_STYLE DarkResistance)
    {
        CUT = CutResistance;
        BASH = BashResistance;
        PIERCE = PierceResistance;
        FIRE = FireResistance;
        ICE = IceResistance;
        ELEC = ElecResistance;
        WIND = WindResistance;
        EARTH = EarthResistance;
        DARK = DarkResistance;
    }
}

//Effectiveness with certain actions, increase power of those actions
[System.Serializable]
public struct AFFINITIES { 
    [SerializeField]
    private int PHYSICAL, MAGIC, HEAL, CUT, BASH, PIERCE, FIRE, ICE, ELEC, WIND, EARTH, DARK; //boost power

    public int Physical {
        get { return PHYSICAL; }
    }
    public int Magic
    {
        get { return MAGIC; }
    }
    public int Heal
    {
        get { return HEAL; }
    }
    public int Cut
    {
        get { return CUT; }
    }
    public int Bash
    {
        get { return BASH; }
    }
    public int Pierce {
        get { return PIERCE; }
    }
    public int Fire
    {
        get { return FIRE; }
    }
    public int Ice
    {
        get { return ICE; }
    }
    public int Elec
    {
        get { return ELEC; }
    }

    public int Wind
    {
        get { return WIND; }
    }

    public int Earth
    {
        get { return EARTH; }
    }

    public int Dark
    {
        get { return DARK; }
    }

    public AFFINITIES(int physical, int magic, int heal, int cut, int bash, int pierce, int fire, int ice, int elec, int wind, int earth, int dark) {
        PHYSICAL = physical;
        MAGIC = magic;
        HEAL = heal;
        CUT = cut;
        BASH = bash;
        PIERCE = pierce;
        FIRE = fire;
        ICE = ice;
        ELEC = elec;
        WIND = wind;
        EARTH = earth;
        DARK = dark;
    }
}

[System.Serializable]
public struct MOD_COUNTER
{
    [SerializeField]
    private int P_ATK, P_DEF, MAG_A, MAG_D, SPD, EVA, ACC, CRIT;

    public int PhysicalAttack
    {
        get
        {
            return P_ATK;
        }
        set
        {
            P_ATK = value;
        }
    }

    public int Defense
    {
        get
        {
            return P_DEF;
        }
        set
        {
            P_DEF = value;
        }
    }

    public int MagicAttack
    {
        get
        {
            return MAG_A;
        }
        set
        {
            MAG_A = value;
        }
    }

    public int MagicDefense
    {
        get
        {
            return MAG_D;
        }
        set
        {
            MAG_D = value;
        }
    }

    public int Speed
    {
        get
        {
            return SPD;
        }
        set
        {
            SPD = value;
        }
    }

    public int Evasion
    {
        get
        {
            return EVA;
        }
        set
        {
            EVA = value;
        }
    }

    public int Accuracy
    {
        get
        {
            return ACC;
        }
        set
        {
            ACC = value;
        }
    }

    public int Critical
    {
        get
        {
            return CRIT;
        }
        set
        {
            CRIT = value;
        }
    }

    public MOD_COUNTER(int physAttack = 0, int physDefense = 0, int magicAttack = 0, int magicDefense = 0, int speed = 0, int evasion = 0, int accuracy = 0, int critical = 0)
    {
        P_ATK = physAttack;
        P_DEF = physDefense;
        MAG_A = magicAttack;
        MAG_D = magicDefense;
        SPD = speed;
        EVA = evasion;
        ACC = accuracy;
        CRIT = critical;
    }

    static readonly MOD_COUNTER zeroCounter = new MOD_COUNTER(0, 0, 0, 0, 0, 0, 0, 0);

    public static MOD_COUNTER Zero{ get { return zeroCounter; } }

    public void Reset()
    {
        P_ATK = 0;
        P_DEF = 0;
        MAG_A = 0;
        MAG_D = 0;
        SPD = 0;
        EVA = 0;
        ACC = 0;
        CRIT = 0;
    }

    public void ModDown() {
        if (P_ATK > 0) {
            P_ATK -= 1;
        }
        if (P_DEF > 0)
        {
            P_DEF -= 1;
        }
        if (MAG_A > 0)
        {
            MAG_A -= 1;
        }
        if (MAG_D > 0)
        {
            MAG_D -= 1;
        }
        if (SPD > 0)
        {
            SPD -= 1;
        }
        if (EVA > 0) {
            EVA -= 1;
        }
        if (ACC > 0)
        {
            ACC -= 1;
        }
        if (CRIT > 0)
        {
            CRIT -= 1;
        }
    }

}

[System.Serializable]
public struct STAT_MODS {
    private static readonly float buffAmount = 0.5f, debuffAmount = -0.3f;
    private const int defaultModDuration = 3;

    [SerializeField]
    private float mod_PhysAttack, mod_MagicAttack, mod_PhysDefense, mod_MagicDefense, mod_Speed, mod_Evasion, mod_Accuracy, mod_CriticalRate;

    public MOD_COUNTER counter;

    public float PhysicalAttack {
        get {return mod_PhysAttack; }
    }

    public float PhysicalDefense {
        get { return mod_PhysDefense; }
    }

    public float MagicAttack {
        get { return mod_MagicAttack; }
    }

    public float MagicDefense {
        get { return mod_MagicDefense; }
    }

    public float Speed {
        get { return mod_Speed; }
    }

    public float Evasion {
        get { return mod_Evasion; }
    }

    public float Accuracy {
        get { return mod_Accuracy; }
    }

    public float CriticalRate {
        get { return mod_CriticalRate; }
    }

    public STAT_MODS(float buffs = 0) {
        mod_PhysAttack = 0;
        mod_PhysDefense = 0;
        mod_MagicAttack = 0;
        mod_MagicDefense = 0;
        mod_Speed = 0;
        mod_Evasion = 0;
        mod_Accuracy = 0;
        mod_CriticalRate = 0;
        counter = MOD_COUNTER.Zero;
    }

    public void Reset() {
        mod_PhysAttack = 0;
        mod_PhysDefense = 0;
        mod_MagicAttack = 0;
        mod_MagicDefense = 0;
        mod_Speed = 0;
        mod_Evasion = 0;
        mod_Accuracy = 0;
        mod_CriticalRate = 0;
        counter.Reset();
    }

    public void BuffPhysAtk(int modDuration = defaultModDuration) {
        if (mod_PhysAttack < 0) {
            mod_PhysAttack = 0;
            counter.PhysicalAttack = 0;
            return;
        }
        if (mod_PhysAttack > 0) {
            counter.PhysicalAttack += modDuration;
            return;
        }
        mod_PhysAttack = buffAmount;
        counter.PhysicalAttack = modDuration;
    }

    public void BuffPhysDef(int modDuration = defaultModDuration) {
        if (mod_PhysDefense < 0)
        {
            mod_PhysDefense = 0;
            counter.Defense = 0;
            return;
        }
        if (mod_PhysDefense > 0)
        {
            counter.Defense += modDuration;
            return;
        }
        mod_PhysDefense = buffAmount;
        counter.Defense = modDuration;
    }

    public void BuffMagicAtk(int modDuration = defaultModDuration) {
        if (mod_MagicAttack < 0)
        {
            mod_MagicAttack = 0;
            counter.MagicAttack = 0;
            return;
        }
        if (mod_MagicAttack > 0)
        {
            counter.MagicAttack += modDuration;
            return;
        }
        mod_MagicAttack = buffAmount;
        counter.MagicAttack = modDuration;
    }

    public void BuffMagicDefense(int modDuration = defaultModDuration) {
        if (mod_MagicDefense < 0)
        {
            mod_MagicDefense = 0;
            counter.MagicDefense = 0;
            return;
        }
        if (mod_MagicDefense > 0)
        {
            counter.MagicDefense += modDuration;
            return;
        }
        mod_MagicDefense = buffAmount;
        counter.MagicDefense = modDuration;
    }

    public void BuffSpeed(int modDuration = defaultModDuration) {
        if (mod_Speed < 0)
        {
            mod_Speed = 0;
            counter.Speed = 0;
            return;
        }
        if (mod_Speed > 0)
        {
            counter.Speed += modDuration;
            return;
        }
        mod_Speed = buffAmount + 0.5f;
        counter.Speed = modDuration;
    }

    public void BuffEvasion(int modDuration = defaultModDuration) {
        if (mod_Evasion < 0)
        {
            mod_Evasion = 0;
            counter.Evasion = 0;
            return;
        }
        if (mod_Evasion > 0)
        {
            counter.Evasion += modDuration;
            return;
        }
        mod_Evasion = buffAmount;
        counter.Evasion = modDuration;
    }

    public void BuffAccuracy(int modDuration = defaultModDuration) {
        if (mod_Accuracy < 0)
        {
            mod_Accuracy = 0;
            counter.Accuracy = 0;
            return;
        }
        if (mod_Accuracy > 0)
        {
            counter.Accuracy += modDuration;
            return;
        }
        mod_Accuracy = buffAmount;
        counter.Accuracy = modDuration;
    }

    public void BuffCriticalRate(int modDuration = defaultModDuration) {
        if (mod_CriticalRate < 0)
        {
            mod_CriticalRate = 0;
            counter.Critical = 0;
            return;
        }
        if (mod_CriticalRate > 0)
        {
            counter.Critical += modDuration;
            return;
        }
        mod_CriticalRate = buffAmount;
        counter.Critical = modDuration;
    }

    public void LowerPhysAtk(int modDuration = defaultModDuration)
    {
        if (mod_PhysAttack > 0)
        {
            mod_PhysAttack = 0;
            counter.PhysicalAttack = 0;
            return;
        }
        if (mod_PhysAttack < 0)
        {
            counter.PhysicalAttack += modDuration;
            return;
        }
        mod_PhysAttack = debuffAmount;
        counter.PhysicalAttack = modDuration;
    }

    public void LowerPhysDef(int modDuration = defaultModDuration)
    {
        if (mod_PhysDefense > 0)
        {
            mod_PhysDefense = 0;
            counter.Defense = 0;
            return;
        }
        if (mod_PhysDefense < 0)
        {
            counter.Defense += modDuration;
            return;
        }
        mod_PhysDefense = debuffAmount;
        counter.Defense = modDuration;
    }

    public void LowerMagicAtk(int modDuration = defaultModDuration)
    {
        if (mod_MagicAttack > 0)
        {
            mod_MagicAttack = 0;
            counter.MagicAttack = 0;
            return;
        }
        if (mod_MagicAttack < 0)
        {
            counter.MagicAttack += modDuration;
            return;
        }
        mod_MagicAttack = debuffAmount;
        counter.MagicAttack = modDuration;
    }

    public void LowerMagicDefense(int modDuration = defaultModDuration)
    {
        if (mod_MagicDefense > 0)
        {
            mod_MagicDefense = 0;
            counter.MagicDefense = 0;
            return;
        }
        if (mod_MagicDefense < 0)
        {
            counter.MagicDefense += modDuration;
            return;
        }
        mod_MagicDefense = debuffAmount;
        counter.MagicDefense = modDuration;
    }

    public void LowerSpeed(int modDuration = defaultModDuration)
    {
        if (mod_Speed > 0)
        {
            mod_Speed = 0;
            counter.Speed = 0;
            return;
        }
        if (mod_Speed < 0)
        {
            counter.Speed += modDuration;
            return;
        }
        mod_Speed = debuffAmount - 0.2f;
        counter.Speed = modDuration;
    }

    public void LowerEvasion(int modDuration = defaultModDuration)
    {
        if (mod_Evasion > 0)
        {
            mod_Evasion = 0;
            counter.Evasion = 0;
            return;
        }
        if (mod_Evasion < 0)
        {
            counter.Evasion += modDuration;
            return;
        }
        mod_Evasion = debuffAmount;
        counter.Evasion = modDuration;
    }

    public void LowerAccuracy(int modDuration = defaultModDuration)
    {
        if (mod_Accuracy > 0)
        {
            mod_Accuracy = 0;
            counter.Accuracy = 0;
            return;
        }
        if (mod_Accuracy < 0)
        {
            counter.Accuracy += modDuration;
            return;
        }
        mod_Accuracy = debuffAmount;
        counter.Accuracy = modDuration;
    }

    public void LowerCriticalRate(int modDuration = defaultModDuration)
    {
        if (mod_CriticalRate > 0)
        {
            mod_CriticalRate = 0;
            counter.Critical = 0;
            return;
        }
        if (mod_CriticalRate < 0)
        {
            counter.Critical += modDuration;
            return;
        }
        mod_CriticalRate = debuffAmount;
        counter.Critical = modDuration;
    }

    public void ModCountDown() {
        counter.ModDown();
        if (counter.PhysicalAttack == 0) {
            mod_PhysAttack = 0;
        }
        if (counter.Defense == 0)
        {
            mod_PhysDefense = 0;
        }
        if (counter.MagicAttack == 0)
        {
           mod_MagicAttack = 0;
        }
        if (counter.MagicDefense == 0)
        {
            mod_MagicDefense = 0;
        }
        if (counter.Speed == 0)
        {
            mod_Speed = 0;
        }
        if (counter.Evasion == 0)
        {
            mod_Evasion= 0;
        }
        if (counter.Accuracy == 0)
        {
            mod_Accuracy = 0;
        }
        if (counter.Critical == 0)
        {
            mod_CriticalRate = 0;
        }
    }
}

public class Fighter : MonoBehaviour
{
    public static int skillLimit = 8;

    //General Info
    [SerializeField]
    protected string _name = "Sample Fighter";
    public string FighterName {
        get { return _name; }
    }

    [SerializeField]
    protected Sprite icon;
    public Sprite Icon {
        get { return icon; }
    }

    [SerializeField]
    protected Sprite _profile;
    public Sprite Profile
    {
        get { return _profile; }
    }

    protected TEAM _team;
    public virtual TEAM team
    {
        get { return _team; }
    }

    protected FIGHTER_TYPE _typeOfFighter = FIGHTER_TYPE.BASIC;
    public virtual FIGHTER_TYPE Type
    {
        get { return _typeOfFighter; }
    }

    //The action used by the fighter when attacking
    [SerializeField]
    protected Action baseAttack;
    public virtual Action BaseAttack {
        get {
            if (baseAttack == null) {

                if (BattleController.S == null)
                {
                    return null;
                }

                return BattleController.S.defaultActions[0];
            }
            else {
                return baseAttack;
            }
        }
    }
    //The default type of the fighter's attack, can be changed by equipped weapon on conjuerer
    [SerializeField]
    protected ATTRIBUTE _baseAttackType = ATTRIBUTE.BASH;
    public virtual ATTRIBUTE AttackType {
        get {
            if (baseAttack != null)
            {
                return baseAttack.ActionAttribute;
            }
            else
            {
                return _baseAttackType;
            }
        }
    }

    [SerializeField]
    protected STATUS _status = STATUS.OK;
    public STATUS Status {
        get { return _status; }
        set { _status = value; }
    }

    //How effectively does this fighter resist status fx, 0 for no res, 100 for full resistance.
    [SerializeField]
    protected int statusResistance = 0;
    public int StatusResistance {
        get {
            if (AbilitiesInclude("Status Guard")) {
                return 100;
            }

            return statusResistance;
        }
    }
    //Fighter Stats
    //Base Stats
    [SerializeField]
    protected int baseHitPoints = 20, baseSkillPoints = 20, baseStrength = 20,
        baseDefense = 20, baseMagic = 20, baseAgility = 20, baseDexterity = 20;//Used to calculate stats
        //Affinity Bonuses
    [SerializeField]
    protected int affinityStrength, affinityDefense, affinityMagic, affinityAgility, affinityDexterity;

    public int AffinityStrength {
        get { return affinityStrength; }
    }
    public int AffinityDefense {
        get { return affinityDefense; }
    }
    public int AffinityMagic {
        get { return affinityMagic; }
    }
    public int AffinityAgility {
        get { return affinityAgility; }
    }
    public int AffinityDexterity {
        get { return affinityDexterity; }
    }

    public void Start()
    {
        PrintStats();
    }

    public void PrintStats()
    {
        Debug.Log("Fighter Stats Calculated : " + _name + " " + Stats.MaxHitPoints +
            " /" + Stats.MaxSkillPoints + " /" + Stats.Strength + " /" + Stats.Defense +
            " /" + Stats.Magic + " /" + Stats.Agility + " /" + Stats.Dexterity);
    }

    //Level is used to calculate stats
    [SerializeField]
    protected int _level = 1;
    public int Level {
        get { return _level; }
    }

    [SerializeField]
    protected RESISTANCES fighterResistances;
    [SerializeField]
    protected AFFINITIES fighterAffinities;

    public virtual STATS Stats
    {
        get {
            int hp = 0, sp = 0, str = 0, def = 0, mag = 0, agi = 0, dex = 0;
            int a_hp = 0, a_sp = 0, a_str = 0, a_def = 0, a_mag = 0, a_agi = 0, a_dex = 0;
            int p_hp = 100, p_sp = 100, p_str = 100, p_def = 100, p_mag = 100, p_agi = 100, p_dex = 100;

            hp = (5 + (baseHitPoints / 10) + ((_level * baseHitPoints) / 5)) * (p_hp / 100) + a_hp;
            sp = (5 + (baseSkillPoints / 10) + ((_level * baseSkillPoints) / 20) + (_level)) * (p_sp / 100) + a_sp;
            str = ((baseStrength / 10) + ((_level * baseStrength) / 50)) * (p_str / 100) + a_str;
            def = ((baseDefense / 10) + ((_level * baseDefense) / 50)) * (p_def / 100) + a_def;
            mag = ((baseMagic / 10) + ((_level * baseMagic) / 50)) * (p_mag / 100) + a_mag;
            agi = ((baseAgility / 10) + ((_level * baseAgility) / 50)) * (p_agi / 100) + a_agi;
            dex = ((baseDexterity / 10) + ((_level * baseDexterity) / 50)) * (p_dex / 100) + a_dex;

            return new STATS(hp, sp, str, def, mag, agi, dex);
        }
    }

    public virtual RESISTANCES Resistances {
        get {
            return new RESISTANCES(ResistanceCheck(ATTRIBUTE.CUT), ResistanceCheck(ATTRIBUTE.BASH), ResistanceCheck(ATTRIBUTE.PIERCE),
                ResistanceCheck(ATTRIBUTE.FIRE), ResistanceCheck(ATTRIBUTE.ICE), ResistanceCheck(ATTRIBUTE.ELEC),
                ResistanceCheck(ATTRIBUTE.WIND), ResistanceCheck(ATTRIBUTE.EARTH), ResistanceCheck(ATTRIBUTE.DARK));
            //return fighterResistances;
        }
    }

    public virtual AFFINITIES Affinities {
        get {
            int phys = fighterAffinities.Physical,
                mag = fighterAffinities.Magic,
                heal = fighterAffinities.Heal,
                cut = fighterAffinities.Cut,
                bash = fighterAffinities.Bash,
                pierce = fighterAffinities.Pierce,
                fire = fighterAffinities.Fire,
                ice = fighterAffinities.Ice,
                elec = fighterAffinities.Elec,
                wind = fighterAffinities.Wind,
                earth = fighterAffinities.Earth,
                dark = fighterAffinities.Dark;

            foreach (Ability s in Abilities)
            {
                phys += s.Affinities.Physical;
                mag += s.Affinities.Magic;
                heal += s.Affinities.Heal;
                cut += s.Affinities.Cut;
                bash += s.Affinities.Bash;
                pierce += s.Affinities.Pierce;
                fire += s.Affinities.Fire;
                ice += s.Affinities.Ice;
                elec += s.Affinities.Elec;
                wind += s.Affinities.Wind;
                earth += s.Affinities.Earth;
                dark += s.Affinities.Dark;
            }
            
            return new AFFINITIES(phys, mag, heal, cut, bash, pierce, fire, ice, elec, wind, earth, dark);
        }
    }

    [SerializeField]
    protected int damage = 0, usable_SP = 0;

    public int Damage
    {
        get
        {
            if (damage < 0)
            {
                return 0;
            }
            if (damage > Stats.MaxHitPoints)
            {
                return Stats.MaxHitPoints;
            }
            return damage;
        }

        set {
            if (value > Stats.MaxHitPoints) {
                damage = Stats.MaxHitPoints;
            }
            if (value < 0) {
                damage = 0;
            }
            damage = value;
        }
    }

    public int UsableSP {
        get {
            return usable_SP;
        }
        set {
            if (value > Stats.MaxSkillPoints)
            {
                usable_SP = Stats.MaxSkillPoints;
            } else if (value < 0) {
                usable_SP = 0;
            } else { usable_SP = value; }
        }
    }


    //STAT MODS
    public STAT_MODS stat_Mods;

    public bool guarding = false;

    //skills
    [SerializeField]
    protected List<Skill> _skillList; //skillList is the skills that a fighter can use
    public List<Skill> Skills {
        get { return _skillList; }
    }

    public List<Action> Actions {
        get {
            List<Action> actions = new List<Action>();
            int skillsChecked = 0;
            foreach (Skill s in Skills) {
                if (skillsChecked >= skillLimit) {
                    break;
                }
                if (s.SkillType == SKILL_TYPE.ACTION) {
                    actions.Add((Action)s);
                }
                skillsChecked++;
            }
            return actions;
        }
    }

    public virtual List<Ability> Abilities {
        get
        {
            List<Ability> abilities = new List<Ability>();
            int skillsChecked = 0;
            foreach (Skill s in Skills)
            {
                if (skillsChecked >= skillLimit){
                    break;
                }
                if (s != null) {
                    if (s.SkillType == SKILL_TYPE.ABILITY)
                    {
                        abilities.Add((Ability)s);
                    }
                }
                
            }
            return abilities;
        }
    }

    public bool AbilitiesInclude(string AbilityName) {
        bool abilityIncluded = false;

        foreach (Ability a in Abilities) {
            if (a.SkillName == AbilityName) {
                abilityIncluded = true;
            }
        }

        return abilityIncluded;
    }

    //Cancels all mods on fighter
    public void CancelMods() {
        stat_Mods.Reset();
        Debug.Log("Stat Mods cancelled on: " + _name + " LV." + _level);
    }

    public virtual void TakeDamage(int damageTaken = 1, ACTION_TYPE damageType = ACTION_TYPE.SPECIAL) {
        if (damageTaken < 0) {
            return;
        }

        damage += damageTaken;
        Debug.Log(_name + " took " + damageTaken + " damage!" );

        if (damage >= Stats.MaxHitPoints) {
            _status = STATUS.KO;
            damage = Stats.MaxHitPoints;
            stat_Mods.Reset();
            if (BattleController.S.timelineManager != null)
            {
                BattleController.S.timelineManager.KOCleanup();
            }
        }
    }

    public virtual void Heal(int healAmount) {
        if (healAmount < 0) {
            return;
        }
        damage -= healAmount;
        Debug.Log(_name + " healed " + healAmount + " damage!");
        if (damage < 0) {
            damage = 0;
        }
    }

    public void HPMaximize() {
        damage = 0;
    }

    public void SPRecover(int spToRecover = 1) {
        usable_SP += spToRecover;
        if (usable_SP > Stats.MaxSkillPoints) {
            usable_SP = Stats.MaxSkillPoints;
        }
        Debug.Log(_name + " gained " + spToRecover + " SP!");
        return;
    }

    public void SPDown(int spDown = 1) {
        usable_SP -= spDown;
        if (usable_SP < 0) {
            usable_SP = 0;
        }
        return;
    }

    public void SPMaximize() {
        usable_SP = Stats.MaxSkillPoints;
    }

    public void FullRecovery() {
        this.SPMaximize();
        this.HPMaximize();
    }

    public void Revive(float heal) {
        if (_status == STATUS.KO) {
            _status = STATUS.OK;
            damage = (int)(Stats.MaxHitPoints * (1 - heal));
        }
    }

    public void CureStatus() {
        if (_status != STATUS.OK && _status != STATUS.KO) {
            _status = STATUS.OK;
        }
    }

    public virtual Fighter TargetSelect() {
        if (BattleController.S != null)
        {
            int randomizer = Random.Range(0, BattleController.S.Fighters.Count);

            return BattleController.S.Fighters[randomizer];
        }
        else {
            return null;
        }
    }

    public virtual void FighterAI()
    {
        return;
    }

    //resets status mods and cleans up fighter mods
    public virtual void Cleanup() {
        stat_Mods.Reset();
        if (_status == STATUS.KO) {
            damage = Stats.MaxHitPoints - 1;
        }
        _status = STATUS.OK;
    }

    public virtual RESIST_STYLE ResistanceCheck(ATTRIBUTE attribute) {
        RESIST_STYLE resist = RESIST_STYLE.NORMAL;
        string styleName = "";

        switch (attribute) {
            case ATTRIBUTE.CUT:
                resist = fighterResistances.Cut;
                styleName = "Cut";
                break;
            case ATTRIBUTE.BASH:
                resist = fighterResistances.Bash;
                styleName = "Bash";
                break;
            case ATTRIBUTE.PIERCE:
                resist = fighterResistances.Pierce;
                styleName = "Pierce";
                break;
            case ATTRIBUTE.FIRE:
                resist = fighterResistances.Fire;
                styleName = "Fire";
                break;
            case ATTRIBUTE.ICE:
                resist = fighterResistances.Ice;
                styleName = "Ice";
                break;
            case ATTRIBUTE.ELEC:
                resist = fighterResistances.Elec;
                styleName = "Elec";
                break;
            case ATTRIBUTE.WIND:
                resist = fighterResistances.Wind;
                styleName = "Wind";
                break;
            case ATTRIBUTE.EARTH:
                resist = fighterResistances.Earth;
                styleName = "Earth";
                break;
            case ATTRIBUTE.DARK:
                resist = fighterResistances.Dark;
                styleName = "Dark";
                break;
        }

        //Checks if fighter has a skill of Resist Style X, depending on their resistance style it will be replaced
        if (AbilitiesInclude("Reflect " + styleName)) {
            if ((int)resist <= 3) {
                resist = RESIST_STYLE.REFLECT;
            }
        } else if (AbilitiesInclude("Drain " + styleName))
        {
            if ((int)resist <= 3)
            {
                resist = RESIST_STYLE.DRAIN;
            }
        } else if (AbilitiesInclude("Absorb " + styleName)) {
            if ((int)resist <= 3)
            {
                resist = RESIST_STYLE.ABSORB;
            }
        } else if (AbilitiesInclude("Null " + styleName))
        {
            if ((int)resist <= 2)
            {
                resist = RESIST_STYLE.NULL;
            }
        } else if (AbilitiesInclude("Resist " + styleName)) {
            if ((int)resist <= 2)
            {
                resist = RESIST_STYLE.RESIST;
            }
        }

        return resist;
    }
}

