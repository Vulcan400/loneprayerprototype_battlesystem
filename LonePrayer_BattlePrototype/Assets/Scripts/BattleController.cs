﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BC_Mode {
    EXECUTING,
    CHOOSE,
    TARGET,
    START,
    END,
    TAGGING
}

public class BattleController : MonoBehaviour
{
    static public BattleController S;//singleton

    public BC_Mode battleControllerMode = BC_Mode.START;
    public GameObject targetBoard;
    public Transform enemyAnchor1, enemyAnchor2, enemyAnchor3; //where to spawn enemies
    public ActionButton attackButton, guardButton; //basic buttons to use
    public Button checkButton, speedButton;
    public Sprite resistSprite, nullSprite, weakSprite, reflectSprite, absorbSprite, drainSprite;
    public TimelineManager timelineManager;
    public List<Action> defaultActions;//Contains Wait, Attack and 
    public List<Player_Fighter> player_party;
    public List<Enemy> enemy_party;
    public List<Fighter> Fighters; // List of Fighters
    public List<Transform> damageAnchors;
    public Fighter[] turnOrder; //Determines Turn Order
    public Fighter turnFighter; // stores the fighter whose turn it is
    public Fighter player_Taunter, enemy_Taunter;
    public int player_Taunt_Count = 0, enemy_Taunt_Count = 0;
    public List<ActionButton> actionButtons;
    public List<TargetButton> targetButtons;
    public Text targetInfoText;
    //public GameObject playerUI;//Use this to set player UI on or off
    public List<FighterPanel> fighterPanels;
    public List<Image> affinityPanels;
    public int battleSpeed = 1;
    public float animationDelay = 0.45f;//how long animations last
    public float notificationSpeed = 1;//how long notifications last
    public float damageSpeed = 0.5f; //how long damage floats up for

    public bool tagSet = false; //Allow for tag actions
    public List<Fighter> tagPasses = new List<Fighter>();//Tracks fighters who have acted in the tag chain
    public Fighter tagNext = null;
    public int tagChain = 0; //Length of tag Chain

    [SerializeField]
    private List<Animator> animationMatrix;

    [SerializeField]
    private DamageText numberNotifier;//prefab for telling damage.
    [SerializeField]
    private CheckNote checkNotifier;//prefab for showing crits/weak hits
    [SerializeField]
    private BattleMessenger messager;
    [SerializeField]
    private Image tagIndicator;
    [SerializeField]
    private float tagStart = 260;
    [SerializeField]
    private int turnIndex = 0;

    [SerializeField]
    private VictoryScreenPrototype victoryScreen;

    public Coroutine battleControls;

    void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        else {
            Destroy(this.gameObject);
        }

        if (victoryScreen == null) {
            victoryScreen = FindObjectOfType<VictoryScreenPrototype>();
        }

        battleControllerMode = BC_Mode.START;
    }

    void Start()
    {
        if (victoryScreen != null) {
            victoryScreen.gameObject.SetActive(false);
        }

        if (CheckScreenPrototype.S != null) {
            CheckScreenPrototype.S.gameObject.SetActive(false);
        }

        attackButton.AssignedAction = defaultActions[0];
        guardButton.AssignedAction = defaultActions[1];

        if (Mob.ActiveMob != null)
        {
            Mob.ActiveMob.SpawnEnemies();
        }
        FindFighters();

        foreach (Fighter f in player_party)
        {
            f.guarding = false;
            f.stat_Mods.Reset();
            f.HPMaximize();
            f.SPMaximize();
            f.Revive(1.0f);
            f.CureStatus();
        }

        messager.gameObject.SetActive(false);
        HideActions();
        HideTargets();
        //Turn Calculator
        OrderRound();

        foreach (Fighter f in turnOrder) {
            foreach (Ability a in f.Abilities) {
                if (a.AbilityType == ABILITY_TYPE.ON_ENTER) {
                    a.AbilityTrigger(f);
                }
            }
        }

        OrderRound();

        ExecuteTurn();
    }

    void Update()
    {
        if (battleControllerMode != BC_Mode.END) {
            CheckForResult();
        }
        
        switch (battleControllerMode) {
            case BC_Mode.CHOOSE:
                guardButton.enabled = true;
                attackButton.enabled = true;
                break;
            default:
                guardButton.enabled = false;
                attackButton.enabled = false;
                break;
        }

    }

    public void OpenCheckScreen() {
        if (CheckScreenPrototype.S != null && battleControllerMode == BC_Mode.CHOOSE) {
            CheckScreenPrototype.S.gameObject.SetActive(true);
            CheckScreenPrototype.S.infoPanel.gameObject.SetActive(false);
            CheckScreenPrototype.S.DisplayChoices();
            return;
        }
        Debug.Log("No Check Screen found");
    }

    public void OrderRound() {

        turnIndex = 0;

        if (Fighters.Count > 0)
        {
            List<Fighter> cueFighter = new List<Fighter>();

            foreach (Fighter f in Fighters)
            {
                if (f.Status != STATUS.KO) {
                    cueFighter.Add(f);
                }
            }

            turnOrder = new Fighter[Fighters.Count];

            for (int i = 0; i < Fighters.Count; i++)
            {
                Fighter topAgility = null;
                for (int s = 0; s < cueFighter.Count; s++) {
                    if (topAgility == null) {
                        topAgility = cueFighter[0];
                    }
                    //cuefighter has greater speed
                    if (cueFighter[s].Stats.Agility * (1 + cueFighter[s].stat_Mods.Speed) > topAgility.Stats.Agility * (1 + topAgility.stat_Mods.Speed)) {
                        if (!topAgility.guarding && !cueFighter[s].guarding)
                        {
                            topAgility = cueFighter[s];
                        }
                        else if (cueFighter[s].guarding && !topAgility.guarding)
                        {
                            topAgility = cueFighter[s];
                        }
                        else if (topAgility.guarding && cueFighter[s].guarding) {
                            topAgility = cueFighter[s];
                        }
                    }
                    //cuefighter has equal speed
                    else if (cueFighter[s].Stats.Agility * (1 + cueFighter[s].stat_Mods.Speed) == topAgility.Stats.Agility * (1 + topAgility.stat_Mods.Speed)) {
                        int randomizer = Random.Range(0, 2);
                        switch (randomizer) {
                            case 0:
                                if (!topAgility.guarding && !cueFighter[s].guarding)
                                {
                                    topAgility = cueFighter[s];
                                }
                                else if (cueFighter[s].guarding && !topAgility.guarding)
                                {
                                    topAgility = cueFighter[s];
                                }
                                break;
                            default:
                                if (cueFighter[s].guarding && !topAgility.guarding)
                                {
                                    topAgility = cueFighter[s];
                                }
                                break;
                        }
                    }
                    //cuefighter has lowerspeed
                    else if (cueFighter[s].Stats.Agility * (1 + cueFighter[s].stat_Mods.Speed) < topAgility.Stats.Agility * (1 + topAgility.stat_Mods.Speed)) {
                        if (cueFighter[s].guarding && !topAgility.guarding)
                        {
                            topAgility = cueFighter[s];
                        }
                    }

                    if (s == cueFighter.Count - 1) {
                        cueFighter.Remove(topAgility);
                    }
                }

                turnOrder[i] = topAgility;
            }

            foreach (Fighter f in Fighters) {
                f.guarding = false;
            }

            if (timelineManager != null) {
                timelineManager.UpdateTimelineTurns();
            }
            
        }
        
    }

    public void RoundEnd()
    {
        foreach (Fighter f in Fighters)
        {
            f.stat_Mods.ModCountDown();
        }

        if (player_Taunt_Count > 0) {
            player_Taunt_Count -= 1;
            if (player_Taunt_Count == 0) {
                player_Taunter = null;
            }
        }

        if (enemy_Taunt_Count > 0) {
            enemy_Taunt_Count -= 1;
            if (enemy_Taunt_Count == 0) {
                enemy_Taunter = null;
            }
        }

        OrderRound();
    }

    public void FindFighters() {
        player_party = new List<Player_Fighter>();
        Fighters = new List<Fighter>();

        foreach (FighterPanel f in fighterPanels) {
            f.gameObject.SetActive(false);
        }

        if (Party.S.leader != null) {
            player_party.Add(Party.S.leader);
            Fighters.Add(Party.S.leader);
            fighterPanels[0].gameObject.SetActive(true);
            fighterPanels[0].fighter = Party.S.leader;
            fighterPanels[0].UpdatePanel();
        }
        else
        {
            fighterPanels[0].gameObject.SetActive(false);
        }

        if (Party.S.spirit1 != null) {
            player_party.Add( Party.S.spirit1);
            Fighters.Add(Party.S.spirit1);
            fighterPanels[1].gameObject.SetActive(true);
            fighterPanels[1].fighter = Party.S.spirit1;
            fighterPanels[1].UpdatePanel();
        }
        else
        {
            fighterPanels[1].gameObject.SetActive(false);
        }

        if (Party.S.spirit2 != null)
        {
            player_party.Add(Party.S.spirit2);
            Fighters.Add(Party.S.spirit2);
            fighterPanels[2].gameObject.SetActive(true);
            fighterPanels[2].fighter = Party.S.spirit2;
            fighterPanels[2].UpdatePanel();
        }
        else {
            fighterPanels[2].gameObject.SetActive(false);
        }

        foreach (Fighter f in enemy_party) {
            Fighters.Add(f);
        }
    }

    //Display Player Actions
    public void DisplayActions(Player_Fighter character) {

        attackButton.gameObject.SetActive(true);
        attackButton.AssignedAction = character.BaseAttack;
        attackButton.SpriteAdapt();
        guardButton.gameObject.SetActive(true);
        speedButton.gameObject.SetActive(true);
        checkButton.gameObject.SetActive(true);
        foreach (ActionButton b in actionButtons) {
            b.gameObject.SetActive(false);
        }

        //int actionLimit = 6;
        int actionsRendered = 0;

        foreach (Action a in character.Actions) {
            if (actionsRendered >= actionButtons.Count) {
                battleControllerMode = BC_Mode.CHOOSE;
                return;
            }

            actionButtons[actionsRendered].gameObject.SetActive(true);
            actionButtons[actionsRendered].AssignedAction = a;
            actionButtons[actionsRendered].gameObject.GetComponentInChildren<Text>().text = a.SkillName + " [" + a.Cost + "]";
            actionButtons[actionsRendered].SpriteAdapt();

            if (character.UsableSP < a.Cost)
            {
                actionButtons[actionsRendered].enabled = false;
                actionButtons[actionsRendered].image.color = actionButtons[actionsRendered].colors.disabledColor;
                actionButtons[actionsRendered].SpriteAdapt();
            }
            else {
                actionButtons[actionsRendered].enabled = true;
                actionButtons[actionsRendered].image.color = actionButtons[actionsRendered].colors.normalColor;
                actionButtons[actionsRendered].SpriteAdapt();
            }
            ++actionsRendered;
        }
        battleControllerMode = BC_Mode.CHOOSE;
        return;
    }

    public void ChooseTag() {
        //TODO
        targetBoard.gameObject.SetActive(true);


        int targetsRendered = 0;
        foreach (TargetButton t in targetButtons) {
            t.gameObject.SetActive(false);
        }   
        
        targetInfoText.text = "Tag a Team Member";
        foreach (Player_Fighter p in player_party)
        {
            if (!tagPasses.Contains(p))
            {
                if (targetsRendered >= targetButtons.Count)
                {
                    break;
                }

                if (p.Status != STATUS.STUN && p.Status != STATUS.KO)
                {
                    targetButtons[targetsRendered].GetAction = null;
                    targetButtons[targetsRendered].gameObject.SetActive(true);
                    targetButtons[targetsRendered].AssignedFighter = p;
                    targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = p.FighterName;
                    affinityPanels[targetsRendered].gameObject.SetActive(false);
                }
            }
            ++targetsRendered;
        }  
        
        battleControllerMode = BC_Mode.TAGGING;
    }

    //Puts player into targeting mode, will auto_choose if only 1 target is applicable
    public void DisplayTargets(Action action) {
        const int targetLimit = 3;
        int targetsRendered = 0;

        targetBoard.gameObject.SetActive(true);

        foreach (TargetButton t in targetButtons)
        {
            t.gameObject.SetActive(false);
        }

        foreach (Image i in affinityPanels)
        {
            i.gameObject.SetActive(false);
        }

        targetsRendered = 0;

        switch (action.TargetMode) {
            case TARGET_TYPE.SELF:
                //Player_Act(action, turnFighter);
                targetInfoText.text = "Targets User";

                targetButtons[0].GetAction = action;
                targetButtons[0].gameObject.SetActive(true);
                targetButtons[0].AssignedFighter = turnFighter;
                targetButtons[0].gameObject.GetComponentInChildren<Text>().text = turnFighter.FighterName;

                battleControllerMode = BC_Mode.TARGET;
                break;
            case TARGET_TYPE.ALL_ALLY:
                //Player_Act(action, turnFighter);
                targetInfoText.text = "Targets all Allies";

                foreach (Player_Fighter p in player_party)
                {
                    if (targetsRendered >= targetLimit)
                    {
                        battleControllerMode = BC_Mode.TARGET;
                        break;
                    }
                    if (action.ActionType != ACTION_TYPE.REVIVE && p.Status != STATUS.KO)
                    {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = p;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = p.FighterName;
                    }
                    else if (action.ActionType == ACTION_TYPE.REVIVE && p.Status == STATUS.KO)
                    {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = p;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = p.FighterName;
                    }

                    ++targetsRendered;
                }

                battleControllerMode = BC_Mode.TARGET;

                break;
            case TARGET_TYPE.ALL_ENEMY:
                //Player_Act(action, turnFighter);
                targetInfoText.text = "Targets all Enemies";

                foreach (Enemy e in enemy_party)
                {
                    if (targetsRendered >= targetLimit)
                    {
                        battleControllerMode = BC_Mode.TARGET;
                        break;
                    }

                    if (e.Status != STATUS.KO)
                    {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = e;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = e.FighterName;

                        if (action.ActionType == ACTION_TYPE.MAGIC || action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.STATUS)
                        {
                            RESIST_STYLE resTarget = RESIST_STYLE.NORMAL;

                            switch (action.ActionAttribute)
                            {
                                case ATTRIBUTE.BASH:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Bash;
                                    break;
                                case ATTRIBUTE.CUT:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Cut;
                                    break;
                                case ATTRIBUTE.PIERCE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Pierce;
                                    break;
                                case ATTRIBUTE.FIRE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Fire;
                                    break;
                                case ATTRIBUTE.ICE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Ice;
                                    break;
                                case ATTRIBUTE.ELEC:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Elec;
                                    break;
                                case ATTRIBUTE.VARIABLE:
                                    switch (turnFighter.AttackType)
                                    {
                                        case ATTRIBUTE.BASH:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Bash;
                                            break;
                                        case ATTRIBUTE.CUT:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Cut;
                                            break;
                                        case ATTRIBUTE.PIERCE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Pierce;
                                            break;
                                        case ATTRIBUTE.FIRE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Fire;
                                            break;
                                        case ATTRIBUTE.ICE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Ice;
                                            break;
                                        case ATTRIBUTE.ELEC:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Elec;
                                            break;
                                    }
                                    break;
                            }

                            if (resTarget != RESIST_STYLE.NORMAL) {
                                affinityPanels[targetsRendered].gameObject.SetActive(true);
                                switch (resTarget)
                                {
                                    case RESIST_STYLE.WEAK:
                                        affinityPanels[targetsRendered].sprite = weakSprite;
                                        break;
                                    case RESIST_STYLE.RESIST:
                                        affinityPanels[targetsRendered].sprite = resistSprite;
                                        break;
                                    case RESIST_STYLE.NULL:
                                        affinityPanels[targetsRendered].sprite = nullSprite;
                                        break;
                                    case RESIST_STYLE.ABSORB:
                                        affinityPanels[targetsRendered].sprite = absorbSprite;
                                        break;
                                    case RESIST_STYLE.DRAIN:
                                        affinityPanels[targetsRendered].sprite = drainSprite;
                                        break;
                                    case RESIST_STYLE.REFLECT:
                                        affinityPanels[targetsRendered].sprite = reflectSprite;
                                        break;
                                }
                            }
                        }
                    }

                    ++targetsRendered;
                }

                battleControllerMode = BC_Mode.TARGET;

                break;
            case TARGET_TYPE.ALLY:
                /*if (player_party.Count <= 1) {
                    Player_Act(action, turnFighter, turnFighter);
                    break;
                }*/
                targetInfoText.text = "Targets an Ally";
                foreach (Player_Fighter p in player_party) {
                    if (targetsRendered >= targetLimit) {
                        battleControllerMode = BC_Mode.TARGET;
                        break;
                    }
                    if (action.ActionType != ACTION_TYPE.REVIVE && p.Status != STATUS.KO)
                    {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = p;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = p.FighterName;
                    }
                    else if (action.ActionType == ACTION_TYPE.REVIVE && p.Status == STATUS.KO) {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = p;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = p.FighterName;
                    }

                    ++targetsRendered;
                }

                battleControllerMode = BC_Mode.TARGET;
                break;
            case TARGET_TYPE.ENEMY:
                targetInfoText.text = "Targets an Enemy";

                foreach (Enemy e in enemy_party)
                {
                    if (targetsRendered >= targetLimit)
                    {
                        battleControllerMode = BC_Mode.TARGET;
                        break;
                    }

                    if (e.Status != STATUS.KO) {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = e;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = e.FighterName;

                        if (action.ActionType == ACTION_TYPE.MAGIC || action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.STATUS)
                        {
                            RESIST_STYLE resTarget = RESIST_STYLE.NORMAL;

                            switch (action.ActionAttribute)
                            {
                                case ATTRIBUTE.BASH:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Bash;
                                    break;
                                case ATTRIBUTE.CUT:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Cut;
                                    break;
                                case ATTRIBUTE.PIERCE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Pierce;
                                    break;
                                case ATTRIBUTE.FIRE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Fire;
                                    break;
                                case ATTRIBUTE.ICE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Ice;
                                    break;
                                case ATTRIBUTE.ELEC:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Elec;
                                    break;
                                case ATTRIBUTE.VARIABLE:
                                    switch (turnFighter.AttackType)
                                    {
                                        case ATTRIBUTE.BASH:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Bash;
                                            break;
                                        case ATTRIBUTE.CUT:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Cut;
                                            break;
                                        case ATTRIBUTE.PIERCE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Pierce;
                                            break;
                                        case ATTRIBUTE.FIRE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Fire;
                                            break;
                                        case ATTRIBUTE.ICE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Ice;
                                            break;
                                        case ATTRIBUTE.ELEC:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Elec;
                                            break;
                                    }
                                    break;
                            }

                            if (resTarget != RESIST_STYLE.NORMAL)
                            {
                                affinityPanels[targetsRendered].gameObject.SetActive(true);
                                switch (resTarget)
                                {
                                    case RESIST_STYLE.WEAK:
                                        affinityPanels[targetsRendered].sprite = weakSprite;
                                        break;
                                    case RESIST_STYLE.RESIST:
                                        affinityPanels[targetsRendered].sprite = resistSprite;
                                        break;
                                    case RESIST_STYLE.NULL:
                                        affinityPanels[targetsRendered].sprite = nullSprite;
                                        break;
                                    case RESIST_STYLE.ABSORB:
                                        affinityPanels[targetsRendered].sprite = absorbSprite;
                                        break;
                                    case RESIST_STYLE.DRAIN:
                                        affinityPanels[targetsRendered].sprite = drainSprite;
                                        break;
                                    case RESIST_STYLE.REFLECT:
                                        affinityPanels[targetsRendered].sprite = reflectSprite;
                                        break;
                                }
                            }

                        }
                    }

                    ++targetsRendered;
                }

                battleControllerMode = BC_Mode.TARGET;

                break;
            case TARGET_TYPE.RANDOM:
                //Player_Act(action, turnFighter);
                targetInfoText.text = "Targets Random Enemies";
                foreach (Enemy e in enemy_party)
                {
                    if (targetsRendered >= targetLimit)
                    {
                        battleControllerMode = BC_Mode.TARGET;
                        break;
                    }

                    if (e.Status != STATUS.KO)
                    {
                        targetButtons[targetsRendered].GetAction = action;
                        targetButtons[targetsRendered].gameObject.SetActive(true);
                        targetButtons[targetsRendered].AssignedFighter = e;
                        targetButtons[targetsRendered].gameObject.GetComponentInChildren<Text>().text = e.FighterName;

                        if (action.ActionType == ACTION_TYPE.MAGIC || action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.STATUS)
                        {
                            RESIST_STYLE resTarget = RESIST_STYLE.NORMAL;

                            switch (action.ActionAttribute)
                            {
                                case ATTRIBUTE.BASH:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Bash;
                                    break;
                                case ATTRIBUTE.CUT:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Cut;
                                    break;
                                case ATTRIBUTE.PIERCE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Pierce;
                                    break;
                                case ATTRIBUTE.FIRE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Fire;
                                    break;
                                case ATTRIBUTE.ICE:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Ice;
                                    break;
                                case ATTRIBUTE.ELEC:
                                    resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Elec;
                                    break;
                                case ATTRIBUTE.VARIABLE:
                                    switch (turnFighter.AttackType)
                                    {
                                        case ATTRIBUTE.BASH:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Bash;
                                            break;
                                        case ATTRIBUTE.CUT:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Cut;
                                            break;
                                        case ATTRIBUTE.PIERCE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Pierce;
                                            break;
                                        case ATTRIBUTE.FIRE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Fire;
                                            break;
                                        case ATTRIBUTE.ICE:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Ice;
                                            break;
                                        case ATTRIBUTE.ELEC:
                                            resTarget = targetButtons[targetsRendered].AssignedFighter.Resistances.Elec;
                                            break;
                                    }
                                    break;
                            }

                            if (resTarget != RESIST_STYLE.NORMAL)
                            {
                                affinityPanels[targetsRendered].gameObject.SetActive(true);
                                switch (resTarget)
                                {
                                    case RESIST_STYLE.WEAK:
                                        affinityPanels[targetsRendered].sprite = weakSprite;
                                        break;
                                    case RESIST_STYLE.RESIST:
                                        affinityPanels[targetsRendered].sprite = resistSprite;
                                        break;
                                    case RESIST_STYLE.NULL:
                                        affinityPanels[targetsRendered].sprite = nullSprite;
                                        break;
                                    case RESIST_STYLE.ABSORB:
                                        affinityPanels[targetsRendered].sprite = absorbSprite;
                                        break;
                                    case RESIST_STYLE.DRAIN:
                                        affinityPanels[targetsRendered].sprite = drainSprite;
                                        break;
                                    case RESIST_STYLE.REFLECT:
                                        affinityPanels[targetsRendered].sprite = reflectSprite;
                                        break;
                                }
                            }

                        }
                    }

                    ++targetsRendered;
                }

                battleControllerMode = BC_Mode.TARGET;
                break;
        }
    }

    public void HideTargets() {
        targetBoard.SetActive(false);
    }

    public void HideActions() {
        foreach (ActionButton b in actionButtons)
        {
            b.gameObject.SetActive(false);
        }
        attackButton.gameObject.SetActive(false);
        guardButton.gameObject.SetActive(false);
        speedButton.gameObject.SetActive(false);
        checkButton.gameObject.SetActive(false);
    }

    public void CancelTargeting() {
        HideTargets();
        battleControllerMode = BC_Mode.CHOOSE;
        tagSet = false;//In case tagging
        tagPasses.Clear();
        tagChain = 0;
    }

    public void ExecuteTurn() {
        if (turnIndex >= turnOrder.Length ) {
            RoundEnd();
        }

        turnFighter = turnOrder[turnIndex];

        if (turnFighter == null ||turnFighter.Status == STATUS.STUN || turnFighter.Status == STATUS.KO ) {
            battleControllerMode = BC_Mode.EXECUTING;
            battleControls = StartCoroutine(Skipturn());
            return;
        }

        if (turnFighter.team == TEAM.PLAYER)
        {
            DisplayActions((Player_Fighter)turnFighter);
            return;
        }
        else if (battleControllerMode != BC_Mode.END) {
            battleControllerMode = BC_Mode.EXECUTING;
            turnFighter.FighterAI();
            return;
        }
        return;

    }

    public void Player_Act(Action action, Fighter user) {
        HideActions();
        user.UsableSP -= action.Cost;
        switch (action.TargetMode)
        {
            case TARGET_TYPE.ALLY:
                battleControls = StartCoroutine(ExecuteAction(action,user,user));
                break;
            case TARGET_TYPE.ALL_ALLY:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
            case TARGET_TYPE.ENEMY:
                battleControls = StartCoroutine(ExecuteAction(defaultActions[1], user));
                break;
            case TARGET_TYPE.ALL_ENEMY:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
            case TARGET_TYPE.SELF:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
            case TARGET_TYPE.RANDOM:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
        }
    }

    public void Player_Act(Action action, Fighter user, Fighter target)
    {
        if (target == null) {
            battleControls = StartCoroutine(ExecuteAction(defaultActions[1], user));
            return;
        }

        HideActions();
        user.UsableSP -= action.Cost;

        switch (action.TargetMode)
        {
            case TARGET_TYPE.ALLY:
                battleControls = StartCoroutine(ExecuteAction(action, user, target));
                break;
            case TARGET_TYPE.ALL_ALLY:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
            case TARGET_TYPE.ENEMY:
                battleControls = StartCoroutine(ExecuteAction(action, user, target));
                break;
            case TARGET_TYPE.ALL_ENEMY:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
            case TARGET_TYPE.SELF:
                battleControls = StartCoroutine(ExecuteAction(action, user));
                break;
        }
    }

    public void DisplayNote(int number, Fighter target, FloatDisplay displayType) {
        Transform noteParent = null;

        for (int i = 0; i < Fighters.Count; i++) {
            if (Fighters[i] == target) {
                noteParent = damageAnchors[i];
            }
        }

        if (number >= 0)
        {
            GameObject NoteObject = Instantiate(numberNotifier.gameObject, noteParent);
            DamageText NoteText = NoteObject.gameObject.GetComponent<DamageText>();
            NoteText.floatDisplay = displayType;
            NoteText.Text.text = number.ToString();
        }
        else {
            switch (number) {
                case -3:
                    //Reflect
                    break;
                case -4:
                    //Absorb
                    break;
                case -5:
                    //Drain
                    break;
                default:
                    GameObject NoteObject = Instantiate(numberNotifier.gameObject, noteParent);
                    DamageText NoteText = NoteObject.gameObject.GetComponent<DamageText>();
                    NoteText.floatDisplay = displayType;
                    if (number == -2)
                    {
                        //Null
                        NoteText.Text.text = "NULL";
                    }
                    else {
                        //Miss
                        NoteText.Text.text = "Miss!";
                    }
                    
                    break;
            }
        }
    }

    public void DisplayNote(string text, Fighter target, FloatDisplay displayType)
    {
        Transform noteParent = null;

        for (int i = 0; i < Fighters.Count; i++)
        {
            if (Fighters[i] == target)
            {
                noteParent = damageAnchors[i];
            }
        }

        GameObject NoteObject = Instantiate(numberNotifier.gameObject, noteParent);
        DamageText NoteText = NoteObject.gameObject.GetComponent<DamageText>();
        NoteText.floatDisplay = displayType;
        NoteText.Text.text = text;
       
    }

    public void DisplayCheck(Fighter target, bool isCrit = false) {
        Transform checkParent = null;

        for (int i = 0; i < Fighters.Count; i++)
        {
            if (Fighters[i] == target)
            {
                checkParent = damageAnchors[i];
            }
        }

        GameObject obj = Instantiate(checkNotifier.gameObject, checkParent);
        CheckNote check = obj.gameObject.GetComponent<CheckNote>();
        check.isCrit = isCrit;
    }

    public void DisplayAnimation(Action action, Fighter target) {
        Transform animationParent = null;

        if (animationMatrix.Count <= 0) {
            Debug.Log("Unable to Animate");
            return;
        }

        Animator animator = animationMatrix[0];

        for (int i = 0; i < Fighters.Count; i++)
        {
            if (Fighters[i] == target)
            {
                animationParent = damageAnchors[i];
            }
        }

        switch (action.ActionType) {
            case ACTION_TYPE.PHYSICAL:
                switch (action.ActionAttribute)
                {
                    case ATTRIBUTE.CUT:
                        animator = animationMatrix[0];
                        break;
                    case ATTRIBUTE.BASH:
                        animator = animationMatrix[1];
                        break;
                    case ATTRIBUTE.PIERCE:
                        animator = animationMatrix[2];
                        break;
                    case ATTRIBUTE.FIRE:
                        animator = animationMatrix[3];
                        break;
                    case ATTRIBUTE.ICE:
                        animator = animationMatrix[4];
                        break;
                    case ATTRIBUTE.ELEC:
                        animator = animationMatrix[5];
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (turnFighter.AttackType)
                        {
                            case ATTRIBUTE.CUT:
                                animator = animationMatrix[0];
                                break;
                            case ATTRIBUTE.BASH:
                                animator = animationMatrix[1];
                                break;
                            case ATTRIBUTE.PIERCE:
                                animator = animationMatrix[2];
                                break;
                            case ATTRIBUTE.FIRE:
                                animator = animationMatrix[3];
                                break;
                            case ATTRIBUTE.ICE:
                                animator = animationMatrix[4];
                                break;
                            case ATTRIBUTE.ELEC:
                                animator = animationMatrix[5];
                                break;
                        }
                        break;
                    default:
                        animator = animationMatrix[6];
                        break;
                }
                break;
            case ACTION_TYPE.MAGIC:
                switch (action.ActionAttribute)
                {
                    case ATTRIBUTE.CUT:
                        animator = animationMatrix[0];
                        break;
                    case ATTRIBUTE.BASH:
                        animator = animationMatrix[1];
                        break;
                    case ATTRIBUTE.PIERCE:
                        animator = animationMatrix[2];
                        break;
                    case ATTRIBUTE.FIRE:
                        animator = animationMatrix[3];
                        break;
                    case ATTRIBUTE.ICE:
                        animator = animationMatrix[4];
                        break;
                    case ATTRIBUTE.ELEC:
                        animator = animationMatrix[5];
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (turnFighter.AttackType)
                        {
                            case ATTRIBUTE.CUT:
                                animator = animationMatrix[0];
                                break;
                            case ATTRIBUTE.BASH:
                                animator = animationMatrix[1];
                                break;
                            case ATTRIBUTE.PIERCE:
                                animator = animationMatrix[2];
                                break;
                            case ATTRIBUTE.FIRE:
                                animator = animationMatrix[3];
                                break;
                            case ATTRIBUTE.ICE:
                                animator = animationMatrix[4];
                                break;
                            case ATTRIBUTE.ELEC:
                                animator = animationMatrix[5];
                                break;
                        }
                        break;
                    default:
                        animator = animationMatrix[6];
                        break;
                }
                break;
            case ACTION_TYPE.HEAL:
                animator = animationMatrix[7];
                break;
            case ACTION_TYPE.STATUS:
                if (action.TargetMode == TARGET_TYPE.ALLY || action.TargetMode == TARGET_TYPE.ALL_ALLY || action.TargetMode == TARGET_TYPE.SELF)
                {
                    animator = animationMatrix[8];
                }
                else {
                    animator = animationMatrix[9];
                }
                break;
            case ACTION_TYPE.REVIVE:
                animator = animationMatrix[7];
                break;
            case ACTION_TYPE.CURE:
                animator = animationMatrix[7];
                break;
            case ACTION_TYPE.SPECIAL:
                switch (action.ActionAttribute)
                {
                    case ATTRIBUTE.CUT:
                        animator = animationMatrix[0];
                        break;
                    case ATTRIBUTE.BASH:
                        animator = animationMatrix[1];
                        break;
                    case ATTRIBUTE.PIERCE:
                        animator = animationMatrix[2];
                        break;
                    case ATTRIBUTE.FIRE:
                        animator = animationMatrix[3];
                        break;
                    case ATTRIBUTE.ICE:
                        animator = animationMatrix[4];
                        break;
                    case ATTRIBUTE.ELEC:
                        animator = animationMatrix[5];
                        break;
                    case ATTRIBUTE.VARIABLE:
                        switch (turnFighter.AttackType)
                        {
                            case ATTRIBUTE.CUT:
                                animator = animationMatrix[0];
                                break;
                            case ATTRIBUTE.BASH:
                                animator = animationMatrix[1];
                                break;
                            case ATTRIBUTE.PIERCE:
                                animator = animationMatrix[2];
                                break;
                            case ATTRIBUTE.FIRE:
                                animator = animationMatrix[3];
                                break;
                            case ATTRIBUTE.ICE:
                                animator = animationMatrix[4];
                                break;
                            case ATTRIBUTE.ELEC:
                                animator = animationMatrix[5];
                                break;
                        }
                        break;
                    default:
                        animator = animationMatrix[6];
                        break;
                }
                break;
            default:
                animator = animationMatrix[6];
                break;
        }

        GameObject AnimObject = Instantiate(animator.gameObject, animationParent);
        Animator Animation = AnimObject.gameObject.GetComponent<Animator>();
        Animation.speed = battleSpeed;
    }

    public void ChangeBattleSpeed() {
        if (S.battleSpeed == 1)
        {
            S.battleSpeed = 2;
        }
        else if (S.battleSpeed == 2)
        {
            S.battleSpeed = 3;
        }
        else {
            S.battleSpeed = 1;
        }
    }

    //For Autotarget Actions
    public IEnumerator ExecuteAction(Action action, Fighter user) {
        messager.gameObject.SetActive(true);
        messager.Message = user.FighterName + " used " + action.SkillName;
        yield return new WaitForSeconds(1.0f/battleSpeed);
        messager.gameObject.SetActive(false);

        switch (action.ActionType)
        {
            case ACTION_TYPE.STATUS:
                switch (action.TargetMode)
                {
                    case TARGET_TYPE.ALL_ALLY:
                        
                        switch (user.team) {
                            case TEAM.PLAYER:
                                foreach (Fighter f in player_party)
                                {

                                    if (f.Status != STATUS.KO)
                                    {
                                        DisplayAnimation(action, f);
                                        action.Action_Effect(user, f);
                                    }
                                }
                                break;
                            case TEAM.ENEMY:
                                foreach (Fighter f in enemy_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        DisplayAnimation(action, f);
                                        action.Action_Effect(user, f);
                                    }
                                }
                                break;
                            default:
                                break;
                        }

                        break;
                    case TARGET_TYPE.ALL_ENEMY:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                foreach (Fighter f in enemy_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        DisplayAnimation(action, f);
                                        action.Action_Effect(user, f);
                                    }
                                }
                                break;
                            case TEAM.ENEMY:
                                foreach (Fighter f in player_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        DisplayAnimation(action, f);
                                        action.Action_Effect(user, f);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case TARGET_TYPE.SELF:
                        action.Action_Effect(user, user);
                        DisplayAnimation(action, user);
                        break;
                    case TARGET_TYPE.RANDOM:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    int randomizer = Random.Range(0, enemy_party.Count);

                                    while (enemy_party[randomizer].Status == STATUS.KO && battleControllerMode != BC_Mode.END)
                                    {
                                        randomizer = Random.Range(0, enemy_party.Count);
                                    }

                                    action.Action_Effect(user, enemy_party[randomizer]);

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    int randomizer = Random.Range(0, player_party.Count);

                                    while (player_party[randomizer].Status == STATUS.KO && battleControllerMode != BC_Mode.END)
                                    {
                                        randomizer = Random.Range(0, player_party.Count);
                                    }

                                    action.Action_Effect(user, player_party[randomizer]);

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }
                break;
            case ACTION_TYPE.CURE:
                switch (action.TargetMode) {
                    case TARGET_TYPE.ALL_ALLY:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                foreach (Fighter f in player_party) {
                                    DisplayAnimation(action, f);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    f.CureStatus();
                                    action.Action_Effect(user, f);
                                }
                                break;
                            case TEAM.ENEMY:
                                foreach (Fighter f in enemy_party)
                                {
                                    DisplayAnimation(action, f);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    f.CureStatus();
                                    action.Action_Effect(user, f);
                                }
                                break;
                            case TEAM.ALLY:
                                foreach (Fighter f in player_party)
                                {
                                    DisplayAnimation(action, f);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    f.CureStatus();
                                    action.Action_Effect(user, f);
                                }
                                user.CureStatus();
                                action.Action_Effect(user,user);
                                break;
                        }
                        break;
                }
                break;
            case ACTION_TYPE.HEAL:
                int healAmount = 0;
                switch (action.TargetMode)
                {
                    case TARGET_TYPE.ALL_ALLY:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                foreach (Fighter f in player_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        healAmount = action.Act_Heal(f, user);
                                        DisplayAnimation(action, f);
                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                        DisplayNote(healAmount, f, FloatDisplay.HEAL);
                                        f.Heal(healAmount);
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;

                            case TEAM.ENEMY:
                                foreach (Fighter f in enemy_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        healAmount = action.Act_Heal(f, user);
                                        DisplayAnimation(action, f);
                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                        DisplayNote(healAmount, f, FloatDisplay.HEAL);
                                        f.Heal(healAmount);
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                                
                            default:
                                break;
                        }

                        break;
                    case TARGET_TYPE.ALL_ENEMY:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                foreach (Fighter f in player_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        healAmount = action.Act_Heal(f, user);
                                        DisplayNote(healAmount, f, FloatDisplay.HEAL);
                                        f.Heal(healAmount);
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;

                            case TEAM.ENEMY:
                                foreach (Fighter f in enemy_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        healAmount = action.Act_Heal(f, user);
                                        DisplayNote(healAmount, f, FloatDisplay.HEAL);
                                        f.Heal(healAmount);
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;

                            default:
                                break;
                        }

                        break;
                    case TARGET_TYPE.SELF:
                        healAmount = action.Act_Heal(user, user);
                        DisplayAnimation(action, user);
                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                        DisplayNote(healAmount, user, FloatDisplay.DAMAGE);
                        user.Heal(healAmount);

                        yield return new WaitForSeconds(damageSpeed / battleSpeed);
                        break;

                    case TARGET_TYPE.RANDOM:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                foreach (Fighter f in player_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        healAmount = action.Act_Heal(f, user);
                                        DisplayAnimation(action, f);
                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                        DisplayNote(healAmount, f, FloatDisplay.HEAL);
                                        f.Heal(healAmount);
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;

                            case TEAM.ENEMY:
                                foreach (Fighter f in enemy_party)
                                {
                                    if (f.Status != STATUS.KO)
                                    {
                                        healAmount = action.Act_Heal(f, user);
                                        DisplayAnimation(action, f);
                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                        DisplayNote(healAmount, f, FloatDisplay.HEAL);
                                        f.Heal(healAmount);
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;

                            default:
                                break;
                        }
                        break;
                }
                break;
            case ACTION_TYPE.PHYSICAL:
                int damageToDealPhys = 0;
                switch (action.TargetMode)
                {
                    case TARGET_TYPE.ALL_ALLY:
                        switch (user.team) {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in player_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            damageToDealPhys = action.CalcDamage(f, user);
                                            DisplayAnimation(action, f);
                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            DisplayNote(damageToDealPhys, f, FloatDisplay.DAMAGE);
                                            f.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in enemy_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            damageToDealPhys = action.CalcDamage(f, user);
                                            DisplayAnimation(action, f);
                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            DisplayNote(damageToDealPhys, f, FloatDisplay.DAMAGE);
                                            f.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            default:
                                break;
                        }

                        break;
                    case TARGET_TYPE.ALL_ENEMY:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in enemy_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            if (enemy_Taunter != null && enemy_Taunter.Status != STATUS.KO) {

                                                damageToDealPhys = action.CalcDamage(enemy_Taunter, user);
                                                if (enemy_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in enemy_Taunter.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(enemy_Taunter, damageToDealPhys, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(enemy_Taunter, damageToDealPhys, action.ActionAttribute);
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                                DisplayAnimation(action, enemy_Taunter);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealPhys, enemy_Taunter, FloatDisplay.DAMAGE);
                                                enemy_Taunter.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);

                                                if (enemy_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in enemy_Taunter.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = enemy_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(enemy_Taunter, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = enemy_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(enemy_Taunter, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }

                                            } else {
                                                damageToDealPhys = action.CalcDamage(f, user);
                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(f, damageToDealPhys, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(f, damageToDealPhys, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                DisplayAnimation(action, f);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealPhys, f, FloatDisplay.DAMAGE);
                                                f.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);


                                                if (f.Status != STATUS.KO) {

                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(f, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        } else if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(f, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in player_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            if (player_Taunter != null && player_Taunter.Status != STATUS.KO)
                                            {

                                                damageToDealPhys = action.CalcDamage(player_Taunter, user);
                                                if (player_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in player_Taunter.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(player_Taunter, damageToDealPhys, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(player_Taunter, damageToDealPhys, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                DisplayAnimation(action, player_Taunter);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealPhys, player_Taunter, FloatDisplay.DAMAGE);
                                                player_Taunter.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);

                                                if (player_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in player_Taunter.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = player_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(player_Taunter, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = player_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(player_Taunter, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }

                                            }
                                            else {

                                                damageToDealPhys = action.CalcDamage(f, user);
                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(f, damageToDealPhys, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealPhys = a.DamageAbsorb(f, damageToDealPhys, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                DisplayAnimation(action, f);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealPhys, f, FloatDisplay.DAMAGE);
                                                f.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);

                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(f, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(f, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            default:
                                break;
                        }

                        break;
                    case TARGET_TYPE.SELF:
                        for (int i = 0; i < action.Hits; i++)
                        {
                            damageToDealPhys = action.CalcDamage(user, user);
                            DisplayAnimation(action, user);
                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                            DisplayNote(damageToDealPhys, user, FloatDisplay.DAMAGE);
                            user.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);
                            yield return new WaitForSeconds(damageSpeed / battleSpeed);
                        }
                        //action.Action_Effect(user, user);
                        break;
                    case TARGET_TYPE.RANDOM:
                        Fighter target = null;
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    int randomizer = Random.Range(0, enemy_party.Count);

                                    while (enemy_party[randomizer].Status == STATUS.KO && battleControllerMode != BC_Mode.END)
                                    {
                                        randomizer = Random.Range(0, enemy_party.Count);
                                    }

                                    target = enemy_party[randomizer];

                                    if (enemy_Taunter != null && enemy_Taunter.Status != STATUS.KO) {
                                        target = enemy_Taunter;
                                    }

                                    damageToDealPhys = action.CalcDamage(target, user);
                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                                            {
                                                if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                {
                                                    damageToDealPhys = a.DamageAbsorb(target, damageToDealPhys, user.AttackType);
                                                }
                                                else
                                                {
                                                    damageToDealPhys = a.DamageAbsorb(target, damageToDealPhys, action.ActionAttribute);
                                                }
                                            }
                                        }
                                    }
                                    DisplayAnimation(action, target);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    DisplayNote(damageToDealPhys, target, FloatDisplay.DAMAGE);
                                    target.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);

                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.TriggerBased)
                                            {
                                                if (Random.Range(0, 100) < a.TriggerRate)
                                                {
                                                    if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                    {
                                                        messager.gameObject.SetActive(true);
                                                        messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                        messager.gameObject.SetActive(false);
                                                        a.DamageTrigger(target, user);
                                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                    }
                                                }
                                            } else if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                            {
                                                messager.gameObject.SetActive(true);
                                                messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                messager.gameObject.SetActive(false);
                                                a.DamageTrigger(target, user);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    int randomizer = Random.Range(0, player_party.Count);

                                    while (player_party[randomizer].Status == STATUS.KO && battleControllerMode != BC_Mode.END)
                                    {
                                        randomizer = Random.Range(0, player_party.Count);
                                    }

                                    target = player_party[randomizer];

                                    if (player_Taunter != null && player_Taunter.Status != STATUS.KO)
                                    {
                                        target = player_Taunter;
                                    }

                                    damageToDealPhys = action.CalcDamage(target, user);
                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                                            {
                                                if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                {
                                                    damageToDealPhys = a.DamageAbsorb(target, damageToDealPhys, user.AttackType);
                                                }
                                                else
                                                {
                                                    damageToDealPhys = a.DamageAbsorb(target, damageToDealPhys, action.ActionAttribute);
                                                }
                                            }
                                        }
                                    }
                                    DisplayAnimation(action, target);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    DisplayNote(damageToDealPhys, target, FloatDisplay.DAMAGE);
                                    target.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);

                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.TriggerBased)
                                            {
                                                if (Random.Range(0, 100) < a.TriggerRate)
                                                {
                                                    if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                    {
                                                        messager.gameObject.SetActive(true);
                                                        messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                        messager.gameObject.SetActive(false);
                                                        a.DamageTrigger(target, user);
                                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                    }
                                                }
                                            } else if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                            {
                                                messager.gameObject.SetActive(true);
                                                messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                messager.gameObject.SetActive(false);
                                                a.DamageTrigger(target, user);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            }
                                        }
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            default:
                                break;
                        }

                        break;
                }

                if (user.Status != STATUS.KO)
                {
                    foreach (Ability a in user.Abilities)
                    {
                        if (a.TriggerBased)
                        {
                            if (Random.Range(0, 100) < a.TriggerRate)
                            {
                                if (a.AbilityType == ABILITY_TYPE.ON_ATK_END)
                                {
                                    messager.gameObject.SetActive(true);
                                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                    messager.gameObject.SetActive(false);
                                    a.AbilityTrigger(user);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                }
                            }
                        } else if (a.AbilityType == ABILITY_TYPE.ON_ATK_END)
                        {
                            messager.gameObject.SetActive(true);
                            messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                            messager.gameObject.SetActive(false);
                            a.AbilityTrigger(user);
                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                        }
                    }
                }

                break;
            case ACTION_TYPE.MAGIC:
                int damageToDealMag = 0;
                switch (action.TargetMode)
                {
                    case TARGET_TYPE.ALL_ALLY:

                        switch (user.team) {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in player_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            damageToDealMag = action.CalcDamage(f, user);
                                            DisplayAnimation(action, f);
                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            DisplayNote(damageToDealMag, f, FloatDisplay.DAMAGE);
                                            f.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);
                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in enemy_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            damageToDealMag = action.CalcDamage(f, user);
                                            DisplayAnimation(action, f);
                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            DisplayNote(damageToDealMag, f, FloatDisplay.DAMAGE);
                                            f.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);
                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            default:
                                break;
                        }

                        break;
                    case TARGET_TYPE.ALL_ENEMY:
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in enemy_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            if (enemy_Taunter != null && enemy_Taunter.Status != STATUS.KO)
                                            {

                                                damageToDealMag = action.CalcDamage(enemy_Taunter, user);
                                                if (enemy_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in enemy_Taunter.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(enemy_Taunter, damageToDealMag, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(enemy_Taunter, damageToDealMag, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                DisplayAnimation(action, enemy_Taunter);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealMag, enemy_Taunter, FloatDisplay.DAMAGE);
                                                enemy_Taunter.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                                                if (enemy_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in enemy_Taunter.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = enemy_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(enemy_Taunter, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = enemy_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(enemy_Taunter, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }

                                            }
                                            else
                                            {

                                                damageToDealMag = action.CalcDamage(f, user);
                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(f, damageToDealMag, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(f, damageToDealMag, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                DisplayAnimation(action, f);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealMag, f, FloatDisplay.DAMAGE);
                                                f.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(f, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(f, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    foreach (Fighter f in player_party)
                                    {
                                        if (f.Status != STATUS.KO)
                                        {
                                            if (player_Taunter != null && player_Taunter.Status != STATUS.KO)
                                            {

                                                damageToDealPhys = action.CalcDamage(player_Taunter, user);
                                                DisplayAnimation(action, player_Taunter);
                                                if (player_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in player_Taunter.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(player_Taunter, damageToDealMag, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(player_Taunter, damageToDealMag, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealMag, player_Taunter, FloatDisplay.DAMAGE);
                                                player_Taunter.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                                                if (player_Taunter.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in player_Taunter.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = player_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(player_Taunter, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = player_Taunter.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(player_Taunter, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }

                                            }
                                            else
                                            {

                                                damageToDealMag = action.CalcDamage(f, user);
                                                DisplayAnimation(action, f);
                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                                                        {
                                                            if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(f, damageToDealMag, user.AttackType);
                                                            }
                                                            else
                                                            {
                                                                damageToDealMag = a.DamageAbsorb(f, damageToDealMag, action.ActionAttribute);
                                                            }
                                                        }
                                                    }
                                                }
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                DisplayNote(damageToDealMag, f, FloatDisplay.DAMAGE);
                                                f.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                                                if (f.Status != STATUS.KO)
                                                {
                                                    foreach (Ability a in f.Abilities)
                                                    {
                                                        if (a.TriggerBased)
                                                        {
                                                            if (Random.Range(0, 100) < a.TriggerRate)
                                                            {
                                                                if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                                {
                                                                    messager.gameObject.SetActive(true);
                                                                    messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                                    messager.gameObject.SetActive(false);
                                                                    a.DamageTrigger(f, user);
                                                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                                }
                                                            }
                                                        }
                                                        else if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                        {
                                                            messager.gameObject.SetActive(true);
                                                            messager.Message = f.FighterName + "'s " + a.SkillName + " activated!";
                                                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                            messager.gameObject.SetActive(false);
                                                            a.DamageTrigger(f, user);
                                                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case TARGET_TYPE.SELF:
                        for (int i = 0; i < action.Hits; i++)
                        {
                            damageToDealMag = action.CalcDamage(user, user);
                            DisplayAnimation(action, user);
                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                            DisplayNote(damageToDealMag, user, FloatDisplay.DAMAGE);
                            user.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                            yield return new WaitForSeconds(damageSpeed / battleSpeed);
                        }
                        //action.Action_Effect(user, user);
                        break;
                    case TARGET_TYPE.RANDOM:
                        Fighter target = null;
                        switch (user.team)
                        {
                            case TEAM.PLAYER:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    int randomizer = Random.Range(0, enemy_party.Count);

                                    while (enemy_party[randomizer].Status == STATUS.KO && battleControllerMode != BC_Mode.END)
                                    {
                                        randomizer = Random.Range(0, enemy_party.Count);
                                    }

                                    target = enemy_party[randomizer];

                                    if (enemy_Taunter != null && enemy_Taunter.Status != STATUS.KO)
                                    {
                                        target = enemy_Taunter;
                                    }

                                    damageToDealMag = action.CalcDamage(target, user);
                                    DisplayAnimation(action, target);
                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                                            {
                                                if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                {
                                                    damageToDealMag = a.DamageAbsorb(target, damageToDealMag, user.AttackType);
                                                }
                                                else
                                                {
                                                    damageToDealMag = a.DamageAbsorb(target, damageToDealMag, action.ActionAttribute);
                                                }
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    DisplayNote(damageToDealMag, target, FloatDisplay.DAMAGE);
                                    target.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.TriggerBased)
                                            {
                                                if (Random.Range(0, 100) < a.TriggerRate)
                                                {
                                                    if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                    {
                                                        messager.gameObject.SetActive(true);
                                                        messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                        messager.gameObject.SetActive(false);
                                                        a.DamageTrigger(target, user);
                                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                    }
                                                }
                                            }
                                            else if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                            {
                                                messager.gameObject.SetActive(true);
                                                messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                messager.gameObject.SetActive(false);
                                                a.DamageTrigger(target, user);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            }
                                        }
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                            case TEAM.ENEMY:
                                for (int i = 0; i < action.Hits; i++)
                                {
                                    int randomizer = Random.Range(0, player_party.Count);

                                    while (player_party[randomizer].Status == STATUS.KO && battleControllerMode != BC_Mode.END)
                                    {
                                        randomizer = Random.Range(0, player_party.Count);
                                    }

                                    target = player_party[randomizer];

                                    if (player_Taunter != null && player_Taunter.Status != STATUS.KO)
                                    {
                                        target = player_Taunter;
                                    }

                                    damageToDealMag = action.CalcDamage(target, user);
                                    DisplayAnimation(action, target);
                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                                            {
                                                if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                                {
                                                    damageToDealMag = a.DamageAbsorb(target, damageToDealMag, user.AttackType);
                                                }
                                                else
                                                {
                                                    damageToDealMag = a.DamageAbsorb(target, damageToDealMag, action.ActionAttribute);
                                                }
                                            }
                                        }
                                    }
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                    DisplayNote(damageToDealMag, target, FloatDisplay.DAMAGE);
                                    target.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                                    if (target.Status != STATUS.KO)
                                    {
                                        foreach (Ability a in target.Abilities)
                                        {
                                            if (a.TriggerBased)
                                            {
                                                if (Random.Range(0, 100) < a.TriggerRate)
                                                {
                                                    if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                                    {
                                                        messager.gameObject.SetActive(true);
                                                        messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                        messager.gameObject.SetActive(false);
                                                        a.DamageTrigger(target, user);
                                                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                                                    }
                                                }
                                            }
                                            else if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                                            {
                                                messager.gameObject.SetActive(true);
                                                messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                                messager.gameObject.SetActive(false);
                                                a.DamageTrigger(target, user);
                                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                                            }
                                        }
                                    }

                                    yield return new WaitForSeconds(damageSpeed / battleSpeed);
                                }
                                break;
                        }
                        break;
                    }

                if (user.Status != STATUS.KO)
                {
                    foreach (Ability a in user.Abilities)
                    {
                        if (a.TriggerBased)
                        {
                            if (Random.Range(0, 100) < a.TriggerRate)
                            {
                                if (a.AbilityType == ABILITY_TYPE.ON_MAG_END)
                                {
                                    messager.gameObject.SetActive(true);
                                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                    messager.gameObject.SetActive(false);
                                    a.AbilityTrigger(user);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                }
                            }
                        }
                        else if (a.AbilityType == ABILITY_TYPE.ON_MAG_END)
                        {
                            messager.gameObject.SetActive(true);
                            messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                            yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                            messager.gameObject.SetActive(false);
                            a.AbilityTrigger(user);
                            yield return new WaitForSeconds(animationDelay / battleSpeed);
                        }
                    }
                }
                break;
        }

        if (action.ActivateGuard)
        {
            user.guarding = true;
        }

        if (action.ActivateTaunt)
        {
            switch (user.team)
            {
                case TEAM.PLAYER:
                    player_Taunter = user;
                    player_Taunt_Count = 2;
                    break;
                case TEAM.ENEMY:
                    enemy_Taunter = user;
                    enemy_Taunt_Count = 2;
                    break;
            }
        }

        foreach (Ability a in user.Abilities) {
            if (a.AbilityType == ABILITY_TYPE.ON_ACT_END)
            {
                if (a.TriggerBased)
                {
                    if (Random.Range(0, 100) < a.TriggerRate)
                    {
                        messager.gameObject.SetActive(true);
                        messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                        messager.gameObject.SetActive(false);
                        a.AbilityTrigger(user);
                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                    }
                }
                else{
                    messager.gameObject.SetActive(true);
                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                    messager.gameObject.SetActive(false);
                    a.AbilityTrigger(user);
                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                }
                    
            }

            if (a.AbilityType == ABILITY_TYPE.ON_GUARD && user.guarding) {
                if (a.TriggerBased)
                {
                    if (Random.Range(0, 100) < a.TriggerRate)
                    {
                        messager.gameObject.SetActive(true);
                        messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                        messager.gameObject.SetActive(false);
                        a.AbilityTrigger(user);
                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                    }
                }
                else {
                    messager.gameObject.SetActive(true);
                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                    messager.gameObject.SetActive(false);
                    a.AbilityTrigger(user);
                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                }
            }
        }

        //Manage Tags
        int validTags = 0;
        Debug.Log("Checking for valid tags");
        if (user.team == TEAM.PLAYER) {
            foreach (Fighter f in player_party)
            {
                if (!tagPasses.Contains(f) && f != user)
                {
                    if (f.Status != STATUS.STUN && f.Status != STATUS.KO)
                    {
                        validTags++;
                    }
                }
            }
        }
        

        if (tagSet && user.team == TEAM.PLAYER && !tagPasses.Contains(user) && validTags > 0 /*&& tagChain < (player_party.Count - 1)*/)
        {
            //TODO
            if (user.Status == STATUS.KO)
            {
                tagSet = false; // In case user died due to counter
                tagPasses.Clear();
            }
            else
            {
                tagPasses.Add(user);//Keep Track of User
                //Set turn fighter and do not pass through
                //Tag Indication In
                tagIndicator.rectTransform.localPosition = new Vector2(tagStart, tagIndicator.rectTransform.localPosition.y);
                float addAmount = tagStart / 20.0f; // amount the indicator increments
                while (tagIndicator.rectTransform.localPosition.x > 0)
                {
                    tagIndicator.rectTransform.localPosition = new Vector2(tagIndicator.rectTransform.localPosition.x - addAmount, tagIndicator.rectTransform.localPosition.y);
                    yield return new WaitForSeconds(1 / 120.0f/ battleSpeed);
                }

                ChooseTag();
                Debug.Log("Choosing TagTeam");
                while (tagSet)
                {
                    //Set false on tag decision
                    yield return new WaitForSeconds(1 / 60.0f);
                }

                //Tag Indicator Out
                while (tagIndicator.rectTransform.localPosition.x > -tagStart)
                {
                    tagIndicator.rectTransform.localPosition = new Vector2(tagIndicator.rectTransform.localPosition.x - addAmount, tagIndicator.rectTransform.localPosition.y);
                    yield return new WaitForSeconds(1 / 120.0f/battleSpeed);
                }

                Debug.Log("Tagteam Chosen");
                if (tagNext != null)
                {
                    turnOrder[turnIndex] = tagNext;
                    turnIndex--;
                    tagNext = null;
                    tagChain++;
                }
                else
                {
                    tagPasses.Clear();
                    tagChain = 0;
                }
                //Open Target menu to select a team member, have them act
                //Make them next on the timeline without updating the timeline
            }
        }
        else
        {
            tagPasses.Clear();
            tagChain = 0;
        }
        tagSet = false;
        //Manage Tags End

        if (user.Status == STATUS.CURSE)
        {
            //Take Curse damage
            int curseDamage = user.Stats.MaxHitPoints / Random.Range(12, 16);
            user.TakeDamage( curseDamage, ACTION_TYPE.STATUS);
            DisplayNote(curseDamage, user, FloatDisplay.DAMAGE);
            yield return new WaitForSeconds(animationDelay / battleSpeed);
        }

        if (battleControllerMode != BC_Mode.END)
        {
            ++turnIndex;
            if (timelineManager != null)
            {
                timelineManager.MoveUp(turnIndex);
            }
            ExecuteTurn();
        }
        yield break;
    }

    //For Targeting Actions
    public IEnumerator ExecuteAction(Action action, Fighter user, Fighter target) {

        messager.gameObject.SetActive(true);
        messager.Message = user.FighterName + " used " + action.SkillName + " on " + target.FighterName;
        yield return new WaitForSeconds(1.0f / battleSpeed);
        messager.gameObject.SetActive(false);

        if (action.ActionType == ACTION_TYPE.PHYSICAL || action.ActionType == ACTION_TYPE.MAGIC) {
            //Taunting
            switch (target.team) {
                case TEAM.PLAYER:
                    if (player_Taunter != null)
                    {
                        target = player_Taunter;
                        messager.gameObject.SetActive(true);
                        messager.Message = target.FighterName + " protected them!";
                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                        messager.gameObject.SetActive(false);
                    }
                    break;
                case TEAM.ENEMY:
                    if (enemy_Taunter != null) {
                        target = enemy_Taunter;
                        messager.gameObject.SetActive(true);
                        messager.Message = target.FighterName + " protected them!";
                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                        messager.gameObject.SetActive(false);
                    }
                    break;
            }
        }

        switch (action.ActionType)
        {
            case ACTION_TYPE.STATUS:
                DisplayAnimation(action, target);
                yield return new WaitForSeconds(animationDelay / battleSpeed);
                action.Action_Effect(user, target);
                break;
            case ACTION_TYPE.CURE:
                target.CureStatus();
                action.Action_Effect(user, target);
                DisplayAnimation(action, target);
                yield return new WaitForSeconds(animationDelay / battleSpeed);

                break;

            case ACTION_TYPE.HEAL:
                int healAmount = 0;
                healAmount = action.Act_Heal(target, user);
                DisplayAnimation(action, target);
                yield return new WaitForSeconds(animationDelay / battleSpeed);
                DisplayNote(healAmount, target, FloatDisplay.HEAL);
                target.Heal(healAmount);
                yield return new WaitForSeconds(damageSpeed / battleSpeed);
                break;
            case ACTION_TYPE.PHYSICAL:
                int damageToDealPhys = 0;
                for (int i = 0; i < action.Hits; i++)
                {
                    damageToDealPhys = action.CalcDamage(target, user);
                    if (target.Status != STATUS.KO)
                    {
                        foreach (Ability a in target.Abilities)
                        {
                            if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_PHYS)
                            {
                                if (action.ActionAttribute == ATTRIBUTE.VARIABLE)
                                {
                                    damageToDealPhys = a.DamageAbsorb(target, damageToDealPhys, user.AttackType);
                                }
                                else
                                {
                                    damageToDealPhys = a.DamageAbsorb(target, damageToDealPhys, action.ActionAttribute);
                                }
                            }
                        }
                    }
                    DisplayAnimation(action, target);
                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                    DisplayNote(damageToDealPhys, target, FloatDisplay.DAMAGE);
                    target.TakeDamage(damageToDealPhys, ACTION_TYPE.PHYSICAL);

                    yield return new WaitForSeconds(damageSpeed / battleSpeed);


                }

                if (target.Status != STATUS.KO)
                {
                    foreach (Ability a in target.Abilities)
                    {
                        if (a.AbilityType == ABILITY_TYPE.ON_PHYS_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                        {
                            if (a.TriggerBased)
                            {
                                if (Random.Range(0, 100) < a.TriggerRate)
                                {
                                    messager.gameObject.SetActive(true);
                                    messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                    messager.gameObject.SetActive(false);
                                    a.DamageTrigger(target, user);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                }
                            }
                            else
                            {
                                messager.gameObject.SetActive(true);
                                messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                messager.gameObject.SetActive(false);
                                a.DamageTrigger(target, user);
                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                            }

                        }
                    }
                }

                if (user.Status != STATUS.KO)
                {
                    foreach (Ability a in user.Abilities)
                    {
                        if (a.AbilityType == ABILITY_TYPE.ON_ATK_END)
                        {
                            if (a.TriggerBased)
                            {
                                if (Random.Range(0, 100) < a.TriggerRate)
                                {
                                    messager.gameObject.SetActive(true);
                                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                    messager.gameObject.SetActive(false);
                                    a.AbilityTrigger(user);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                }
                            }
                            else
                            {
                                messager.gameObject.SetActive(true);
                                messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                messager.gameObject.SetActive(false);
                                a.AbilityTrigger(user);
                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                            }
                        }
                    }
                }
                break;
            case ACTION_TYPE.MAGIC:
                int damageToDealMag = 0;
                for (int i = 0; i < action.Hits; i++)
                {
                    damageToDealMag = action.CalcDamage(target, user);
                    DisplayAnimation(action, target);
                    if (target.Status != STATUS.KO)
                    {
                        foreach (Ability a in target.Abilities)
                        {
                            if (a.AbilityType == ABILITY_TYPE.ABSORB_DAMAGE || a.AbilityType == ABILITY_TYPE.ABSORB_MAG)
                            {
                                damageToDealMag = a.DamageAbsorb(target, damageToDealMag, action.ActionAttribute);
                            }
                        }
                    }
                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                    DisplayNote(damageToDealMag, target, FloatDisplay.DAMAGE);
                    target.TakeDamage(damageToDealMag, ACTION_TYPE.MAGIC);

                    yield return new WaitForSeconds(damageSpeed / battleSpeed);


                }

                if (target.Status != STATUS.KO)
                {
                    foreach (Ability a in target.Abilities)
                    {
                        if (a.AbilityType == ABILITY_TYPE.ON_MAG_DAMAGE || a.AbilityType == ABILITY_TYPE.ON_DAMAGE)
                        {
                            if (a.TriggerBased)
                            {
                                if (Random.Range(0, 100) < a.TriggerRate)
                                {
                                    messager.gameObject.SetActive(true);
                                    messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                    messager.gameObject.SetActive(false);
                                    a.DamageTrigger(target, user);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                }
                            }
                            else
                            {
                                messager.gameObject.SetActive(true);
                                messager.Message = target.FighterName + "'s " + a.SkillName + " activated!";
                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                messager.gameObject.SetActive(false);
                                a.DamageTrigger(target, user);
                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                            }

                        }
                    }
                }

                if (user.Status != STATUS.KO)
                {
                    foreach (Ability a in user.Abilities)
                    {
                        if (a.AbilityType == ABILITY_TYPE.ON_MAG_END)
                        {
                            if (a.TriggerBased)
                            {
                                if (Random.Range(0, 100) < a.TriggerRate)
                                {
                                    messager.gameObject.SetActive(true);
                                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                    messager.gameObject.SetActive(false);
                                    a.AbilityTrigger(user);
                                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                                }
                            }
                            else
                            {
                                messager.gameObject.SetActive(true);
                                messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                                yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                                messager.gameObject.SetActive(false);
                                a.AbilityTrigger(user);
                                yield return new WaitForSeconds(animationDelay / battleSpeed);
                            }

                        }
                    }
                }
                break;
        }

        if (action.ActivateGuard)
        {
            user.guarding = true;
        }

        if (action.ActivateTaunt)
        {
            switch (user.team)
            {
                case TEAM.PLAYER:
                    player_Taunter = user;
                    player_Taunt_Count = 2;
                    break;
                case TEAM.ENEMY:
                    enemy_Taunter = user;
                    enemy_Taunt_Count = 2;
                    break;
            }
        }

        foreach (Ability a in user.Abilities)
        {
            if (a.AbilityType == ABILITY_TYPE.ON_ACT_END)
            {
                if (a.TriggerBased)
                {
                    if (Random.Range(0, 100) < a.TriggerRate)
                    {
                        messager.gameObject.SetActive(true);
                        messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                        messager.gameObject.SetActive(false);
                        a.AbilityTrigger(user);
                        yield return new WaitForSeconds(animationDelay / battleSpeed);
                    }
                }
                else
                {
                    messager.gameObject.SetActive(true);
                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                    messager.gameObject.SetActive(false);
                    a.AbilityTrigger(user);
                    yield return new WaitForSeconds(animationDelay / battleSpeed);
                }
                
            }

            if (a.AbilityType == ABILITY_TYPE.ON_GUARD && user.guarding)
            {
                if (a.TriggerBased)
                {
                    if (Random.Range(0, 100) < a.TriggerRate)
                    {
                        messager.gameObject.SetActive(true);
                        messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                        yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                        messager.gameObject.SetActive(false);
                        a.AbilityTrigger(user);
                    }
                }
                else
                {
                    messager.gameObject.SetActive(true);
                    messager.Message = user.FighterName + "'s " + a.SkillName + " activated!";
                    yield return new WaitForSeconds(notificationSpeed / battleSpeed);
                    messager.gameObject.SetActive(false);
                    a.AbilityTrigger(user);
                }
            }
        }

        //Manage Tags
        int validTags = 0;
        Debug.Log("Checking for valid tags");
        if (user.team == TEAM.PLAYER) {
            foreach (Fighter f in player_party)
            {
                if (!tagPasses.Contains(f) && f != user)
                {
                    if (f.Status != STATUS.STUN && f.Status != STATUS.KO)
                    {
                        validTags++;
                    }
                }
            }
        }

        if (tagSet && user.team == TEAM.PLAYER && !tagPasses.Contains(user) && validTags > 0 /*&& tagChain < (player_party.Count-1)*/)
        {
            //TODO
            if (user.Status == STATUS.KO)
            {
                tagSet = false; // In case user died due to counter
                tagPasses.Clear();
            }
            else
            {
                tagPasses.Add(user);//Keep Track of User
                //Set turn fighter and do not pass through
                //Tag Indication In
                tagIndicator.rectTransform.localPosition = new Vector2(tagStart, tagIndicator.rectTransform.localPosition.y);
                float addAmount = tagStart / 20.0f; // amount the indicator increments
                while (tagIndicator.rectTransform.localPosition.x > 0) {
                    tagIndicator.rectTransform.localPosition = new Vector2(tagIndicator.rectTransform.localPosition.x - addAmount, tagIndicator.rectTransform.localPosition.y);
                    yield return new WaitForSeconds(1 / 120.0f/battleSpeed);
                }

                ChooseTag();
                Debug.Log("Choosing TagTeam");
                while (tagSet) {
                    //Set false on tag decision
                    yield return new WaitForSeconds(1/60.0f);
                }

                //Tag Indicator Out
                while (tagIndicator.rectTransform.localPosition.x > -tagStart)
                {
                    tagIndicator.rectTransform.localPosition = new Vector2(tagIndicator.rectTransform.localPosition.x - addAmount, tagIndicator.rectTransform.localPosition.y);
                    yield return new WaitForSeconds(1 / 120.0f/battleSpeed);
                }

                Debug.Log("Tagteam Chosen");
                if (tagNext != null)
                {
                    turnOrder[turnIndex] = tagNext;
                    turnIndex--;
                    tagNext = null;
                    tagChain++;
                }
                else {
                    tagPasses.Clear();
                    tagChain = 0;
                }
                //Open Target menu to select a team member, have them act
                //Make them next on the timeline without updating the timeline
            }
        }
        else {
            tagPasses.Clear();
            tagChain = 0;
        }
        tagSet = false;
        //Manage Tags End


        if (user.Status == STATUS.CURSE)
        {
            //Take Curse damage
            int curseDamage = user.Stats.MaxHitPoints / Random.Range(12, 16);
            user.TakeDamage(curseDamage, ACTION_TYPE.STATUS);
            DisplayNote(curseDamage, user, FloatDisplay.DAMAGE);
            yield return new WaitForSeconds(animationDelay / battleSpeed);
        }

        if (battleControllerMode != BC_Mode.END) {
            ++turnIndex;
            if (timelineManager != null)
            {
                timelineManager.MoveUp(turnIndex);
            }
            ExecuteTurn();
        }
        
        yield break;
    }

    public IEnumerator ExecuteItem(Item item)
    {
        yield break;
    }

    public IEnumerator ExecuteItem(Item item, Fighter target) {
        yield break;
    }

    public IEnumerator Skipturn() {
        if (turnFighter != null)
        {
            if (turnFighter.Status == STATUS.STUN)
            {
                messager.gameObject.SetActive(true);
                messager.Message = turnFighter.FighterName + " is stunned";
                turnFighter.CureStatus();
                yield return new WaitForSeconds(1.0f / battleSpeed);
                messager.gameObject.SetActive(false);
            }
            else
            {
                Debug.Log(turnFighter.FighterName + "/" + turnFighter.name + " Cannot Act");
            }
        }
        
        //StopCoroutine(battleControls);
        ++turnIndex;
        if (timelineManager != null)
        {
            timelineManager.MoveUp(turnIndex);
        }
        ExecuteTurn();
        yield break;
    }

    public void CheckForResult() {
        bool allEnemyDead = true;
        bool allPlayerDead = true;

        foreach (Fighter f in enemy_party) {
            if (f.Status != STATUS.KO) {
                allEnemyDead = false;
            }
        }

        if (allEnemyDead) {
            battleControllerMode = BC_Mode.END;
            Victory();
            return;
        }

        foreach (Fighter f in player_party) {
            if(f.Status != STATUS.KO) {
                allPlayerDead = false;
            }
        }

        if (allPlayerDead) {
            battleControllerMode = BC_Mode.END;
            GameOver();
            return;
        }


    }

    public void GameOver() {
        if (victoryScreen != null) {
            victoryScreen.gameObject.SetActive(true);
            victoryScreen.PrintLose();
        }
        Debug.Log("YOU DIED");
    }

    public void Victory() {
        if (victoryScreen != null) {
            victoryScreen.gameObject.SetActive(true);
            victoryScreen.PrintResults();
        }
        Debug.Log("YOU WIN");
    }
}
