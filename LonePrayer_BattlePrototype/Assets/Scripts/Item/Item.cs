﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ITEM_TYPE {
CONSUMABLE,
EQUIP
}

public class Item : MonoBehaviour
{
    [SerializeField]
    protected Sprite _sprite;
    public Sprite Item_Sprite {
        get { return _sprite; }
    }

    [SerializeField]
    protected string _name = "Sample Item", _description = "An item. . . I guess";

    public string Item_Name {
        get { return _name; }
    }

    public string Item_Description {
        get { return _description; }
    }

    //[SerializeField]
    //protected ITEM_TYPE _itemType = ITEM_TYPE.CONSUMABLE;

    public virtual bool BattleUsable {
        get { return false; }
    }

    public virtual bool FieldUsable {
        get { return false; }
    }

    public virtual ITEM_TYPE Item_Type {
        get { return ITEM_TYPE.CONSUMABLE; }
    }

    [SerializeField]
    protected int _cost = 10;
    public int Cost {
        get { return _cost; }
    }

    public virtual void Use() {
        return;
    }
}
