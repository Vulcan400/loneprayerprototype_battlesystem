﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ItemSlot {
    [SerializeField]
    private Item item;
    [SerializeField]
    private int quantity;

    public Item GetItem {
        get { return item; }
    }

    public int Quantity {
        get { return quantity; }
    }

    public ItemSlot(Item _item, int _quantity = 1) {
        item = _item;
        quantity = _quantity;
    }

    public void Use(int useAmount = 1) {
        if (quantity - useAmount < 0)
        {
            return;
        }

        quantity -= useAmount;
    }

    public void Get(int getAmount = 1) {
        if (quantity + getAmount < 1) {
            return;
        }

        quantity += getAmount;
    }
}

public class Bag: MonoBehaviour
{
    [SerializeField]
    private List<ItemSlot> itemSlots;

    public List<ItemSlot> ItemSlots {
        get { return itemSlots; }
    }

    [SerializeField]
    protected int money = 5;
    public int Money {
        get { return money; }
        set {
            int moneyGet = value + money;
            if (moneyGet < 0) {
                money = 0;
            }
            else if (moneyGet > 999999) {
                money = 999999;
            }
            else {
                money = moneyGet;
            }
        }
    }

    public void UseItem(int slot) {
        itemSlots[slot].Use();
        if (itemSlots[slot].Quantity <= 0) {
            itemSlots.Remove(itemSlots[slot]);
        }
    }

    public void SellItem(int slot, int numberToSell = 1) {
        if (numberToSell > itemSlots[slot].Quantity)
        {
            numberToSell = itemSlots[slot].Quantity;
        }
        itemSlots[slot].Use(numberToSell);
        money += (itemSlots[slot].GetItem.Cost * numberToSell);
        Debug.Log("Trashed " + numberToSell + " " + itemSlots[slot].GetItem.Item_Name + ". " + itemSlots[slot].Quantity + " left.");
        if (itemSlots[slot].Quantity <= 0)
        {
            itemSlots.Remove(itemSlots[slot]);
        }
    }

    public void Trash(int slot, int numberToTrash = 1) {
        if (numberToTrash > itemSlots[slot].Quantity) {
            numberToTrash = itemSlots[slot].Quantity;
        }
        itemSlots[slot].Use(numberToTrash);
        Debug.Log("Trashed " + numberToTrash + " " + itemSlots[slot].GetItem.Item_Name + ". " + itemSlots[slot].Quantity + " left.");
        if (itemSlots[slot].Quantity <= 0)
        {
            itemSlots.Remove(itemSlots[slot]);
        }
    }
}
