﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charm_Equip : Equipment_Item
{
    //Proportional Boosts give a percent based boost based on the base stats of a fighter. ie 10*(20/100)=12. 
    //these become stronger in the late game.
    [SerializeField]
    protected STATS _proportionalBoost;
    public STATS ProportionalBoost
    {
        get { return _proportionalBoost; }
    }

    public override ITEM_TYPE Item_Type
    {
        get { return ITEM_TYPE.EQUIP; }
    }

    public override EQUIP_TYPE Type
    {
        get { return EQUIP_TYPE.CHARM; }
    }
}
