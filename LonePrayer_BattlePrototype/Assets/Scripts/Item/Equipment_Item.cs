﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EQUIP_TYPE {
    WEAPON,
    BODY,
    CHARM
    //Charms can be equipped by either spirits or conjurers
}

public class Equipment_Item : Item
{
    //[SerializeField]
    //protected EQUIP_TYPE _equipmentType = EQUIP_TYPE.CHARM;
    public virtual EQUIP_TYPE Type {
        get { return EQUIP_TYPE.CHARM; }
    }

    public override ITEM_TYPE Item_Type
    {
        get { return ITEM_TYPE.EQUIP; }
    }

    //Direct Boosts give a hard addition to specific stats. i.e 10+5 = 15
    [SerializeField]
    protected STATS _directBoost;
    public STATS DirectBoost {
        get {return _directBoost; }
    }

    //To Add : ABility Slot
    [SerializeField]
    protected Ability ability;
    public Ability Ability {
        get {return ability; }
    }


}
