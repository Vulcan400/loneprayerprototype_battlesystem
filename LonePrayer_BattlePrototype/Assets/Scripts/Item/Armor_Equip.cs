﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor_Equip : Equipment_Item
{
    [SerializeField]
    protected RESISTANCES _resistBonus;
    public RESISTANCES ResistBonus {
        get { return _resistBonus; }
    }

    public override ITEM_TYPE Item_Type
    {
        get { return ITEM_TYPE.EQUIP; }
    }

    public override EQUIP_TYPE Type
    {
        get { return EQUIP_TYPE.BODY; }
    }
}
