﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Consumable_Item : Item
{
    [SerializeField]
    protected ACTION_TYPE actionType = ACTION_TYPE.HEAL;

    public ACTION_TYPE ActionType {
        get {
            return actionType;
        }
    }

    [SerializeField]
    protected TARGET_TYPE target_Type = TARGET_TYPE.ALLY;

    public TARGET_TYPE TargetType {
        get { return target_Type; }
    }

    [SerializeField]
    protected int itemPower = 50;

    public override bool BattleUsable {
        get {
            return true;
        }
    }

    public override bool FieldUsable {
        get {
            bool usable = false;
            switch (actionType) {
                case ACTION_TYPE.HEAL:
                    usable = true;
                    break;
                case ACTION_TYPE.SP_RESTORE:
                    usable = true;
                    break;
                case ACTION_TYPE.REVIVE:
                    usable = true;
                    break;
                default:
                    usable = false;
                    break;
            }
            return usable;
        }
    }
}
