﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_Equip : Equipment_Item
{
    [SerializeField]
    protected Action attack;

    public Action Attack {
        get { if (attack == null) {

                if (BattleController.S == null) {
                    return null;
                }

                return BattleController.S.defaultActions[0];
            } else {
                return attack;
            }
        }
    }

    [SerializeField]
    protected ATTRIBUTE _attackAttribute = ATTRIBUTE.CUT;
    public ATTRIBUTE AttackAttribute {
        get {
            if (attack == null)
            {
                return _attackAttribute;
            }
            else
            {
                return Attack.ActionAttribute;
            }
        }
    }

    public override ITEM_TYPE Item_Type
    {
        get { return ITEM_TYPE.EQUIP; }
    }

    public override EQUIP_TYPE Type
    {
        get { return EQUIP_TYPE.WEAPON; }
    }
}
