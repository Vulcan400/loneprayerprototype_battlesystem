﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Party: MonoBehaviour
{
    public static Party S;

    public Conjurer leader;
    public Spirit spirit1, spirit2;

    //Array of Fighters
    protected List<Player_Fighter> _party;
    public List<Player_Fighter> activeFighters {
        get { return _party; }
        set { _party = value; }
    }

    //Fighters not in use by the player go here

    public List<Spirit> reserveFighters;
    public Bag bag;

    void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        else { Destroy(this.gameObject); }
    }

    void Start()
    {
        leader.Start();
        spirit1.Start();
        spirit2.Start();
    }
}
